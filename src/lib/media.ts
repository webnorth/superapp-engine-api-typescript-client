import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, ICollection, IMediaLike, IMediaListParams, IMediaUploadSpecs, IMediaSpecs, IMediaEndpoint } from './common/interfaces';
import { isMediaLike } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * Media endpoint
 *
 * @class MediaEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IMediaEndpoint}
 */
export class MediaEndpoint extends Endpoint implements IMediaEndpoint {

  /**
   * Creates an instance of MediaEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof MediaEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/media');
  }

  /**
   * List all media files.
   *
   * @param {IMediaListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IMediaLike>>>}
   *
   * @memberof MediaEndpoint
   */
  list(params?: IMediaListParams): Observable<AxiosResponse<ICollection<IMediaLike>>> {
    // console.log('list(params?: IMediaListParams)', params);
    const url: string = this.prefix;
    return this.getCollection<IMediaLike>(url, params, isMediaLike);
  }

  /**
   * Upload new media file.
   *
   * @param {IMediaUploadSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IMediaLike>>>}
   *
   * @memberof MediaEndpoint
   */
  upload(specs: IMediaUploadSpecs): Observable<AxiosResponse<IContainer<IMediaLike>>> {
    // console.log('upload(specs: IMediaUploadSpecs)', specs);
    const url: string = this.prefix;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IMediaLike>(promise, isMediaLike);
  }

  /**
   * Get media file by id.
   *
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<IMediaLike>>>}
   *
   * @memberof MediaEndpoint
   */
  get(id: number): Observable<AxiosResponse<IContainer<IMediaLike>>> {
    // console.log('get(id: number)', id);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.get(url);
    return this.getContainer<IMediaLike>(promise, isMediaLike);
  }

  /**
   * Update media file by id.
   *
   * @param {number} id
   * @param {IMediaSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IMediaLike>>>}
   *
   * @memberof MediaEndpoint
   */
  update(id: number, specs: IMediaSpecs): Observable<AxiosResponse<IContainer<IMediaLike>>> {
    // console.log('update(id: number, specs: IMediaSpecs)', id, specs);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IMediaLike>(promise, isMediaLike);
  }

  /**
   * Delete media file by id.
   *
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof MediaEndpoint
   */
  delete(id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('delete(id: number)', id);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

}
