import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, ILangParam, IAttributeValues, ICategory, } from './common/interfaces';
import { isAttributeValues, isCategory, } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * Article raw object
 *
 * @export
 * @interface IArticle
 */
export interface IArticle {
  id: number;
  name: string;
  description: string;
  body: string;
  sort_order: number;
  categories: ICategory[];
  publish_at: string;
  publish_until: string;
  attributes?: IAttributeValues;
}

/**
 * Article specs
 *
 * @export
 * @interface IArticleSpecs
 */
export interface IArticleSpecs extends ILangParam {
  name: string;
  description?: string;
  body?: string;
  categories?: string;
  publish_at?: string;
  publish_until?: string;
}

/**
 * Article get params
 *
 * @export
 * @interface IArticleGetParams
 */
export interface IArticleGetParams extends ILangParam {
  with_attributes?: 0 | 1;
  expand_attributes?: 0 | 1;
}

/**
 * Article list params
 *
 * @export
 * @interface IArticleListParams
 */
export interface IArticleListParams extends ICollectionParams, IArticleGetParams {
  group_plugin_id?: number;
  categories?: string;
  upcoming?: 0 | 1;
  sort?: string;
}

/**
 * Article endpoint methods
 *
 * @export
 * @interface IArticleEndpoint
 */
export interface IArticleEndpoint {
  list(group_id: number, params?: IArticleListParams): Observable<AxiosResponse<ICollection<IArticle>>>;
  create(group_id: number, specs: IArticleSpecs): Observable<AxiosResponse<IContainer<IArticle>>>;
  get(group_id: number, id: number, params?: IArticleGetParams): Observable<AxiosResponse<IContainer<IArticle>>>;
  update(group_id: number, id: number, specs: IArticleSpecs): Observable<AxiosResponse<IContainer<IArticle>>>;
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}

/**
 * Guard of IArticle.
 *
 * @export
 * @param {*} data
 * @returns {data is IArticle}
 */
export function isArticle(data): data is IArticle {
  if (data === null || typeof data !== 'object') {
    console.error('IArticle must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IArticle.id must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('IArticle.name must be a string', data);
    return false;
  }
  if (typeof data.description !== 'string' && data.description !== null) {
    console.error('IArticle.description must be a string or NULL', data);
    return false;
  }
  if (typeof data.body !== 'string' && data.body !== null) {
    console.error('IArticle.body must be a string or NULL', data);
    return false;
  }
  if (typeof data.sort_order !== 'number') {
    console.error('IArticle.sort_order must be a number', data);
    return false;
  }
  if (!Array.isArray(data.categories)) {
    console.error('IArticle.categories must be an array', data);
    return false;
  }
  if (!data.categories.every(isCategory)) {
    return false;
  }
  // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
  if (typeof data.publish_at !== 'string' && data.publish_at !== null) {
    console.error('IArticle.publish_at must be a string or NULL', data);
    return false;
  }
  // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
  if (typeof data.publish_until !== 'string' && data.publish_until !== null) {
    console.error('IArticle.publish_until must be a string or NULL', data);
    return false;
  }
  if (data.attributes && !isAttributeValues(data.attributes)) {
    return false;
  }
  return true;
}

/**
 * Article endpoint
 *
 * @class ArticleEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IArticleEndpoint}
 */
export class ArticleEndpoint extends Endpoint implements IArticleEndpoint {

  /**
   * Creates an instance of ArticleEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof ArticleEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/groups/:group_id/articles');
  }

  /**
   * List all articles.
   *
   * @param {number} group_id
   * @param {IArticleListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IArticle>>>}
   *
   * @memberof ArticleEndpoint
   */
  list(group_id: number, params?: IArticleListParams): Observable<AxiosResponse<ICollection<IArticle>>> {
    // console.log('list(group_id, params?)', group_id, params);
    const url: string = this.replacePrefixSegments({ group_id });
    // console.log('url', url);
    return this.getCollection<IArticle>(url, params, isArticle);
  }

  /**
   * Create new article.
   *
   * @param {number} group_id
   * @param {IArticleSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IArticle>>>}
   *
   * @memberof ArticleEndpoint
   */
  create(group_id: number, specs: IArticleSpecs): Observable<AxiosResponse<IContainer<IArticle>>> {
    // console.log('create(group_id, specs)', group_id, specs);
    const url: string = this.replacePrefixSegments({ group_id });
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IArticle>(promise, isArticle);
  }

  /**
   * Get article by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {IArticleGetParams} [params]
   * @returns {Observable<AxiosResponse<IContainer<IArticle>>>}
   *
   * @memberof ArticleEndpoint
   */
  get(group_id: number, id: number, params?: IArticleGetParams): Observable<AxiosResponse<IContainer<IArticle>>> {
    // console.log('get(group_id, id, params?)', group_id, id, params);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    const promise = this.api.http.get(url, { params });
    return this.getContainer<IArticle>(promise, isArticle);
  }

  /**
   * Update article by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {IArticleSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IArticle>>>}
   *
   * @memberof ArticleEndpoint
   */
  update(group_id: number, id: number, specs: IArticleSpecs): Observable<AxiosResponse<IContainer<IArticle>>> {
    // console.log('update(group_id, id, specs)', group_id, id, specs);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IArticle>(promise, isArticle);
  }

  /**
   * Delete article by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof ArticleEndpoint
   */
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('get(group_id, id)', group_id, id);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

}
