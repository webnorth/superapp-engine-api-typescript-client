import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, IUser, IUserSpecs, IUserEndpoint, } from './common/interfaces';
import { isUser } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * User endpoint
 *
 * @class UserEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IUserEndpoint}
 */
export class UserEndpoint extends Endpoint implements IUserEndpoint {

  /**
   * Creates an instance of UserEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof UserEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/users');
  }

  /**
   * Create new user.
   *
   * @param {IUserSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IUser>>>}
   *
   * @memberof UserEndpoint
   */
  create(specs: IUserSpecs): Observable<AxiosResponse<IContainer<IUser>>> {
    // console.log('create(specs: IUserSpecs)', specs);
    const url: string = this.prefix;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IUser>(promise, isUser);
  }

  /**
   * Delete user by id.
   *
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof UserEndpoint
   */
  delete(id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('delete(id: number)', id);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

  /**
   * List all users.
   *
   * @param {ICollectionParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IUser>>>}
   *
   * @memberof UserEndpoint
   */
  list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IUser>>> {
    // console.log('list(params?: ICollectionParams)', params);
    const url: string = this.prefix;
    return this.getCollection<IUser>(url, params, isUser);
  }

  /**
   * Get user by id.
   *
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<IUser>>>}
   *
   * @memberof UserEndpoint
   */
  get(id: number): Observable<AxiosResponse<IContainer<IUser>>> {
    // console.log('get(id: number)', id);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.get(url);
    return this.getContainer<IUser>(promise, isUser);
  }

  /**
   * Update user by id.
   *
   * @param {number} id
   * @param {IUserSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IUser>>>}
   *
   * @memberof UserEndpoint
   */
  update(id: number, specs: IUserSpecs): Observable<AxiosResponse<IContainer<IUser>>> {
    // console.log('update(id: number, specs: IUserSpecs)', id, specs);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IUser>(promise, isUser);
  }

}
