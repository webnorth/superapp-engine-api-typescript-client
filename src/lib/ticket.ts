import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import {
  IContainer,
  ICollection,
  ITicket,
  IAuthTicketListParams,
  ITicketListParams,
  ITicketCheckParams,
  ITicketGetParams,
  ITicketSpecs,
  ITicketEndpoint,
} from './common/interfaces';
import { isTicket } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * Ticket endpoint
 *
 * @class TicketEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {ITicketEndpoint}
 */
export class TicketEndpoint extends Endpoint implements ITicketEndpoint {

  /**
   * Creates an instance of TicketEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof TicketEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    // TODO: The correct prefix
    super(superappEngine, '/groups/:group_id/tickets');
  }

  /**
   * List auth tickets.
   *
   * @param {IAuthTicketListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<ITicket>>>}
   *
   * @memberof TicketEndpoint
   */
  authList(params?: IAuthTicketListParams): Observable<AxiosResponse<ICollection<ITicket>>> {
    // console.log('list(params?: IAuthTicketListParams)', params);
    const url: string = '/auth/tickets';
    // console.log('url', url);
    return this.getCollection<ITicket>(url, params, isTicket);
  }

  /**
   * List all tickets.
   *
   * @param {number} group_id
   * @param {ITicketListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<ITicket>>>}
   *
   * @memberof TicketEndpoint
   */
  list(group_id: number, params?: ITicketListParams): Observable<AxiosResponse<ICollection<ITicket>>> {
    // console.log('list(group_id: number, params?: ITicketListParams)', group_id, params);
    const url: string = this.replacePrefixSegments({ group_id });
    // console.log('url', url);
    return this.getCollection<ITicket>(url, params, isTicket);
  }

  /**
   * Create new ticket.
   *
   * @param {number} group_id
   * @param {ITicketSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
   *
   * @memberof TicketEndpoint
   */
  create(group_id: number, specs: ITicketSpecs): Observable<AxiosResponse<IContainer<ITicket>>> {
    // console.log('create(group_id: number, specs: ITicketSpecs)', group_id, specs);
    const url: string = this.replacePrefixSegments({ group_id });
    const promise = this.api.http.post(url, specs);
    return this.getContainer<ITicket>(promise, isTicket);
  }

  /**
   * Check ticket by code.
   *
   * @param {number} group_id
   * @param {ITicketCheckParams} params
   * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
   *
   * @memberof TicketEndpoint
   */
  check(group_id: number, params: ITicketCheckParams): Observable<AxiosResponse<IContainer<ITicket>>> {
    // console.log('check(group_id: number, params: ITicketCheckParams)', group_id, params);
    const url: string = this.replacePrefixSegments({ group_id }) + '/check';
    const promise = this.api.http.get(url, { params });
    return this.getContainer<ITicket>(promise, isTicket);
  }

  /**
   * Checkin ticket by code.
   *
   * @param {number} group_id
   * @param {ITicketCheckParams} params
   * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
   *
   * @memberof TicketEndpoint
   */
  checkin(group_id: number, params: ITicketCheckParams): Observable<AxiosResponse<IContainer<ITicket>>> {
    // console.log('checkin(group_id: number, params: ITicketCheckParams)', group_id, params);
    const url: string = this.replacePrefixSegments({ group_id }) + '/checkin';
    const promise = this.api.http.get(url, { params });
    return this.getContainer<ITicket>(promise, isTicket);
  }

  /**
   * Get ticket by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {ITicketGetParams} [params]
   * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
   *
   * @memberof TicketEndpoint
   */
  get(group_id: number, id: number, params?: ITicketGetParams): Observable<AxiosResponse<IContainer<ITicket>>> {
    // console.log('get(group_id: number, id: number, params?: ITicketGetParams)', group_id, id, params);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    const promise = this.api.http.get(url, { params });
    return this.getContainer<ITicket>(promise, isTicket);
  }

  /**
   * Checkout ticket by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {ITicketGetParams} [params]
   * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
   *
   * @memberof TicketEndpoint
   */
  checkout(group_id: number, id: number, params?: ITicketGetParams): Observable<AxiosResponse<IContainer<ITicket>>> {
    // console.log('checkout(group_id: number, id: number, params?: ITicketGetParams)', group_id, id, params);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id + '/checkout';
    const promise = this.api.http.get(url, { params });
    return this.getContainer<ITicket>(promise, isTicket);
  }

  /**
   * Invalidate ticket by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {ITicketGetParams} [params]
   * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
   *
   * @memberof TicketEndpoint
   */
  invalidate(group_id: number, id: number, params?: ITicketGetParams): Observable<AxiosResponse<IContainer<ITicket>>> {
    // console.log('invalidate(group_id: number, id: number, params?: ITicketGetParams)', group_id, id, params);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id + '/invalidate';
    const promise = this.api.http.get(url, { params });
    return this.getContainer<ITicket>(promise, isTicket);
  }

  /**
   * Delete ticket by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof TicketEndpoint
   */
  delete_pdf(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('get(group_id: number, id: number)', group_id, id);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id + '/delete_pdf';
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

}
