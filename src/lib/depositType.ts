import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import {
  IContainer,
  ICollection,
  IDepositType,
  IDepositTypeSpecs,
  IDepositTypeGetParams,
  IDepositTypeListParams,
  IDepositTypeUpdateSpecs,
  IDepositTypeEndpoint,
} from './common/interfaces';
import { isDepositType } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * DepositType endpoint
 *
 * @class DepositTypeEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IDepositTypeEndpoint}
 */
export class DepositTypeEndpoint extends Endpoint implements IDepositTypeEndpoint {

  /**
   * Creates an instance of DepositTypeEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof DepositTypeEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine)', superappEngine);
    super(superappEngine, '/groups/:group_id/deposit_types');
  }

  /**
   * List all deposit types.
   *
   * @param {number} group_id
   * @param {IDepositTypeListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IDepositType>>>}
   *
   * @memberof DepositTypeEndpoint
   */
  list(group_id: number, params?: IDepositTypeListParams): Observable<AxiosResponse<ICollection<IDepositType>>> {
    // console.log('list(group_id, params?)', group_id, params);
    const url: string = this.replacePrefixSegments({ group_id });
    // console.log('url', url);
    return this.getCollection<IDepositType>(url, params, isDepositType);
  }

  /**
   * Create new deposit type.
   *
   * @param {number} group_id
   * @param {IDepositTypeSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IDepositType>>>}
   *
   * @memberof DepositTypeEndpoint
   */
  create(group_id: number, specs: IDepositTypeSpecs): Observable<AxiosResponse<IContainer<IDepositType>>> {
    // console.log('create(group_id, specs)', group_id, specs);
    const url: string = this.replacePrefixSegments({ group_id });
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IDepositType>(promise, isDepositType);
  }

  /**
   * Get deposit type by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {IDepositTypeGetParams} [params]
   * @returns {Observable<AxiosResponse<IContainer<IDepositType>>>}
   *
   * @memberof DepositTypeEndpoint
   */
  get(group_id: number, id: number, params?: IDepositTypeGetParams): Observable<AxiosResponse<IContainer<IDepositType>>> {
    // console.log('get(group_id, id)', group_id, id);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    // console.log('url', url);
    const promise = this.api.http.get(url);
    return this.getContainer<IDepositType>(promise, isDepositType);
  }

  /**
   * Update deposit type by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {IDepositTypeUpdateSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IDepositType>>>}
   *
   * @memberof DepositTypeEndpoint
   */
  update(group_id: number, id: number, specs: IDepositTypeUpdateSpecs): Observable<AxiosResponse<IContainer<IDepositType>>> {
    // console.log('update(group_id, id, specs)', group_id, id, specs);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IDepositType>(promise, isDepositType);
  }

  /**
   * Delete deposit type by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof DepositTypeEndpoint
   */
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('get(group_id, id)', group_id, id);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

}
