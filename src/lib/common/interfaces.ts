import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios';

/********************************************/
/**
 * SuperApp Engine
 */
/********************************************/


export {
  ISetting,
} from '../setting';


/**
 * AttributeValues object
 *
 * @export
 * @interface IAttributeValues
 */
export interface IAttributeValues {
  [key: string]: any;
}

/**
 * User object stub
 *
 * @export
 * @interface IUserStub
 */
export interface IUserStub {
  id: number;
  is_guest: 0 | 1;
  email: string;
  first_name: string;
  last_name: string;
  phone_code: string | number;
  phone: string;
  lang_pref: string[];
}

/**
 * User-like object
 *
 * @export
 * @interface IUserLike
 */
export interface IUserLike extends IUserStub {
  terms_accepted_at: string;
  timezone: string;
  attributes?: IAttributeValues;
}

/**
 * User-like specs
 *
 * @export
 * @interface IUserLikeSpecs
 */
export interface IUserLikeSpecs extends IAuthMeParams {
  email: string;
  password: string;
  first_name?: string;
  last_name?: string;
  phone_code?: string | number;
  phone?: string;
  lang_pref?: string[];
}

/**
 * Auth register() specs
 *
 * @export
 * @interface IAuthRegisterSpecs
 */
export interface IAuthRegisterSpecs extends IUserLikeSpecs {
  first_name: string;
  last_name: string;
  phone_code: string | number;
  phone: string;
  terms_accepted_at?: string;
  timezone?: string;
}

/**
 * Auth register() guest specs
 *
 * @export
 * @interface IAuthGuestRegisterSpecs
 */
export interface IAuthGuestRegisterSpecs extends IAuthMeParams {
  is_guest: 1;
  lang_pref?: string[];
  terms_accepted_at?: string;
}

/**
 * AuthUser raw object
 *
 * @export
 * @interface IAuthUser
 */
export interface IAuthUser extends IUserLike {
  token?: string;
  groups: number[];
}

/**
 * Auth reminder() params
 *
 * @export
 * @interface IAuthReminderParams
 */
export interface IAuthReminderParams {
  email?: string;
  phone?: string;
  phone_code?: string | number;
  lang?: string;
}

/**
 * Auth login() params
 *
 * @export
 * @interface IAuthLoginParams
 */
export interface IAuthLoginParams extends IAuthMeParams {
  email?: string;
  password?: string;
  pin?: string;
  phone?: string;
  phone_code?: string | number;
}

/**
 * Auth me() params
 *
 * @export
 * @interface IAuthMeParams
 */
export interface IAuthMeParams {
  with_attributes?: 0 | 1;
}

/**
 * Auth update() specs
 *
 * @export
 * @interface IAuthUpdateSpecs
 */
export interface IAuthUpdateSpecs extends IAuthMeParams {
  email?: string;
  password?: string;
  first_name?: string;
  last_name?: string;
  phone?: string;
  phone_code?: string | number;
  lang_pref?: string[];
  is_guest?: 0 | 1;
  terms_accepted_at?: string;
  timezone?: string;
  [key: string]: any;
}

/**
 * Auth subscribe() params
 *
 * @export
 * @interface IAuthSubscribeParams
 */
export interface IAuthSubscribeParams {
  groups: string;
}

/**
 * Autbuy() specs
 *
 * @export
 * @interface IAuthBuySpecs
 */
export interface IAuthBuySpecs {
  product: string;
}

/**
 * AuthIsBoughtParams
 *
 * @export
 * @interface IAuthIsBoughtParams
 */
export interface IAuthIsBoughtParams {
  product: string;
}

/**
 * AuthIsBought
 *
 * @export
 * @interface IAuthIsBought
 */
export interface IAuthIsBought {
  is_bought: boolean;
}

/**
 * Auth endpoint methods
 *
 * @export
 * @interface IAuthEndpoint
 */
export interface IAuthEndpoint {
  register(specs: IAuthRegisterSpecs | IAuthGuestRegisterSpecs): Observable<AxiosResponse<IContainer<IAuthUser>>>;
  reminder(params: IAuthReminderParams): Observable<AxiosResponse<IContainer<void>>>;
  login(params: IAuthLoginParams): Observable<AxiosResponse<IContainer<IAuthUser>>>;
  me(params: IAuthMeParams): Observable<AxiosResponse<IContainer<IUserLike>>>;
  update(specs: IAuthUpdateSpecs): Observable<AxiosResponse<IContainer<IUserLike>>>;
  subscribe(params: IAuthSubscribeParams): Observable<AxiosResponse<IContainer<void>>>;
  unsubscribe(params: IAuthSubscribeParams): Observable<AxiosResponse<IContainer<void>>>;
  logout(): Observable<AxiosResponse<IContainer<void>>>;
  request_deletion(): Observable<AxiosResponse<IContainer<void>>>;
  download(): Observable<AxiosResponse<IContainer<void>>>;
  buy(specs: IAuthBuySpecs): Observable<AxiosResponse<IContainer<void>>>;
  is_bought(params: IAuthIsBoughtParams): Observable<AxiosResponse<IContainer<IAuthIsBought>>>;
}

/**
 * User raw object
 *
 * @export
 * @interface IUser
 */
export interface IUser extends IUserLike {
  roles: IRole[];
}

/**
 * UserSpecs
 *
 * @export
 * @interface IUserSpecs
 */
export interface IUserSpecs extends IUserLikeSpecs {

}

/**
 * User endpoint methods
 *
 * @export
 * @interface IUserEndpoint
 */
export interface IUserEndpoint {
  list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IUser>>>;
  create(specs: IUserSpecs): Observable<AxiosResponse<IContainer<IUser>>>;
  get(id: number): Observable<AxiosResponse<IContainer<IUser>>>;
  update(id: number, specs: IUserSpecs): Observable<AxiosResponse<IContainer<IUser>>>;
  delete(id: number): Observable<AxiosResponse<IContainer<void>>>;
  // TODO: search(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IUser>>>;
}

/**
 * PushToken raw object
 *
 * @export
 * @interface IPushToken
 */
export interface IPushToken {
  id: number;
  push_token: string;
  device_uid: string;
  platform: string;
  app_version: string;
  active: boolean;
  broken: boolean;
}

/**
 * PushToken specs
 *
 * @export
 * @interface IPushTokenSpecs
 */
export interface IPushTokenSpecs {
  push_token: string;
  device_uid?: string;
  platform?: 'android' | 'ios';
  app_version?: string;
  active?: 0 | 1;
  broken?: 0 | 1;
}

/**
 * PushToken endpoint methods
 *
 * @export
 * @interface IPushTokenEndpoint
 */
export interface IPushTokenEndpoint {
  list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IPushToken>>>;
  create(specs: IPushTokenSpecs): Observable<AxiosResponse<IContainer<IPushToken>>>;
  get(id: number): Observable<AxiosResponse<IContainer<IPushToken>>>;
  update(id: number, specs: IPushTokenSpecs): Observable<AxiosResponse<IContainer<IPushToken>>>;
  delete(id: number): Observable<AxiosResponse<IContainer<void>>>;
}

/**
 * MessageType raw object
 *
 * @export
 * @type IMessageType
 */
export type IMessageType = 'email' | 'push' | 'sms';

/**
 * MessageLike object
 *
 * @export
 * @interface IMessageLike
 */
export interface IMessageLike {
  id: number;
  subject: string;
  body: string;
  plain_body: string;
  type: IMessageType;
  last_read: string;
  template_attributes: IAttributeValues;
  template_destinations: IDestination[];
  created_at: string;
}

/**
 * MessageLike list params
 *
 * @export
 * @interface IMessageLikeListParams
 */
export interface IMessageLikeListParams extends ICollectionParams {
  type?: IMessageType;
  sort?: string;
}

/**
 * AuthMessage list params
 *
 * @export
 * @interface IAuthMessageListParams
 */
export interface IAuthMessageListParams extends IMessageLikeListParams {
  groups?: string;
}

/**
 * AuthMessage endpoint methods
 *
 * @export
 * @interface IAuthMessageEndpoint
 */
export interface IAuthMessageEndpoint {
  list(params?: IAuthMessageListParams): Observable<AxiosResponse<ICollection<IMessageLike>>>;
  touch(id: number): Observable<AxiosResponse<IContainer<IMessageLike>>>;
}

/**
 * PaymentMethod object
 *
 * @export
 * @interface IPaymentMethod
 */
export interface IPaymentMethod {
  id: string;
  card: IPaymentCard,
  type: 'card'
}

/**
 * PaymentCard object
 *
 * @export
 * @interface IPaymentCard
 */
export interface IPaymentCard {
  brand: string;
  last4: string;
}

/**
 * PaymentIntent object
 *
 * @export
 * @interface IPaymentIntent
 */
export interface IPaymentIntent {
  id: string;
  client_secret: string;
}

/**
 * StripeDeposit object
 *
 * @export
 * @interface IStripeDeposit
 */
export interface IStripeDeposit {
  transaction: string;
}

/**
 * Stripe payment method specs
 *
 * @export
 * @interface IPaymentMethodSpecs
 */
export interface IPaymentMethodSpecs {
  payment_method_id: number;
}

/**
 * StripeDeposit specs
 *
 * @export
 * @interface IStripeDepositSpecs
 */
export interface IStripeDepositSpecs {
  amount: number;
  currency: IDepositCurency;
}

/**
 * DepositCurency raw object
 *
 * @export
 * @type IDepositCurency
 */
export type IDepositCurency = 'DKK' | 'EUR' | 'USD';

/**
 * AuthStripe endpoint methods
 *
 * @export
 * @interface IAuthStripeEndpoint
 */
export interface IAuthStripeEndpoint {
  list(): Observable<AxiosResponse<ICollection<IPaymentMethod>>>;
  create(specs: IPaymentMethodSpecs): Observable<AxiosResponse<IContainer<void>>>;
  deposit(specs: IStripeDepositSpecs): Observable<AxiosResponse<IContainer<IStripeDeposit>>>;
  intent(): Observable<AxiosResponse<IContainer<IPaymentIntent>>>;
  delete(): Observable<AxiosResponse<IContainer<void>>>;
}

/**
 * MediaLike object
 *
 * @export
 * @interface IMediaLike
 */
export interface IMediaLike {
  id: number;
  filename: string;
  size: number;
  mime_type: any;
  name: string;
  description: string;
  url: string;
}

/**
 * ImageType raw object
 *
 * @export
 * @type IImageType
 */
export type IImageType = 'image/jpeg' | 'image/png' | 'image/gif';

/**
 * MediaImage raw object
 *
 * @export
 * @interface IMediaImage
 */
export interface IMediaImage extends IMediaLike {
  mime_type: IImageType;
  presets: { [key: string]: string };
}

/**
 * Media list() params
 *
 * @export
 * @interface IMediaListParams
 */
export interface IMediaListParams extends ICollectionParams {
  media_type?: string;
  search?: string;
}

/**
 * Media specs
 *
 * @export
 * @interface IMediaSpecs
 */
export interface IMediaSpecs {
  name?: string;
  description?: string;
}

/**
 * Media upload() specs
 *
 * @export
 * @interface IMediaUploadSpecs
 */
export interface IMediaUploadSpecs extends IMediaSpecs {
  file: Blob;
}

/**
 * Media endpoint methods
 *
 * @export
 * @interface IMediaEndpoint
 */
export interface IMediaEndpoint {
  list(params?: IMediaListParams): Observable<AxiosResponse<ICollection<IMediaLike>>>;
  upload(specs: IMediaUploadSpecs): Observable<AxiosResponse<IContainer<IMediaLike>>>;
  get(id: number): Observable<AxiosResponse<IContainer<IMediaLike>>>;
  update(id: number, specs: IMediaSpecs): Observable<AxiosResponse<IContainer<IMediaLike>>>;
  delete(id: number): Observable<AxiosResponse<IContainer<void>>>;
}

/**
 * Role raw object
 *
 * @export
 * @interface IRole
 */
export interface IRole {
  id: number;
  name: string;
  description: string;
  guard_name: 'api';
  permissions: string[];
}

/**
 * Role specs
 *
 * @export
 * @interface IRoleSpecs
 */
export interface IRoleSpecs {
  name: string;
  description?: string;
}

/**
 * Role endpoint methods
 *
 * @export
 * @interface IRoleEndpoint
 */
export interface IRoleEndpoint {
  list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IRole>>>;
  create(specs: IRoleSpecs): Observable<AxiosResponse<IContainer<IRole>>>;
  get(id: number): Observable<AxiosResponse<IContainer<IRole>>>;
  update(id: number, specs: IRoleSpecs): Observable<AxiosResponse<IContainer<IRole>>>;
  delete(id: number): Observable<AxiosResponse<IContainer<void>>>;
}

/**
 * PermissionCategory raw object
 *
 * @export
 * @interface IPermissionCategory
 */
export interface IPermissionCategory {
  id: number;
  name: string;
  slug: string;
  description: string;
}

/**
 * Permission raw object
 *
 * @export
 * @interface IPermission
 */
export interface IPermission {
  id: number;
  name: string;
  description: string;
  guard_name: 'api';
  categories: IPermissionCategory[];
}

/**
 * PermissionSpecs
 *
 * @export
 * @interface IPermissionSpecs
 */
export interface IPermissionSpecs {
  name: string;
  guard_name?: string;
  description?: string;
}

/**
 * Permission endpoint methods
 *
 * @export
 * @interface IPermissionEndpoint
 */
export interface IPermissionEndpoint {
  list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IPermission>>>;
  create(specs: IPermissionSpecs): Observable<AxiosResponse<IContainer<IPermission>>>;
  get(id: number): Observable<AxiosResponse<IContainer<IPermission>>>;
  update(id: number, specs: IPermissionSpecs): Observable<AxiosResponse<IContainer<IPermission>>>;
  delete(id: number): Observable<AxiosResponse<IContainer<void>>>;
}

/**
 * Plugin raw object
 *
 * @export
 * @interface IPlugin
 */
export interface IPlugin {
  id: number;
  name: string;
  description: string;
  key: string;
  enabled: boolean;
}

/**
 * Plugin endpoint methods
 *
 * @export
 * @interface IPluginEndpoint
 */
export interface IPluginEndpoint {
  list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IPlugin>>>;
  get(id: number): Observable<AxiosResponse<IContainer<IPlugin>>>;
}


/**
 * Category raw object
 *
 * @export
 * @interface ICategory
 */
export interface ICategory {
  id: number;
  name: string;
  description: string;
  slug: string;
  group_plugin_id?: number;
  parent_id?: number;
  has_children?: boolean;
  attributes?: IAttributeValues;
}

/**
 * Category specs
 *
 * @export
 * @interface ICategorySpecs
 */
export interface ICategorySpecs extends ILangParam {
  name: string;
  description?: string;
  slug?: string;
  categories?: string;
}

/**
 * Category get() params
 *
 * @export
 * @interface ICategoryGetParams
 */
export interface ICategoryGetParams extends ILangParam {
  with_attributes?: 0 | 1;
}

/**
 * Category list() params
 *
 * @export
 * @interface ICategoryListParams
 */
export interface ICategoryListParams extends ICollectionParams, ICategoryGetParams {
  group_plugin_id?: number;
}


/**
 * Option raw object
 *
 * @export
 * @interface IOption
 */
export interface IOption {
  id: number;
  name: string;
  value: string;
  sort_order: number;
}

/**
 * Option specs
 *
 * @export
 * @interface IOptionSpecs
 */
export interface IOptionSpecs {
  name: string;
  value: string;
  // TODO: how to reorder?
  // order: number;
}

/**
 * AttributeType raw object
 *
 * @export
 * @type IAttributeType
 */
export type IAttributeType = 'varchar' | 'integer' | 'float' | 'options' | 'image' | 'boolean' | 'text' | 'datetime';

/**
 * Attribute raw object
 *
 * @export
 * @interface IAttribute
 */
export interface IAttribute {
  id: number;
  name: string;
  description: string;
  slug: string;
  type: IAttributeType;
  is_collection: boolean;
  sort_order: number;
  enabled: 0 | 1;
  hidden: 0 | 1;
  categories: ICategory[];
  options: IOption[];
}

/**
 * Attribute specs
 *
 * @export
 * @interface IAttributeSpecs
 */
export interface IAttributeSpecs extends ILangParam {
  name?: string;
  description?: string;
  type?: IAttributeType;
  is_collection?: 0 | 1;
  sort_order?: number;
  enabled?: 0 | 1;
  hidden?: 0 | 1;
  categories?: string;
  options?: IOptionSpecs[];
}

/**
 * Attribute get params
 *
 * @export
 * @interface IAttributeGetParams
 */
export interface IAttributeGetParams extends ILangParam {

}

/**
 * Attribute list params
 *
 * @export
 * @interface IAttributeListParams
 */
export interface IAttributeListParams extends ICollectionParams, IAttributeGetParams {

}


export {
  IGroup,
  IGroupSpecs,
  IGroupGetParams,
  IGroupListParams,
} from '../group';


export {
  IGroupPlugin,
  IGroupPluginGetParams,
  IGroupPluginListParams,
  IGroupPluginSpecs,
  IGroupPluginUpdateSpecs,
} from '../groupPlugin';


/**
 * DestinationLike object
 *
 * @export
 * @interface IDestinationLike
 */
export interface IDestinationLike {
  id: number;
  name: string;
  description: string;
  component: string;
  group_plugin_id: number;
  icon: string;
  enabled: boolean;
  promoted: boolean;
  order: number;
  active_from: string;
  active_till: string;
  is_public: boolean;
}

/**
 * Destination raw object
 *
 * @export
 * @interface IDestinationRaw
 */
export interface IDestinationRaw extends IDestinationLike {
  params: string;
}

/**
 * Destination object
 *
 * @export
 * @interface IDestination
 */
export interface IDestination extends IDestinationLike {
  params: any;
}

/**
 * Destination specs
 *
 * @export
 * @interface IDestinationSpecs
 */
export interface IDestinationSpecs {
  id?: number;
  name?: string;
  description?: string;
  component?: string;
  params?: string;
  group_plugin_id?: number;
  icon?: string;
  enabled?: 0 | 1;
  promoted?: 0 | 1;
  sort_order?: number;
  active_from?: string;
  active_till?: string;
  is_public?: 0 | 1;
}


export {
  IMenu,
  IMenuGetParams,
  IMenuListParams,
  IMenuSpecs,
} from '../menu';


export {
  IUserAttributeListRelatedParams,
  IUserAttributeNextParams,
} from '../userAttribute';


export {
  ITagsParams,
} from '../tag';



/**
 * Tag
 *
 * @export
 * @interface ITag
 */
export interface ITag {
  id: number;
  name: string;
  description: string;
  slug: string;
  hide_name: boolean;
  hide_description: boolean;
  order_column: number;
  group_plugin_id: number;
  group_id: number;
  type: null;
}


export {
  IArticle,
  IArticleGetParams,
  IArticleListParams,
  IArticleSpecs,
} from '../article';


/**
 * Deposit raw object
 *
 * @export
 * @interface IDeposit
 */
export interface IDeposit {
  id: number;
  name: string;
  group_plugin_id: number;
  deposit_type: IDepositType;
  customer?: IUser;
  created_at: string;
  updated_at: string;
  expires_at: string;
  disabled: 0 | 1;
}

/**
 * Deposit list params
 *
 * @export
 * @interface IDepositListParams
 */
export interface IDepositListParams extends ICollectionParams {
  group_plugin_id?: number;
  categories?: string;
  customer_id?: number;
  with_customer?: 0 | 1;
  with_attributes?: 0 | 1;
}

/**
 * AuthDeposit list params
 *
 * @export
 * @interface IAuthDepositListParams
 */
export interface IAuthDepositListParams extends ICollectionParams {
  group?: number;
  group_plugin_id?: number;
  deposit_types?: string;
  with_customer?: 0 | 1;
}

/**
 * Deposit specs
 *
 * @export
 * @interface IDepositSpecs
 */
export interface IDepositSpecs {
  name?: string;
  slug?: string;
  group_plugin_id: number;
  depository_id: number;
  customer_id: number;
  expires_at?: string;
}

/**
 * Deposit update specs
 *
 * @export
 * @interface IDepositUpdateSpecs
 */
export interface IDepositUpdateSpecs {
  name?: string;
  expires_at?: string;
  disabled?: 0 | 1;
}

/**
 * Deposit endpoint methods
 *
 * @export
 * @interface IDepositEndpoint
 */
export interface IDepositEndpoint {
  authList(params?: IAuthDepositListParams): Observable<AxiosResponse<ICollection<IDeposit>>>;
  list(group_id: number, params?: IDepositListParams): Observable<AxiosResponse<ICollection<IDeposit>>>;
  create(group_id: number, specs: IDepositSpecs): Observable<AxiosResponse<IContainer<IDeposit>>>;
  update(group_id: number, id: number, specs: IDepositUpdateSpecs): Observable<AxiosResponse<IContainer<IDeposit>>>;
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}


/**
 * DepositType raw object
 *
 * @export
 * @interface IDepositType
 */
export interface IDepositType {
  id: number;
  name: string;
  singular: string;
  plural: string;
  icon: string;
  color: string;
  group_plugin_id: number;
}

/**
 * IDepositType get params
 *
 * @export
 * @interface IDepositTypeGetParams
 */
export interface IDepositTypeGetParams extends ILangParam {

}

/**
 * IDepositType list params
 *
 * @export
 * @interface IDepositTypeListParams
 */
export interface IDepositTypeListParams extends ICollectionParams, IDepositTypeGetParams {
  group_plugin_id?: number;
}

/**
 * DepositType specs
 *
 * @export
 * @interface IDepositTypeSpecs
 */
export interface IDepositTypeSpecs extends IDepositTypeGetParams {
  name: string;
  singular: string;
  plural: string;
  icon: string;
  color: string;
  group_plugin_id: number;
}

/**
 * DepositType specs
 *
 * @export
 * @interface IDepositTypeUpdateSpecs
 */
export interface IDepositTypeUpdateSpecs extends IDepositTypeGetParams {
  name?: string;
  singular?: string;
  plural?: string;
  icon?: string;
  color?: string;
}

/**
 * DepositType endpoint methods
 *
 * @export
 * @interface IDepositTypeEndpoint
 */
export interface IDepositTypeEndpoint {
  list(group_id: number, params?: IDepositTypeListParams): Observable<AxiosResponse<ICollection<IDepositType>>>;
  create(group_id: number, specs: IDepositTypeSpecs): Observable<AxiosResponse<IContainer<IDepositType>>>;
  get(group_id: number, id: number, params?: IDepositTypeGetParams): Observable<AxiosResponse<IContainer<IDepositType>>>;
  update(group_id: number, id: number, specs: IDepositTypeSpecs): Observable<AxiosResponse<IContainer<IDepositType>>>;
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}


export {
  ITicketTemplate,
  ITicketTemplateGetParams,
  ITicketTemplateListParams,
  ITicketTemplateSpecs,
  ITicketTemplateUpdateSpecs,
} from '../ticketTemplate';


/**
 * AuthTicket list params
 *
 * @export
 * @interface IAuthTicketListParams
 */
export interface IAuthTicketListParams extends ICollectionParams {
  group_plugin_id?: number;
  ticket_template_id?: number;
  sort?: string;
}

/**
 * Ticket list params
 *
 * @export
 * @interface ITicketListParams
 */
export interface ITicketListParams extends ICollectionParams {
  group_plugin_id?: number;
  ticket_template_id?: number;
  redeemed?: 0 | 1;
  search?: string;
  sort?: string;
}

/**
 * Ticket get params
 *
 * @export
 * @interface ITicketGetParams
 */
export interface ITicketGetParams {
  group_plugin_id?: number;
  ticket_template_id?: number;
}

/**
 * Ticket check() params
 *
 * @export
 * @interface ITicketCheckParams
 */
export interface ITicketCheckParams {
  code: string;
  ticket_template_id?: number;
}

/**
 * Ticket raw object
 *
 * @export
 * @interface ITicket
 */
export interface ITicket {
  id: number;
  code: string;
  redeemed: boolean;
  security_token: string;
  user: IUserStub;
  // template: ITicketTemplate;
  created_at: string;
  expires_at: string;
  expired: boolean;
  redeemed_at: string;
  valid: boolean;
  user_info_text: string;
}

/**
 * Ticket specs
 *
 * @export
 * @interface ITicketSpecs
 */
export interface ITicketSpecs {
  group_plugin_id: number;
  ticket_template_id: number;
  user_id: number;
}

/**
 * Ticket endpoint methods
 *
 * @export
 * @interface ITicketEndpoint
 */
export interface ITicketEndpoint {
  authList(params?: IAuthTicketListParams): Observable<AxiosResponse<ICollection<ITicket>>>;

  list(group_id: number, params?: ITicketListParams): Observable<AxiosResponse<ICollection<ITicket>>>;
  create(group_id: number, specs: ITicketSpecs): Observable<AxiosResponse<IContainer<ITicket>>>;

  check(group_id: number, params: ITicketCheckParams): Observable<AxiosResponse<IContainer<ITicket>>>;
  checkin(group_id: number, params: ITicketCheckParams): Observable<AxiosResponse<IContainer<ITicket>>>;

  get(group_id: number, id: number, params?: ITicketGetParams): Observable<AxiosResponse<IContainer<ITicket>>>;
  checkout(group_id: number, id: number, params?: ITicketGetParams): Observable<AxiosResponse<IContainer<ITicket>>>;
  invalidate(group_id: number, id: number, params?: ITicketGetParams): Observable<AxiosResponse<IContainer<ITicket>>>;
  delete_pdf(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}


export {
  IPhysicalToken,
} from '../physicalToken';


/**
 * Specs to API constructor
 *
 * @export
 * @interface IAPISpecs
 */
export interface IAPISpecs {
  headers?: { [key: string]: string };
  host?: string;
  prefix?: string;
  protocol?: 'http' | 'https';
  timeout?: number;
  invalidResponse?: Error;
}

/**
 * Error interface
 *
 * @export
 * @interface IError
 * @template C
 */
export interface IError {
  status: string;
  error: {
    message: string;
    status_code: number;
    error_code: number;
    required_permission?: string;
  };
}

/**
 * Pagination interface
 *
 * @export
 * @interface IPagination
 */
export interface IPagination {
  total: number;
  count: number;
  per_page: number;
  current_page: number;
  total_pages: number;
}

/**
 * Container interface [used in paginated responses]
 *
 * @export
 * @interface IContainer
 * @template C
 */
export interface IContainer<C> {
  status: string;
  data: C;
}

/**
 * Collection interface [used in paginated responses]
 *
 * @export
 * @interface ICollection
 * @template C
 */
export interface ICollection<C> {
  status: string;
  data: C[];
  pagination?: IPagination;
}

/**
 * Collection request default Params
 *
 * @export
 * @interface ICollectionParams
 */
export interface ICollectionParams {
  page?: number;
  per_page?: number;
  [key: string]: any;
}

/**
 * Translatable request param
 *
 * @export
 * @interface ILangParam
 */
export interface ILangParam {
  lang?: string;
}

/**
 * NumbersMap interface
 *
 * @export
 * @interface IStringsMap
 */
export interface IStringsMap {
  [key: string]: number | string;
}
