import {
  IAttributeValues,
  IUserStub,
  IUserLike,
  IAuthUser,
  IAuthIsBought,
  IUser,
  IPushToken,
  IMessageType,
  IMessageLike,
  IPaymentMethod,
  IPaymentIntent,
  IStripeDeposit,
  IMediaLike,
  IImageType,
  IMediaImage,
  IRole,
  IPermissionCategory,
  IPermission,
  IPlugin,
  ICategory,
  IOption,
  IAttributeType,
  IAttribute,
  IGroup,
  IDestination,
  IDepositType,
  IDeposit,
  ITicket,
  ICollection,
  IPagination,
  ITag,
} from './interfaces';

/**
 * Guard of string[];
 *
 * @export
 * @param {*} data
 * @returns {data is string[]}
 */
export function isArrayOfString(data): data is string[] {
  if (!Array.isArray(data)) return false;
  return data.every((value) => typeof value === 'string');
}

/**
 * Guard of number[];
 *
 * @export
 * @param {*} data
 * @returns {data is number[]}
 */
export function isArrayOfNumber(data): data is number[] {
  if (!Array.isArray(data)) return false;
  return data.every((elem) => elem.typeof !== 'number');
}

/**
 * Guard of IAttributeValues.
 *
 * @export
 * @param {*} data
 * @returns {data is IAttributeValues}
 */
export function isAttributeValues(data): data is IAttributeValues {
  if (data === null || typeof data !== 'object') {
    console.error('IAttributeValues must be an object', data);
    return false;
  }
  var allValid = true;
  Object.keys(data).forEach(
    (key) => {
      if (typeof key !== 'string') {
        console.error('IAttributeValues key must be a string', key);
        allValid = false;
        return;
      }
    }
  );
  if (!allValid) {
    return false;
  }
  return true;
}

// start isUserStub
/**
 * Guard of IUserStub.
 *
 * @export
 * @param {*} data
 * @returns {data is IUserStub}
 */
export function isUserStub(data): data is IUserStub {
  if (data === null || typeof data !== 'object') {
    console.error('IUserStub must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IUserStub.id must be a number', data);
    return false;
  }
  if (typeof data.email !== 'string') {
    console.error('IUserStub.email must be a string', data);
    return false;
  }
  if (typeof data.first_name !== 'string') {
    console.error('IUserStub.first_name must be a string', data);
    return false;
  }
  if (typeof data.last_name !== 'string') {
    console.error('IUserStub.last_name must be a string', data);
    return false;
  }
  if (typeof data.phone_code !== 'string' && typeof data.phone_code !== 'number' && data.phone_code !== null) {
    console.error('IUserStub.phone_code must be a string or number or NULL', data);
    return false;
  }
  if (typeof data.phone !== 'string' && data.phone !== null) {
    console.error('IUserStub.phone must be a string or NULL', data);
    return false;
  }
  if (!isArrayOfString(data.lang_pref)) {
    console.error('IUserStub.lang_pref must be an array of strings', data);
    return false;
  }
  return true;
}
// end isUserStub

// start isUserLike
/**
 * Guard of IUserLike.
 *
 * @export
 * @param {*} data
 * @returns {data is IUserLike}
 */
export function isUserLike(data): data is IUserLike {
  if (!isUserStub(data as IUserLike)) {
    console.error('IUserLike must be an IUserStub', data);
    return false;
  }
  if (typeof data.terms_accepted_at !== 'string' && data.terms_accepted_at !== null) {
    console.error('IUserLike.terms_accepted_at must be a string or NULL', data);
    return false;
  }
  if (typeof data.timezone !== 'string' && data.timezone !== null) {
    console.error('IUserLike.timezone must be a string or NULL', data);
    return false;
  }
  if (data.attributes && !isAttributeValues(data.attributes)) {
    return false;
  }
  return true;
}
// end isUserLike

/**
 * Guard of IAuthUser.
 *
 * @export
 * @param {*} data
 * @returns {data is IAuthUser}
 */
export function isAuthUser(data): data is IAuthUser {
  if (!isUserLike(data as IAuthUser)) {
    console.error('IAuthUser must be an IUserLike', data);
    return false;
  }
  if (data.token && typeof data.token !== 'string') {
    console.error('IAuthUser.token must be absent or a string', data);
    return false;
  }
  if (!isArrayOfNumber(data.groups)) {
    console.error('IAuthUser.groups must be an array of numbers', data);
    return false;
  }
  return true;
}

/**
 * Guard of IUser.
 *
 * @export
 * @param {*} data
 * @returns {data is IUser}
 */
export function isUser(data): data is IUser {
  if (!isUserLike(data as IUser)) {
    console.error('IUser must be an IUserLike', data);
    return false;
  }
  if (!Array.isArray(data.roles)) {
    console.error('IUser.roles must be an array', data);
    return false;
  }
  if (!data.roles.every(isRole)) return false;
  return true;
}
// end isUser

// start isAuthPaid
/**
 * Guard of IAuthIsBought.
 *
 * @export
 * @param {*} data
 * @returns {data is IAuthIsBought}
 */
export function isAuthPaid(data): data is IAuthIsBought {
  // console.log('isAuthPaid(data)', data);
  if (data === null || typeof data !== 'object') {
    console.error('IAuthIsBought must be an object', data);
    return false;
  }
  if (typeof data.is_bought !== 'boolean') {
    console.error('IAuthIsBought.is_bought must be a boolean', data);
    return false;
  }
  return true;
}
// end isAuthPaid

/**
 * Guard of IPushToken.
 *
 * @export
 * @param {*} data
 * @returns {data is IPushToken}
 */
export function isPushToken(data): data is IPushToken {
  // console.log('isPushToken(data)', data);
  if (data === null || typeof data !== 'object') {
    console.error('IPushToken must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IPushToken.id must be a number', data);
    return false;
  }
  if (typeof data.push_token !== 'string') {
    console.error('IPushToken.push_token must be a string', data);
    return false;
  }
  if (typeof data.device_uid !== 'string') {
    console.error('IPushToken.device_uid must be a string', data);
    return false;
  }
  if (data.platform !== 'android' && data.platform !== 'ios') {
    console.error('IPushToken.platform must be "android" or "ios"', data);
    return false;
  }
  if (typeof data.app_version !== 'string') {
    console.error('IPushToken.app_version must be a string', data);
    return false;
  }
  if (typeof data.active !== 'boolean') {
    console.error('IPushToken.active must be a boolean', data);
    return false;
  }
  if (typeof data.broken !== 'boolean') {
    console.error('IPushToken.broken must be a boolean', data);
    return false;
  }
  return true;
}
// end isPushToken

// start isMessageType
/**
 * Guard of IMessageType.
 *
 * @export
 * @param {*} data
 * @returns {data is IMessageType}
 */
export function isMessageType(data): data is IMessageType {
  const types = ['email', 'push', 'sms'];
  if (types.indexOf(data) < 0) {
    console.error('IMessageType must be one of ' + types.join(', '), data);
    return false;
  }
  return true;
}
// end isMessageType

/**
 * Guard of IMessageLike.
 *
 * @export
 * @param {*} data
 * @returns {data is IMessageLike}
 */
export function isMessageLike(data): data is IMessageLike {
  if (data === null || typeof data !== 'object') {
    console.error('IMessageLike must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IMessageLike.id must be a number', data);
    return false;
  }
  if (typeof data.subject !== 'string' && data.subject !== null) {
    console.error('IMessageLike.subject must be a string or NULL', data);
    return false;
  }
  if (typeof data.body !== 'string') {
    console.error('IMessageLike.body must be a string', data);
    return false;
  }
  if (typeof data.plain_body !== 'string') {
    console.error('IMessageLike.plain_body must be a string', data);
    return false;
  }
  if (!isMessageType(data.type)) return false;

  // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
  if (typeof data.last_read !== 'string' && data.last_read !== null) {
    console.error('IMessageLike.last_read must be a string or NULL', data);
    return false;
  }
  if (!isAttributeValues(data.template_attributes)) {
    return false;
  }
  if (!Array.isArray(data.template_destinations)) {
    console.error('IMessageLike.template_destinations must be an array', data);
    return false;
  }
  if (!data.template_destinations.every(isDestination)) return false;
  // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
  if (typeof data.created_at !== 'string') {
    console.error('IMessageLike.created_at must be a string', data);
    return false;
  }
  return true;
}
// end isMessageLike

// start isPaymentMethod
/**
 * Guard of IPaymentMethod.
 *
 * @export
 * @param {*} data
 * @returns {data is IPaymentMethod}
 */
export function isPaymentMethod(data): data is IPaymentMethod {
  if (data === null || typeof data !== 'object') {
    console.error('IPaymentMethod must be an object', data);
    return false;
  }
  if (typeof data.id !== 'string') {
    console.error('IPaymentMethod.id must be a string', data);
    return false;
  }
  if (data.card === null || typeof data.card !== 'object') {
    console.error('IPaymentMethod.card must be an object', data);
    return false;
  }
  if (data.type !== 'card') {
    console.error('IPaymentMethod.type must be "card"', data);
    return false;
  }
  return true;
}
// end isPaymentMethod

// start isPaymentIntent
/**
 * Guard of IPaymentMethod.
 *
 * @export
 * @param {*} data
 * @returns {data is IPaymentIntent}
 */
export function isPaymentIntent(data): data is IPaymentIntent {
  if (data === null || typeof data !== 'object') {
    console.error('IPaymentIntent must be an object', data);
    return false;
  }
  if (typeof data.id !== 'string') {
    console.error('IPaymentIntent.id must be a string', data);
    return false;
  }
  if (typeof data.client_secret !== 'string') {
    console.error('IPaymentIntent.client_secret must be a string', data);
    return false;
  }
  return true;
}
// end isPaymentIntent

// start isStripeDeposit
/**
 * Guard of IStripeDeposit.
 *
 * @export
 * @param {*} data
 * @returns {data is IStripeDeposit}
 */
export function isStripeDeposit(data): data is IStripeDeposit {
  if (data === null || typeof data !== 'object') {
    console.error('IStripeDeposit must be an object', data);
    return false;
  }
  if (typeof data.transaction !== 'string') {
    console.error('IStripeDeposit.transaction must be a string', data);
    return false;
  }
  return true;
}
// end isStripeDeposit

// start isMediaLike
/**
 * Guard of IMediaLike.
 *
 * @export
 * @param {*} data
 * @returns {data is IMediaLike}
 */
export function isMediaLike(data): data is IMediaLike {
  if (data === null || typeof data !== 'object') {
    console.error('IMediaLike must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IMediaLike.id must be a number', data);
    return false;
  }
  if (typeof data.filename !== 'string') {
    console.error('IMediaLike.filename must be a string', data);
    return false;
  }
  if (typeof data.size !== 'number') {
    console.error('IMediaLike.size must be a number', data);
    return false;
  }
  if (typeof data.mime_type === 'undefined') {
    console.error('IMediaLike.mime_type must be defined', data);
    return false;
  }
  if (typeof data.name !== 'string' && data.name !== null) {
    console.error('IMediaLike.name must be a string or NULL', data);
    return false;
  }
  if (typeof data.description !== 'string' && data.description !== null) {
    console.error('IMediaLike.description must be a string or NULL', data);
    return false;
  }
  if (typeof data.url !== 'string') {
    console.error('IMediaLike.url must be a string', data);
    return false;
  }
  return true;
}
// end IMediaLike

// start isImageType
/**
 * Guard of IImageType.
 *
 * @export
 * @param {*} data
 * @returns {data is IImageType}
 */
export function isImageType(data): data is IImageType {
  const types = ['image/jpeg', 'image/png', 'image/gif'];
  if (types.indexOf(data) < 0) {
    console.error('IImageType must be one of ' + types.join(', '), data);
    return false;
  }
  return true;
}
// end isImageType

// start isMediaImage
/**
 * Guard of IMediaImage.
 *
 * @export
 * @param {*} data
 * @returns {data is IMediaImage}
 */
export function isMediaImage(data): data is IMediaImage {
  if (!isImageType(data.mime_type)) {
    return false;
  }
  if (data.presets === null || typeof data.presets !== 'object') {
    console.error('IMediaImage.presets must be an object', data);
    return false;
  }
  return true;
}
// end isMediaImage

// start isRole
/**
 * Guard of IRole.
 *
 * @export
 * @param {*} data
 * @returns {data is IRole}
 */
export function isRole(data): data is IRole {
  if (data === null || typeof data !== 'object') {
    console.error('IRole must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IRole.id must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('IRole.name must be a string', data);
    return false;
  }
  if (typeof data.description !== 'string') {
    console.error('IRole.description must be a string', data);
    return false;
  }
  if (typeof data.guard_name !== 'string') {
    console.error('IRole.guard_name must be a string', data);
    return false;
  }
  if (!isArrayOfString(data.permissions)) {
    console.error('IRole.permissions must be an array of strings', data);
    return false;
  }
  return true;
}
// end isRole

/**
 * Guard of IPermissionCategory.
 *
 * @export
 * @param {*} data
 * @returns {data is IPermissionCategory}
 */
export function isPermissionCategory(data): data is IPermissionCategory {
  // console.log('isPermissionCategory(data)', data);
  if (data === null || typeof data !== 'object') {
    console.error('IPermissionCategory must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IPermissionCategory.id must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('IPermissionCategory.name must be a string', data);
    return false;
  }
  if (typeof data.slug !== 'string') {
    console.error('IPermissionCategory.slug must be a string', data);
    return false;
  }
  if (typeof data.description !== 'string' && data.description !== null) {
    console.error('IPermissionCategory.description must be a string or NULL', data);
    return false;
  }
  return true;
}

/**
 * Guard of IPermission.
 *
 * @export
 * @param {*} data
 * @returns {data is IPermission}
 */
export function isPermission(data): data is IPermission {
  // console.log('isPermission(data)', data);
  if (data === null || typeof data !== 'object') {
    console.error('IPermission must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IPermission.id must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('IPermission.name must be a string', data);
    return false;
  }
  if (typeof data.description !== 'string' && data.description !== null) {
    console.error('IPermission.description must be a string or NULL', data);
    return false;
  }
  if (typeof data.guard_name !== 'string') {
    console.error('IPermission.guard_name must be a string', data);
    return false;
  }
  if (!Array.isArray(data.categories)) {
    console.error('IPermission.categories must be an array', data);
    return false;
  }
  if (!data.categories.every(isPermissionCategory)) return false;
  return true;
}

/**
 * Guard of IPlugin.
 *
 * @export
 * @param {*} data
 * @returns {data is IPlugin}
 */
export function isPlugin(data): data is IPlugin {
  if (data === null || typeof data !== 'object') {
    console.error('IPlugin must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IPluginid must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('IPluginname must be a string', data);
    return false;
  }
  if (typeof data.description !== 'string') {
    console.error('IPlugindescription must be a string', data);
    return false;
  }
  if (typeof data.key !== 'string') {
    console.error('IPluginkey must be a string', data);
    return false;
  }
  if (typeof data.enabled !== 'boolean') {
    console.error('IPluginenabled must be a boolean', data);
    return false;
  }
  return true;
}

/**
 * Guard of ICategory.
 *
 * @export
 * @param {*} data
 * @returns {data is ICategory}
 */
export function isCategory(data): data is ICategory {
  if (data === null || typeof data !== 'object') {
    console.error('ICategory must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('ICategory.id must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('ICategory.name must be a string', data);
    return false;
  }
  if (typeof data.description !== 'string' && data.description !== null) {
    console.error('ICategory.description must be a string or NULL', data);
    return false;
  }
  if (typeof data.slug !== 'string') {
    console.error('ICategory.slug must be a string', data);
    return false;
  }
  if (typeof data.group_plugin_id !== 'undefined' && typeof data.group_plugin_id !== 'number' && data.group_plugin_id !== null) {
    console.error('ICategory.category.group_plugin_id, when present, must be a number or NULL', data);
    return false;
  }
  if (typeof data.parent_id !== 'undefined' && typeof data.parent_id !== 'number' && data.parent_id !== null) {
    console.error('ICategory.category.parent_id, when present, must be a number or NULL', data);
    return false;
  }
  if (typeof data.has_children !== 'undefined' && typeof data.has_children !== 'boolean') {
    console.error('ICategory.category.has_children, when present, must be a boolean', data);
    return false;
  }
  if (data.attributes && !isAttributeValues(data.attributes)) {
    return false;
  }
  return true;
}

/**
 * Guard of IOption.
 *
 * @export
 * @param {*} data
 * @returns {data is IOption}
 */
export function isOption(data): data is IOption {
  if (data === null || typeof data !== 'object') {
    console.error('IOption must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IOption.id must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('IOption.name must be a string', data);
    return false;
  }
  if (typeof data.value !== 'string') {
    console.error('IOption.value must be a string', data);
    return false;
  }
  if (typeof data.sort_order !== 'number') {
    console.error('IOption.sort_order must be a number', data);
    return false;
  }
  return true;
}

// start isAttributeType
/**
 * Guard of IAttributeType.
 *
 * @export
 * @param {*} data
 * @returns {data is IAttributeType}
 */
export function isAttributeType(data): data is IAttributeType {
  const types = ['varchar', 'integer', 'float', 'options', 'image', 'boolean', 'text', 'datetime'];
  if (types.indexOf(data) < 0) {
    console.error('IAttributeType must be one of ' + types.join(', '), data);
    return false;
  }
  return true;
}
// end isAttributeType

/**
 * Guard of IAttribute.
 *
 * @export
 * @param {*} data
 * @returns {data is IAttribute | void}
 */
export function isAttributeOrVoid(data): data is IAttribute | void {
  // console.log('isAttributeOrVoid(data)', data, typeof data);
  if (data && !data.length) return true;
  return isAttribute(data);
}

/**
 * Guard of IAttribute.
 *
 * @export
 * @param {*} data
 * @returns {data is IAttribute}
 */
export function isAttribute(data): data is IAttribute {
  if (data === null || typeof data !== 'object') {
    console.error('IAttribute must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IAttribute.id must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('IAttribute.name must be a string', data);
    return false;
  }
  if (typeof data.description !== 'string' && data.description !== null) {
    console.error('IAttribute.description must be a string or NULL', data);
    return false;
  }
  if (typeof data.slug !== 'string') {
    console.error('IAttribute.slug must be a string', data);
    return false;
  }
  if (!isAttributeType(data.type)) {
    return false;
  }
  if (typeof data.is_collection !== 'boolean') {
    console.error('IAttribute.is_collection must be a boolean', data);
    return false;
  }
  if (typeof data.sort_order !== 'number') {
    console.error('IAttribute.sort_order must be a number', data);
    return false;
  }
  if (data.enabled !== 0 && data.enabled !== 1) {
    console.error('IAttribute.enabled must be 0 or 1', data);
    return false;
  }
  if (data.hidden !== 0 && data.hidden !== 1) {
    console.error('IAttribute.hidden must be 0 or 1', data);
    return false;
  }
  if (!Array.isArray(data.categories)) {
    console.error('IAttribute.categories must be an array', data);
    return false;
  }
  if (!data.categories.every(isCategory)) return false;
  if (!Array.isArray(data.options)) {
    console.error('IAttribute.options must be an array', data);
    return false;
  }
  if (data.type == 'options' && !data.options.every(isOption)) return false;
  return true;
}

/**
 * Guard of IGroup.
 *
 * @export
 * @param {*} data
 * @returns {data is IGroup}
 */
export function isGroup(data: any): data is IGroup {
  if (data === null || typeof data !== 'object') {
    console.error('IGroup must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IGroup.id must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('IGroup.name must be a string', data);
    return false;
  }
  if (typeof data.domain !== 'string' && data.domain !== null) {
    console.error('IGroup.domain must be a string or NULL', data);
    return false;
  }
  // TODO: Change from if-present to if-not-null when API updated
  if (data.subdomain && typeof data.subdomain !== 'string') {
    console.error('IGroup.subdomain must be a string or NULL', data);
    return false;
  }
  if (typeof data.short_description !== 'string' && data.short_description !== null) {
    console.error('IGroup.short_description must be a string or NULL', data);
    return false;
  }
  if (typeof data.description !== 'string' && data.description !== null) {
    console.error('IGroup.description must be a string or NULL', data);
    return false;
  }
  if (data.published !== 0 && data.published !== 1) {
    console.error('IGroup.published must be 0 or 1', data);
    return false;
  }
  if (data.public !== 0 && data.public !== 1) {
    console.error('IGroup.public must be 0 or 1', data);
    return false;
  }
  if (data.protected !== 0 && data.protected !== 1) {
    console.error('IGroup.protected must be 0 or 1', data);
    return false;
  }
  if (data.attributes && !isAttributeValues(data.attributes)) {
    return false;
  }
  return true;
}

/**
 * Guard of ITag.
 *
 * @export
 * @param {*} data
 * @returns {data is ITag}
 */
export function isTag(data: ITag): data is ITag {
  if (data === null || typeof data !== 'object') {
    console.error('IAttribute must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IAttribute.id must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('IAttribute.name must be a string', data);
    return false;
  }
  if (typeof data.description !== 'string' && data.description !== null) {
    console.error('IAttribute.description must be a string or NULL', data);
    return false;
  }
  if (typeof data.slug !== 'string') {
    console.error('IAttribute.slug must be a string', data);
    return false;
  }
  if (typeof data.hide_name !== 'boolean') {
    console.error('IAttribute.hide_name must be a boolean', data);
    return false;
  }
  if (typeof data.hide_description !== 'boolean') {
    console.error('IAttribute.hide_description must be a boolean', data);
    return false;
  }
  if (typeof data.order_column !== 'number') {
    console.error('IAttribute.order_column must be a number', data);
    return false;
  }
  if (typeof data.group_plugin_id !== 'number') {
    console.error('IAttribute.group_plugin_id must be a number', data);
    return false;
  }
  if (typeof data.group_id !== 'number') {
    console.error('IAttribute.group_id must be a number', data);
    return false;
  }
  return true;
}

/**
 * Guard of IDestination.
 *
 * @export
 * @param {*} data
 * @returns {data is IDestination}
 */
export function isDestination(data): data is IDestination {
  if (data === null || typeof data !== 'object') {
    console.error('IDestination must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IDestination.id must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('IDestination.name must be a string', data);
    return false;
  }
  if (typeof data.component !== 'string') {
    console.error('IDestination.component must be a string', data);
    return false;
  }
  if (typeof data.params === 'undefined') {
    console.error('IDestination.params must be present', data);
    return false;
  }
  if (typeof data.group_plugin_id !== 'number' && data.group_plugin_id !== null) {
    console.error('IDestination.group_plugin_id must be a number or NULL', data);
    return false;
  }
  if (typeof data.icon !== 'string' && data.icon !== null) {
    console.error('IDestination.icon must be a string or NULL', data);
    return false;
  }
  if (typeof data.enabled !== 'boolean') {
    console.error('IDestination.enabled must be a boolean', data);
    return false;
  }
  if (typeof data.order !== 'number') {
    console.error('IDestination.order must be a number', data);
    return false;
  }
  return true;
}
// end isDestination

// start isDepositType
/**
 * Guard of IDepositType.
 *
 * @export
 * @param {*} data
 * @returns {data is IDepositType}
 */
export function isDepositType(data): data is IDepositType {
  if (data === null || typeof data !== 'object') {
    console.error('IDepositType must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IDepositType.id must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('IDepositType.name must be a string', data);
    return false;
  }
  if (typeof data.singular !== 'string') {
    console.error('IDepositType.singular must be a string', data);
    return false;
  }
  if (typeof data.plural !== 'string') {
    console.error('IDepositType.plural must be a string', data);
    return false;
  }
  if (typeof data.icon !== 'string') {
    console.error('IDepositType.icon must be a string', data);
    return false;
  }
  if (typeof data.color !== 'string') {
    console.error('IDepositType.color must be a string', data);
    return false;
  }
  if (typeof data.group_plugin_id !== 'number') {
    console.error('IDepositType.group_plugin_id must be a number', data);
    return false;
  }
  return true;
}
// end isDepositType

// start isDeposit
/**
 * Guard of IDeposit.
 *
 * @export
 * @param {*} data
 * @returns {data is IDeposit}
 */
export function isDeposit(data): data is IDeposit {
  if (data === null || typeof data !== 'object') {
    console.error('IDeposit must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IDeposit.id must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('IDeposit.name must be a string', data);
    return false;
  }
  if (typeof data.slug !== 'string') {
    console.error('IDeposit.slug must be a string', data);
    return false;
  }
  if (typeof data.group_plugin_id !== 'number') {
    console.error('IDeposit.group_plugin_id must be a number', data);
    return false;
  }
  if (!isDepositType(data.deposit_type)) {
    return false;
  }
  if (typeof data.customer !== 'undefined' && !isUser(data.customer)) {
    return false;
  }
  // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
  if (typeof data.created_at !== 'string') {
    console.error('IDeposit.created_at must be a string or NULL', data);
    return false;
  }
  // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
  if (typeof data.updated_at !== 'string') {
    console.error('IDeposit.updated_at must be a string or NULL', data);
    return false;
  }
  // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
  if (typeof data.expires_at !== 'string' && data.expires_at !== null) {
    console.error('IDeposit.expires_at must be a string or NULL', data);
    return false;
  }
  if (data.disabled !== 0 && data.disabled !== 1) {
    console.error('IDeposit.disabled must be 0 or 1', data);
    return false;
  }
  return true;
}
// end isDeposit

// start isTicket
/**
 * Guard of ITicket.
 *
 * @export
 * @param {*} data
 * @returns {data is ITicket}
 */
export function isTicket(data): data is ITicket {
  if (data === null || typeof data !== 'object') {
    console.error('ITicket must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('ITicket.id must be a number', data);
    return false;
  }
  if (typeof data.code !== 'string') {
    console.error('ITicket.code must be a string', data);
    return false;
  }
  if (typeof data.redeemed !== 'boolean') {
    console.error('ITicket.redeemed must be a boolean', data);
    return false;
  }
  if (typeof data.security_token !== 'string') {
    console.error('ITicket.security_token must be a string', data);
    return false;
  }
  if (!isUserStub(data.user)) {
    return false;
  }
  /*
  if (!isTicketTemplate(data.template)) {
    return false;
  }
  */
  // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
  if (typeof data.created_at !== 'string') {
    console.error('ITicket.created_at must be a string', data);
    return false;
  }
  // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
  if (typeof data.expires_at !== 'string' && data.expires_at !== null) {
    console.error('ITicket.expires_at must be a string or NULL', data);
    return false;
  }
  if (typeof data.expired !== 'boolean') {
    console.error('ITicket.expired must be a boolean', data);
    return false;
  }
  // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
  if (typeof data.redeemed_at !== 'string' && data.redeemed_at !== null) {
    console.error('ITicket.redeemed_at must be a string or NULL', data);
    return false;
  }
  if (typeof data.valid !== 'boolean') {
    console.error('ITicket.valid must be a boolean', data);
    return false;
  }
  return true;
}
// end isTicket

// start isCollection
/**
 * Guard of ICollection<any>
 *
 * @export
 * @param {*} coll
 * @returns {coll is ICollection<any>}
 */
export function isCollection<T>(
  coll,
  guard: (d) => d is T): coll is ICollection<T> {
  // console.log('isCollection(coll, guard)', coll, guard);
  if (coll === null || typeof coll !== 'object') {
    console.error('ICollection must be an object', coll);
    return false;
  }
  if (!Array.isArray(coll.data)) {
    console.error('ICollection.data must be an array', coll);
    return false;
  }
  // TODO: track from where guard is coming undefined
  if (typeof guard === 'function' && !coll.data.every(guard)) return false;

  if (coll.pagination && !isPagination(coll.pagination)) {
    return false;
  }
  return true;
}
// end isCollection

// start isPagination
/**
 * Guard of IPagination.
 *
 * @export
 * @param {*} data
 * @returns {data is IPagination}
 */
export function isPagination(data): data is IPagination {
  if (data === null || typeof data !== 'object') {
    console.error('IPagination must be an object', data);
    return false;
  }
  if (typeof data.total !== 'number') {
    console.error('IPagination.total must be a number', data);
    return false;
  }
  if (typeof data.count !== 'number') {
    console.error('IPagination.count must be a number', data);
    return false;
  }
  if (typeof data.per_page !== 'number') {
    console.error('IPagination.per_page must be a number', data);
    return false;
  }
  if (typeof data.current_page !== 'number') {
    console.error('IPagination.current_page must be a number', data);
    return false;
  }
  if (typeof data.total_pages !== 'number') {
    console.error('IPagination.total_pages must be a number', data);
    return false;
  }
  return true;
}
// end isPagination
