import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';

import { SettingEndpoint } from '../setting';
import { AuthEndpoint } from '../auth';
import { PushTokenEndpoint } from '../pushToken';
import { PhysicalTokenEndpoint } from '../physicalToken';
import { AuthMessageEndpoint } from '../authMessage';
import { AuthStripeEndpoint } from '../authStripe';
import { MediaEndpoint } from '../media';
import { UserEndpoint } from '../user';
import { RoleEndpoint } from '../role';
import { PermissionEndpoint } from '../permission';
import { PluginEndpoint } from '../plugin';
import { GroupEndpoint } from '../group';
import { GroupPluginEndpoint } from '../groupPlugin';
import { MenuEndpoint } from '../menu';
import { UserAttributeEndpoint } from '../userAttribute';
import { UserAttributeCategoryEndpoint } from '../userAttributeCategory';
import { ArticleEndpoint } from '../article';
import { DepositEndpoint } from '../deposit';
import { DepositTypeEndpoint } from '../depositType';
import { TicketTemplateEndpoint } from '../ticketTemplate';
import { TicketEndpoint } from '../ticket';

import { IAPISpecs } from './interfaces';
import { TagEndpoint } from '../tag';

/**
 * Generic API client
 *
 * @export
 * @abstract
 * @class API
 */
export default abstract class API {

  /**
   * Setting endpoint
   *
   * @type {SettingEndpoint}
   * @memberof SuperappEngine
   */
  Setting: SettingEndpoint;

  /**
   * Auth endpoint
   *
   * @type {AuthEndpoint}
   * @memberof SuperappEngine
   */
  Auth: AuthEndpoint;

  /**
   * PushToken endpoint
   *
   * @type {PushTokenEndpoint}
   * @memberof SuperappEngine
   */
  PushToken: PushTokenEndpoint;

  /**
   * PhysicalToken endpoint
   *
   * @type {PhysicalTokenEndpoint}
   * @memberof SuperappEngine
   */
  PhysicalToken: PhysicalTokenEndpoint;

  /**
   * AuthMessage endpoint
   *
   * @type {AuthMessageEndpoint}
   * @memberof SuperappEngine
   */
  AuthMessage: AuthMessageEndpoint;

  /**
   * AuthStripe endpoint
   *
   * @type {AuthStripeEndpoint}
   * @memberof SuperappEngine
   */
  AuthStripe: AuthStripeEndpoint;

  /**
   * Media endpoint
   *
   * @type {MediaEndpoint}
   * @memberof SuperappEngine
   */
  Media: MediaEndpoint;

  /**
   * User endpoint
   *
   * @type {UserEndpoint}
   * @memberof SuperappEngine
   */
  User: UserEndpoint;

  /**
   * Role endpoint
   *
   * @type {RoleEndpoint}
   * @memberof SuperappEngine
   */
  Role: RoleEndpoint;

  /**
   * Permission endpoint
   *
   * @type {PermissionEndpoint}
   * @memberof SuperappEngine
   */
  Permission: PermissionEndpoint;

  /**
   * Plugin endpoint
   *
   * @type {PluginEndpoint}
   * @memberof SuperappEngine
   */
  Plugin: PluginEndpoint;

  /**
   * Group endpoint
   *
   * @type {GroupEndpoint}
   * @memberof SuperappEngine
   */
  Group: GroupEndpoint;

  /**
   * GroupPlugin endpoint
   *
   * @type {GroupPluginEndpoint}
   * @memberof SuperappEngine
   */
  GroupPlugin: GroupPluginEndpoint;

  /**
   * Menu endpoint
   *
   * @type {MenuEndpoint}
   * @memberof SuperappEngine
   */
  Menu: MenuEndpoint;

  /**
   * Tag endpoint
   *
   * @type {TagEndpoint}
   * @memberof SuperappEngine
   */
  Tag: TagEndpoint;

  /**
   * UserAttribute endpoint
   *
   * @type {UserAttributeEndpoint}
   * @memberof SuperappEngine
   */
  UserAttribute: UserAttributeEndpoint;

  /**
   * UserAttributeCategory endpoint
   *
   * @type {UserAttributeCategoryEndpoint}
   * @memberof SuperappEngine
   */
  UserAttributeCategory: UserAttributeCategoryEndpoint;

  /**
   * Article endpoint
   *
   * @type {ArticleEndpoint}
   * @memberof SuperappEngine
   */
  Article: ArticleEndpoint;

  /**
   * Deposit endpoint
   *
   * @type {DepositEndpoint}
   * @memberof SuperappEngine
   */
  Deposit: DepositEndpoint;

  /**
   * DepositType endpoint
   *
   * @type {DepositTypeEndpoint}
   * @memberof SuperappEngine
   */
  DepositType: DepositTypeEndpoint;

  /**
   * TicketTemplate endpoint
   *
   * @type {TicketTemplateEndpoint}
   * @memberof SuperappEngine
   */
  TicketTemplate: TicketTemplateEndpoint;

  /**
   * Ticket endpoint
   *
   * @type {TicketEndpoint}
   * @memberof SuperappEngine
   */
  Ticket: TicketEndpoint;

  /**
   * Default invalid response throwable error
   *
   * @public
   * @type {Error}
   * @memberof API
   */
  public invalidResponse: Error;

  /**
   * request timeout
   *
   * @public
   * @type {number}
   * @memberof API
   */
  public timeout: number;

  /**
   * default request headers
   *
   * @protected
   * @type {{ [key: string]: string }}
   * @memberof API
   */
  protected headers: { [key: string]: string };

  /**
   * host $protocol://$host$prefix
   *
   * @protected
   * @type {string}
   * @memberof API
   */
  protected host: string;

  /**
   * axios client
   *
   * @public
   * @type {AxiosInstance}
   * @memberof API
   */
  public http: AxiosInstance;

  /**
   * prefix $protocol://$host$prefix
   *
   * @protected
   * @type {string}
   * @memberof API
   */
  protected prefix: string;

  /**
   * protocol $protocol://$host$prefix
   *
   * @protected
   * @type {('http'|'https')}
   * @memberof API
   */
  protected protocol: 'http' | 'https';

  /**
   *  $protocol://$host$prefix
   *
   * @readonly
   * @private
   * @type {string}
   * @memberof API
   */
  private get baseUrl(): string {
    // console.log('get baseUrl()');
    return `${this.protocol}://${this.host}${this.prefix}`;
  }

  /**
   * add headers
   *
   * @readonly
   * @public
   *
   * @memberof API
   */
  public addHeaders(headers: { [key: string]: string }) {
    // console.log('addHeaders(headers)', headers);
    this.headers = Object.assign({}, this.headers, headers);
    // update axios instance
    this.http.defaults.headers.common = this.headers;
    // console.log('this.http.defaults.headers', this.http.defaults.headers);
  }

  /**
   * axios config object
   *
   * @readonly
   * @private
   * @type {AxiosRequestConfig}
   * @memberof API
   */
  private get axiosConfig(): AxiosRequestConfig {
    // console.log('get axiosConfig()');

    return { baseURL: this.baseUrl, headers: this.headers, timeout: this.timeout };
  }

  /**
   * Creates an instance of API.
   * 
   * @param {IAPISpecs} specs
   *
   * @memberof API
   */
  constructor(specs: IAPISpecs) {
    // console.log('constructor(specs: IAPISpecs)', specs);

    this.headers = specs.headers;
    this.host = specs.host;
    this.invalidResponse = specs.invalidResponse;
    this.prefix = specs.prefix || '/';
    this.protocol = specs.protocol;
    this.timeout = specs.timeout;
    this.http = axios.create(this.axiosConfig);
    this.loadEndpoints();
  }

  /**
   * load all endpoints instances
   *
   * @private
   *
   * @memberof SuperappEngine
   */
  private loadEndpoints(): void {
    // console.log('loadEndpoints()');

    this.Setting = new SettingEndpoint(this);
    this.Auth = new AuthEndpoint(this);
    this.PushToken = new PushTokenEndpoint(this);
    this.PhysicalToken = new PhysicalTokenEndpoint(this);
    this.AuthMessage = new AuthMessageEndpoint(this);
    this.AuthStripe = new AuthStripeEndpoint(this);
    this.Media = new MediaEndpoint(this);
    this.User = new UserEndpoint(this);
    this.Role = new RoleEndpoint(this);
    this.Permission = new PermissionEndpoint(this);
    this.Plugin = new PluginEndpoint(this);
    this.Group = new GroupEndpoint(this);
    this.GroupPlugin = new GroupPluginEndpoint(this);
    this.Menu = new MenuEndpoint(this);
    this.Tag = new TagEndpoint(this);
    this.UserAttribute = new UserAttributeEndpoint(this);
    this.UserAttributeCategory = new UserAttributeCategoryEndpoint(this);
    this.Article = new ArticleEndpoint(this);
    this.Deposit = new DepositEndpoint(this);
    this.DepositType = new DepositTypeEndpoint(this);
    this.TicketTemplate = new TicketTemplateEndpoint(this);
    this.Ticket = new TicketEndpoint(this);
  }

}
