import { AxiosPromise, AxiosResponse } from 'axios';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import API from './api';
import { IContainer, ICollection, ICollectionParams, IStringsMap } from './interfaces';
import { isCollection } from './type.guards';

export type predicate<T> = (value: T, index?: number) => boolean;

/**
 * Endpoint belongs to APIs
 *
 * @abstract
 * @class Endpoint
 * @template API
 */
export default abstract class Endpoint {

  /**
   * reference to API instance
   *
   * @protected
   * @type {API}
   * @memberof Endpoint
   */
  protected readonly api: API;

  /**
   * url prefix
   *
   * @protected
   * @type {string}
   * @memberof Endpoint
   */
  protected readonly prefix: string;

  /**
   * Creates an instance of Endpoint.
   * 
   * @param {API} api
   * @param {string} prefix
   *
   * @memberof Endpoint
   */
  constructor(api: API, prefix: string) {
    // console.log('constructor(api: API, prefix: string)', api, prefix);
    this.api = api;
    this.prefix = prefix;
  }

  /**
   * Default function to get and process pagination responses
   *
   * @protected
   * @template C
   * @param {string} url
   * @param {ICollectionParams} params
   * @param {string} property
   * @param {any} [guard]
   * @returns {Observable<AxiosResponse<ICollection<C>>>}
   * @memberof Endpoint
   */
  protected getCollection<C>(
    url: string,
    params: ICollectionParams,
    guard: (data) => data is C,
  ): Observable<AxiosResponse<ICollection<C>>> {
    // console.log('getCollection(url, params, guard?)', url, params, guard);
    params = this.getCollectionParams(params || {});
    let observable = Observable.from(this.api.http.get<ICollection<C>>(url, { params }));
    return observable.pipe(
      tap(
        (res: AxiosResponse<ICollection<C>>) => {
          // console.log('validate res.data', res);
          if (!isCollection<C>(res.data, guard)) {
            // console.log('throw error');
            throw this.api.invalidResponse;
          }
        }
      ),
    );
  }

  /**
   * Default function to get item
   *
   * @protected
   * @template C
   * @param {string} url
   * @param {IContainerParams} params
   * @param {string} property
   * @param {any} [guard]
   * @returns {Observable<AxiosResponse<IContainer<C>>>}
   * @memberof Endpoint
   */
  protected getContainer<C>(
    promise: AxiosPromise<IContainer<C>>,
    guard?: (data) => data is C,
  ): Observable<AxiosResponse<IContainer<C>>> {
    // console.log('getContainer(url, params, guard?)', url, params, guard);
    let observable = Observable.from(promise);
    return observable.pipe(
      tap(
        (res) => {
          // console.log('validate res.data.data', res);
          if (guard && !guard(res.data.data)) {
            // console.log('throw error');
            throw this.api.invalidResponse;
          }
        }
      ),
    );
  }

  /**
   * Get object with collection params
   *
   * @private
   * @param {ICollectionParams} params
   * @returns {ICollectionParams}
   *
   * @memberof Endpoint
   */
  private getCollectionParams(params: ICollectionParams): ICollectionParams {
    // console.log('getCollectionParams(params: ICollectionParams)', params);
    params.per_page = params.per_page || 15;
    params.page = params.page || 1;
    return params;
  }

  /**
   * Replace segments in URL
   *
   * @protected
   * @param {IStringsMap} values
   * @returns {string}
   *
   * @memberof Endpoint
   */
  protected replacePrefixSegments(values: IStringsMap): string {
    // console.log('replacePrefixSegments(values)', values);
    var parts = this.prefix.split('/');
    var segments = [];
    parts.forEach(part => {
      // console.log('part', part);
      let pos = part.indexOf(':');
      if (pos < 0) {
        segments.push(part);
      } else if (pos > 0) {
        console.warn('Unexpected URL segment syntax');
        segments.push(part);
      } else {
        let key = part.substring(1);
        segments.push(String(values[key]));
      }
    });
    // console.log('segments', segments);
    return segments.join('/');
  }

}
