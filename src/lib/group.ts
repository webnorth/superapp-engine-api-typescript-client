import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, ILangParam, IAttributeValues, } from './common/interfaces';
import { isGroup } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * Group object
 *
 * @export
 * @interface IGroup
 */
export interface IGroup {
  id: number;
  name: string;
  domain: string | null;
  subdomain: string | null;
  short_description: string;
  description: string;
  published: 0 | 1;
  public: 0 | 1;
  protected: 0 | 1;
  attributes?: IAttributeValues;
}

/**
 * Group specs
 *
 * @export
 * @interface IGroupSpecs
 */
export interface IGroupSpecs extends ILangParam {
  name: string;
  domain: string;
  subdomain: string;
  short_description?: string;
  description?: string;
  published?: 0 | 1;
  protected?: 0 | 1;
  public?: 0 | 1;
}

/**
 * Group get params
 *
 * @export
 * @interface IGroupGetParams
 */
export interface IGroupGetParams extends ILangParam {
  with_attributes?: 0 | 1;
  expand_attributes?: 0 | 1;
}

/**
 * Group list params
 *
 * @export
 * @interface IGroupListParams
 */
export interface IGroupListParams extends ICollectionParams, IGroupGetParams {
  managed?: 0 | 1;
  subscribed?: 0 | 1;
  public?: 0 | 1;
}

/**
 * Group endpoint methods
 *
 * @export
 * @interface IGroupEndpoint
 */
export interface IGroupEndpoint {
  list(params?: IGroupListParams): Observable<AxiosResponse<ICollection<IGroup>>>;
  create(specs: IGroupSpecs): Observable<AxiosResponse<IContainer<IGroup>>>;
  get(id: number, params?: IGroupGetParams): Observable<AxiosResponse<IContainer<IGroup>>>;
  update(id: number, specs: IGroupSpecs): Observable<AxiosResponse<IContainer<IGroup>>>;
  delete(id: number): Observable<AxiosResponse<IContainer<void>>>;
}

/**
 * Group endpoint
 *
 * @class GroupEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IGroupEndpoint}
 */
export class GroupEndpoint extends Endpoint implements IGroupEndpoint {

  /**
   * Creates an instance of GroupEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof GroupEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/groups');
  }

  /**
   * Create new group.
   *
   * @param {IGroupSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IGroup>>>}
   *
   * @memberof GroupEndpoint
   */
  create(specs: IGroupSpecs): Observable<AxiosResponse<IContainer<IGroup>>> {
    // console.log('create(specs)', specs);
    const url: string = this.prefix;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IGroup>(promise, isGroup);
  }

  /**
   * Delete group by id.
   *
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof GroupEndpoint
   */
  delete(id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('delete(id)', id);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

  /**
   * List all groups.
   *
   * @param {IGroupListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IGroup>>>}
   *
   * @memberof GroupEndpoint
   */
  list(params?: IGroupListParams): Observable<AxiosResponse<ICollection<IGroup>>> {
    // console.log('list(params?)', params);
    const url: string = this.prefix;
    return this.getCollection<IGroup>(url, params, isGroup);
  }

  /**
   * Get group by id.
   *
   * @param {number} id
   * @param {IGroupGetParams} [params]
   * @returns {Observable<AxiosResponse<IContainer<IGroup>>>}
   *
   * @memberof GroupEndpoint
   */
  get(id: number, params?: IGroupGetParams): Observable<AxiosResponse<IContainer<IGroup>>> {
    // console.log('get(id, params?)', id, params);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.get(url, { params });
    return this.getContainer<IGroup>(promise, isGroup);
  }

  /**
   * Update group by id.
   *
   * @param {number} id
   * @param {IGroupSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IGroup>>>}
   *
   * @memberof GroupEndpoint
   */
  update(id: number, specs: IGroupSpecs): Observable<AxiosResponse<IContainer<IGroup>>> {
    // console.log('update(id, specs)', id, specs);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IGroup>(promise, isGroup);
  }

}
