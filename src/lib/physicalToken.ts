import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, IUserStub, } from './common/interfaces';
import { isUserStub } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * Supported types of PhysicalToken
 *
 * @export
 * @interface IPhysicalTokenType
 */
export type IPhysicalTokenType = 'rfid';

/**
 * PhysicalToken raw object
 *
 * @export
 * @interface IPhysicalToken
 */
export interface IPhysicalToken {
  id: number;
  token: string;
  type: IPhysicalTokenType;
  user_id: number;
}

/**
 * PhysicalToken specs
 *
 * @export
 * @interface IPhysicalTokenSpecs
 */
export interface IPhysicalTokenSpecs {
  token: string;
  type: IPhysicalTokenType;
}

/**
 * PhysicalToken list() params
 *
 * @export
 * @interface IPhysicalTokenListParams
 */
export interface IPhysicalTokenListParams extends ICollectionParams {
  sort?: string;
}

/**
 * PhysicalToken endpoint methods
 *
 * @export
 * @interface IPhysicalTokenEndpoint
 */
export interface IPhysicalTokenEndpoint {
  list(group_id: number, user_id: number, token_type: IPhysicalTokenType, params?: IPhysicalTokenListParams): Observable<AxiosResponse<ICollection<IPhysicalToken>>>;
  create(group_id: number, user_id: number, token_type: IPhysicalTokenType, specs: IPhysicalTokenSpecs): Observable<AxiosResponse<IContainer<IPhysicalToken>>>;
  get_user(group_id: number, token_type: IPhysicalTokenType, token: string): Observable<AxiosResponse<IContainer<IUserStub>>>;
  delete(group_id: number, user_id: number, token_type: IPhysicalTokenType, token: string): Observable<AxiosResponse<IContainer<void>>>;
}

/**
 * Guard of IPhysicalTokenType.
 *
 * @export
 * @param {*} data
 * @returns {data is IPhysicalTokenType}
 */
export function isPhysicalTokenType(data): data is IPhysicalTokenType {
  const types = ['rfid'];
  if (types.indexOf(data) < 0) {
    console.error('IPhysicalTokenType must be one of ' + types.join(', '), data);
    return false;
  }
  return true;
}

/**
 * Guard of IPhysicalToken.
 *
 * @export
 * @param {*} data
 * @returns {data is IPhysicalToken}
 */
export function isPhysicalToken(data): data is IPhysicalToken {
  if (data === null || typeof data !== 'object') {
    console.error('IPhysicalToken must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IPhysicalToken.id must be a number', data);
    return false;
  }
  if (typeof data.token !== 'string') {
    console.error('IPhysicalToken.token must be a string', data);
    return false;
  }
  if (!isPhysicalTokenType(data.type)) {
    return false;
  }
  if (typeof data.user_id !== 'number') {
    console.error('IPhysicalToken.user_id must be a number', data);
    return false;
  }
  return true;
}

/**
 * PhysicalToken endpoint
 *
 * @class PhysicalTokenEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IPhysicalTokenEndpoint}
 */
export class PhysicalTokenEndpoint extends Endpoint implements IPhysicalTokenEndpoint {

  /**
   * Creates an instance of PhysicalTokenEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof PhysicalTokenEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/groups/:group_id/users/:user_id_and_token_type/tokens');
  }

  /**
   * List all deposit tokens.
   *
   * @param {number} group_id
   * @param {number} user_id
   * @param {IPhysicalTokenListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IPhysicalToken>>>}
   *
   * @memberof PhysicalTokenEndpoint
   */
  list(group_id: number, user_id: number, token_type: IPhysicalTokenType, params?: IPhysicalTokenListParams): Observable<AxiosResponse<ICollection<IPhysicalToken>>> {
    // console.log('list(group_id, user_id, token_type, params)', group_id, user_id, token_type, params);
    const url: string = this.replacePrefixSegments({ group_id, 'user_id_and_token_type': user_id + '/' + token_type });
    // console.log('url', url);
    return this.getCollection<IPhysicalToken>(url, params, isPhysicalToken);
  }

  /**
   * Create new physical token.
   *
   * @param {number} group_id
   * @param {number} user_id
   * @param {IPhysicalTokenSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IPhysicalToken>>>}
   *
   * @memberof PhysicalTokenEndpoint
   */
  create(group_id: number, user_id: number, token_type: IPhysicalTokenType, specs: IPhysicalTokenSpecs): Observable<AxiosResponse<IContainer<IPhysicalToken>>> {
    // console.log('create(group_id, user_id, specs)', group_id, user_id, specs);
    const url: string = this.replacePrefixSegments({ group_id, 'user_id_and_token_type': user_id + '/' + token_type });
    // console.log('url', url);
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IPhysicalToken>(promise, isPhysicalToken);
  }

  /**
   * Get user by token string.
   *
   * @param {number} group_id
   * @param {IPhysicalTokenType} token_type
   * @param {string} token
   * @returns {Observable<AxiosResponse<IContainer<IPhysicalToken>>>}
   *
   * @memberof PhysicalTokenEndpoint
   */
  get_user(group_id: number, token_type: IPhysicalTokenType, token: string): Observable<AxiosResponse<IContainer<IUserStub>>> {
    // console.log('get(group_id, token_type, token)', group_id, token_type, token);
    const url: string = this.replacePrefixSegments({ group_id, 'user_id_and_token_type': token_type }) + '/' + token;
    // console.log('url', url);
    const promise = this.api.http.get(url);
    return this.getContainer<IUserStub>(promise, isUserStub);
  }

  /**
   * Delete physical token by token string.
   *
   * @param {number} group_id
   * @param {number} user_id
   * @param {IPhysicalTokenType} token_type
   * @param {string} token
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof PhysicalTokenEndpoint
   */
  delete(group_id: number, user_id: number, token_type: IPhysicalTokenType, token: string): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('get(group_id, user_id, token_type, token)', group_id, user_id, token_type, token);
    const url: string = this.replacePrefixSegments({ group_id, 'user_id_and_token_type': user_id + '/' + token_type }) + '/' + token;
    // console.log('url', url);
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

}
