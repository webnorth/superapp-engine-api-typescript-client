import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, ICollection, IDeposit, IDepositListParams, IAuthDepositListParams, IDepositSpecs, IDepositUpdateSpecs, IDepositEndpoint } from './common/interfaces';
import { isDeposit } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * Deposit endpoint
 *
 * @class DepositEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IDepositEndpoint}
 */
export class DepositEndpoint extends Endpoint implements IDepositEndpoint {

  /**
   * Creates an instance of DepositEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof DepositEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    // TODO: The correct prefix
    super(superappEngine, '/groups/:group_id/deposits');
  }

  /**
   * List auth deposits.
   *
   * @param {IAuthDepositListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IDeposit>>>}
   *
   * @memberof DepositEndpoint
   */
  authList(params?: IAuthDepositListParams): Observable<AxiosResponse<ICollection<IDeposit>>> {
    // console.log('list(params?: IAuthDepositListParams)', params);
    const url: string = '/auth/deposits';
    // console.log('url', url);
    return this.getCollection<IDeposit>(url, params, isDeposit);
  }

  /**
   * List all deposits.
   *
   * @param {number} group_id
   * @param {IDepositListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IDeposit>>>}
   *
   * @memberof DepositEndpoint
   */
  list(group_id: number, params?: IDepositListParams): Observable<AxiosResponse<ICollection<IDeposit>>> {
    // console.log('list(group_id: number, params?: IDepositListParams)', group_id, params);
    const url: string = this.replacePrefixSegments({ group_id });
    // console.log('url', url);
    return this.getCollection<IDeposit>(url, params, isDeposit);
  }

  /**
   * Create new deposit.
   *
   * @param {number} group_id
   * @param {IDepositSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IDeposit>>>}
   *
   * @memberof DepositEndpoint
   */
  create(group_id: number, specs: IDepositSpecs): Observable<AxiosResponse<IContainer<IDeposit>>> {
    // console.log('create(group_id: number, specs: IDepositSpecs)', group_id, specs);
    const url: string = this.replacePrefixSegments({ group_id });
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IDeposit>(promise, isDeposit);
  }

  /**
   * Update deposit by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {IDepositUpdateSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IDeposit>>>}
   *
   * @memberof DepositEndpoint
   */
  update(group_id: number, id: number, specs: IDepositUpdateSpecs): Observable<AxiosResponse<IContainer<IDeposit>>> {
    // console.log('update(group_id: number, id: number, specs: IDepositUpdateSpecs)', group_id, id, specs);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IDeposit>(promise, isDeposit);
  }

  /**
   * Delete deposit by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof DepositEndpoint
   */
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('get(group_id: number, id: number)', group_id, id);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

}
