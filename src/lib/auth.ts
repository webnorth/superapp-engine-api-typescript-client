import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import {
  IContainer,
  IAuthUser,
  IAuthRegisterSpecs,
  IAuthGuestRegisterSpecs,
  IAuthReminderParams,
  IAuthLoginParams,
  IAuthMeParams,
  IAuthUpdateSpecs,
  IAuthSubscribeParams,
  IAuthBuySpecs,
  IAuthIsBought,
  IAuthIsBoughtParams,
  IAuthEndpoint,
} from './common/interfaces';
import { isAuthUser, isAuthPaid } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * Auth endpoint
 *
 * @class AuthEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IAuthEndpoint}
 */
export class AuthEndpoint extends Endpoint implements IAuthEndpoint {

  /**
   * Creates an instance of AuthEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof AuthEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/auth');
  }

  /**
   * Register.
   *
   * @param {IAuthRegisterSpecs|IAuthGuestRegisterSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IAuthUser>>>}
   *
   * @memberof AuthEndpoint
   */
  register(specs: IAuthRegisterSpecs | IAuthGuestRegisterSpecs): Observable<AxiosResponse<IContainer<IAuthUser>>> {
    // console.log('register(specs)', specs);
    const url = `${this.prefix}/register`;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IAuthUser>(promise, isAuthUser);
  }

  /**
   * Request verification code.
   *
   * @param {IAuthReminderParams} params
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof AuthEndpoint
   */
  reminder(params: IAuthReminderParams): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('reminder(params)', params);
    const url = `${this.prefix}/reminder`;
    const promise = this.api.http.get(url, { params });
    return this.getContainer<void>(promise);
  }

  /**
   * Log in.
   *
   * @param {IAuthLoginParams} params
   * @returns {Observable<AxiosResponse<IContainer<IAuthUser>>>}
   *
   * @memberof AuthEndpoint
   */
  login(params: IAuthLoginParams): Observable<AxiosResponse<IContainer<IAuthUser>>> {
    // console.log('login(params)', params);
    const url: string = `${this.prefix}/login`;
    const promise = this.api.http.get(url, { params });
    return this.getContainer<IAuthUser>(promise, isAuthUser);
  }

  /**
   * Profile.
   *
   * @param {IAuthMeParams} params
   * @returns {Observable<AxiosResponse<IContainer<IAuthUser>>>}
   *
   * @memberof AuthEndpoint
   */
  me(params: IAuthMeParams): Observable<AxiosResponse<IContainer<IAuthUser>>> {
    // console.log('me(params)', params);
    const url: string = `${this.prefix}/me`;
    const promise = this.api.http.get(url, { params });
    return this.getContainer<IAuthUser>(promise, isAuthUser);
  }

  /**
   * Update.
   *
   * @param {IAuthUpdateSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IAuthUser>>>}
   *
   * @memberof AuthEndpoint
   */
  update(specs: IAuthUpdateSpecs): Observable<AxiosResponse<IContainer<IAuthUser>>> {
    // console.log('update(specs)', specs);
    const url = `${this.prefix}/update`;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IAuthUser>(promise, isAuthUser);
  }

  /**
   * Quick update.
   *
   * @param {IAuthUpdateSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof AuthEndpoint
   */
  quick_update(specs: IAuthUpdateSpecs): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('quick_update(specs)', specs);
    const url = `${this.prefix}/quick_update`;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<void>(promise);
  }

  /**
   * Subscribe.
   *
   * @param {IAuthSubscribeParams} params
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof AuthEndpoint
   */
  subscribe(params: IAuthSubscribeParams): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('subscribe(params)', params);
    const url = `${this.prefix}/subscribe`;
    const promise = this.api.http.get(url, { params });
    return this.getContainer<void>(promise);
  }

  /**
   * Unsubscribe.
   *
   * @param {IAuthSubscribeParams} params
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof AuthEndpoint
   */
  unsubscribe(params: IAuthSubscribeParams): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('unsubscribe(params)', params);
    const url = `${this.prefix}/unsubscribe`;
    const promise = this.api.http.get(url, { params });
    return this.getContainer<void>(promise);
  }

  /**
   * Log out.
   *
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof AuthEndpoint
   */
  logout(): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('logout()');
    const url = `${this.prefix}/logout`;
    const promise = this.api.http.get(url);
    return this.getContainer<void>(promise);
  }

  /**
   * Request deletion.
   *
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof AuthEndpoint
   */
  request_deletion(): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('request_deletion()');
    const url = `${this.prefix}/request_deletion`;
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

  /**
   * Download.
   *
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof AuthEndpoint
   */
  download(): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('download()');
    const url = `${this.prefix}/download`;
    const promise = this.api.http.get(url);
    return this.getContainer<void>(promise);
  }

  /**
   * Buy.
   *
   * @param {IAuthBuySpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof AuthEndpoint
   */
  buy(specs: IAuthBuySpecs): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('buy(specs)', specs);
    const url = `${this.prefix}/buy`;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<void>(promise);
  }

  /**
   * Check if product is bought.
   *
   * @param {IAuthIsBoughtParams} params
   * @returns {Observable<AxiosResponse<IContainer<IAuthIsBought>>>}
   *
   * @memberof AuthEndpoint
   */
  is_bought(params: IAuthIsBoughtParams): Observable<AxiosResponse<IContainer<IAuthIsBought>>> {
    // console.log('is_bought(params)', params);
    const url = `${this.prefix}/is_bought`;
    const promise = this.api.http.get(url, { params });
    return this.getContainer<IAuthIsBought>(promise, isAuthPaid);
  }

}
