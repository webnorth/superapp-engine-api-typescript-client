import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, ICollection, IAttributeValues, IAttribute, IAttributeSpecs, IAttributeGetParams, IAttributeListParams, } from './common/interfaces';
import { isAttribute, isAttributeOrVoid, isTag } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * UserAttribute list_related() params
 *
 * @export
 * @interface IUserAttributeListRelatedParams
 */
export interface IUserAttributeListRelatedParams extends IAttributeListParams {
  category_id: number;
  answers?: IAttributeValues;
}

/**
 * UserAttribute next() params
 *
 * @export
 * @interface IUserAttributeNextParams
 */
export interface IUserAttributeNextParams extends IAttributeGetParams {
  category_id: number;
  previous_id?: number;
  only_empty?: 0 | 1;
}

/**
 * UserAttribute endpoint methods
 *
 * @export
 * @interface IUserAttributeEndpoint
 */
export interface IUserAttributeEndpoint {
  list(group_id: number, params?: IAttributeListParams): Observable<AxiosResponse<ICollection<IAttribute>>>;
  create(group_id: number, specs: IAttributeSpecs): Observable<AxiosResponse<IContainer<IAttribute>>>;
  get(group_id: number, id: number, params?: IAttributeGetParams): Observable<AxiosResponse<IContainer<IAttribute>>>;
  list_related(group_id: number, params: IUserAttributeListRelatedParams): Observable<AxiosResponse<ICollection<IAttribute>>>;
  next(group_id: number, params: IUserAttributeNextParams): Observable<AxiosResponse<IContainer<IAttribute | void>>>;
  update(group_id: number, id: number, specs: IAttributeSpecs): Observable<AxiosResponse<IContainer<IAttribute>>>;
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}

/**
 * UserAttribute endpoint
 *
 * @class UserAttributeEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IUserAttributeEndpoint}
 */
export class UserAttributeEndpoint extends Endpoint implements IUserAttributeEndpoint {

  /**
   * Creates an instance of UserAttributeEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof UserAttributeEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/groups/:group_id/users/attributes');
  }

  /**
   * List all group user attributes.
   *
   * @param {number} group_id
   * @param {IAttributeListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IAttribute>>>}
   *
   * @memberof UserAttributeEndpoint
   */
  list(group_id: number, params?: IAttributeListParams): Observable<AxiosResponse<ICollection<IAttribute>>> {
    // console.log('list(group_id, params?)', group_id, params);
    const url: string = this.replacePrefixSegments({ group_id });
    // console.log('url', url);
    return this.getCollection<IAttribute>(url, params, isAttribute);
  }

  /**
   * Create new group user attribute.
   *
   * @param {number} group_id
   * @param {IAttributeSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IAttribute>>>}
   *
   * @memberof UserAttributeEndpoint
   */
  create(group_id: number, specs: IAttributeSpecs): Observable<AxiosResponse<IContainer<IAttribute>>> {
    // console.log('create(group_id, specs)', group_id, specs);
    const url: string = this.replacePrefixSegments({ group_id });
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IAttribute>(promise, isAttribute);
  }

  /**
   * Get group user attribute by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {IAttributeGetParams} [params]
   * @returns {Observable<AxiosResponse<IContainer<IAttribute>>>}
   *
   * @memberof UserAttributeEndpoint
   */
  get(group_id: number, id: number, params?: IAttributeGetParams): Observable<AxiosResponse<IContainer<IAttribute>>> {
    // console.log('get(group_id, id, params?)', group_id, id, params);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    // console.log('url', url);
    const promise = this.api.http.get(url);
    return this.getContainer<IAttribute>(promise, isAttribute);
  }

  /**
   * List all group user attributes list_related.
   *
   * @param {number} group_id
   * @param {IUserAttributeListRelatedParams} params
   * @returns {Observable<AxiosResponse<ICollection<IAttribute>>>}
   *
   * @memberof UserAttributeEndpoint
   */
  list_related(group_id: number, params: IUserAttributeListRelatedParams): Observable<AxiosResponse<ICollection<IAttribute>>> {
    // console.log('list(group_id, params)', group_id, params);
    const url: string = this.replacePrefixSegments({ group_id }) + '/list_related';
    // console.log('url', url);
    return this.getCollection<IAttribute>(url, params, isAttribute);
  }

  /**
   * Get group user attribute based on - category; previous attribute; user value status.
   *
   * @param {number} group_id
   * @param {IUserAttributeNextParams} params
   * @returns {Observable<AxiosResponse<IContainer<IAttribute>>>}
   *
   * @memberof UserAttributeEndpoint
   */
  next(group_id: number, params: IUserAttributeNextParams): Observable<AxiosResponse<IContainer<IAttribute | void>>> {
    // console.log('next(group_id, params)', group_id, params);
    const url: string = this.replacePrefixSegments({ group_id }) + '/next';
    // console.log('url', url);
    const promise = this.api.http.get(url, { params });
    return this.getContainer<IAttribute | void>(promise, isAttributeOrVoid);
  }

  /**
   * Update group user attribute by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {IAttributeSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IAttribute>>>}
   *
   * @memberof UserAttributeEndpoint
   */
  update(group_id: number, id: number, specs: IAttributeSpecs): Observable<AxiosResponse<IContainer<IAttribute>>> {
    // console.log('update(group_id, id, specs)', group_id, id, specs);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IAttribute>(promise, isAttribute);
  }

  /**
   * Delete group user attribute by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof UserAttributeEndpoint
   */
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('get(group_id, id)', group_id, id);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

}
