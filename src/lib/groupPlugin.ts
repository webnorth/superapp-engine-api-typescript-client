import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import {
  IContainer,
  ICollection,
  ICollectionParams,
  IAttributeValues,
  ILangParam,
} from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * GroupPlugin raw object
 *
 * @export
 * @interface IGroupPlugin
 */
export interface IGroupPlugin {
  id: number;
  plugin_id: number | null;
  name: string;
  description: string;
  icon: string;
  key: string | null;
  entity_id: number;
  prefix: string;
  attributes?: IAttributeValues;
  created_at: string;
  updated_at: string;
}

/**
 * GroupPlugin get params
 *
 * @export
 * @interface IGroupPluginGetParams
 */
export interface IGroupPluginGetParams extends ILangParam {
  with_attributes?: 0 | 1;
}

/**
 * GroupPlugin list params
 *
 * @export
 * @interface IGroupPluginListParams
 */
export interface IGroupPluginListParams extends ICollectionParams, IGroupPluginGetParams {
  prefix?: string;
}

/**
 * GroupPlugin specs
 *
 * @export
 * @interface IGroupPluginSpecs
 */
export interface IGroupPluginSpecs extends IGroupPluginGetParams {
  plugin_id?: number;
  name: string;
  description?: string;
  icon?: string;
  prefix?: string;
}

/**
 * GroupPlugin update specs
 *
 * @export
 * @interface IGroupPluginUpdateSpecs
 */
export interface IGroupPluginUpdateSpecs extends IGroupPluginGetParams {
  name?: string;
  description?: string;
  icon?: string;
  prefix?: string;
}

/**
 * GroupPlugin endpoint methods
 *
 * @export
 * @interface IGroupPluginEndpoint
 */
export interface IGroupPluginEndpoint {
  isGroupPlugin(data: any): data is IGroupPlugin;

  list(group_id: number, params?: IGroupPluginListParams): Observable<AxiosResponse<ICollection<IGroupPlugin>>>;
  create(group_id: number, specs: IGroupPluginSpecs): Observable<AxiosResponse<IContainer<IGroupPlugin>>>;
  get(group_id: number, id: number, params?: IGroupPluginGetParams): Observable<AxiosResponse<IContainer<IGroupPlugin>>>;
  update(group_id: number, id: number, specs: IGroupPluginUpdateSpecs): Observable<AxiosResponse<IContainer<IGroupPlugin>>>;
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}

/**
 * GroupPlugin endpoint
 *
 * @class GroupPluginEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IGroupPluginEndpoint}
 */
export class GroupPluginEndpoint extends Endpoint implements IGroupPluginEndpoint {

  /**
   * Creates an instance of GroupPluginEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof GroupPluginEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/groups/:group_id/plugins');
  }

  /**
   * Guard of IGroupPlugin.
   *
   * @param {*} data
   * @returns {data is IGroupPlugin}
   */
  isGroupPlugin(data: any): data is IGroupPlugin {
    if (data === null || typeof data !== 'object') {
      console.error('IGroupPlugin must be an object', data);
      return false;
    }
    if (typeof data.id !== 'number') {
      console.error('IGroupPlugin.id must be a number', data);
      return false;
    }
    if (typeof data.plugin_id !== 'number' && data.plugin_id !== null) {
      console.error('IGroupPlugin.plugin_id must be a number or NULL', data);
      return false;
    }
    if (typeof data.name !== 'string') {
      console.error('IGroupPlugin.name must be a string', data);
      return false;
    }
    if (typeof data.icon !== 'string' && data.icon !== null) {
      console.error('IGroupPlugin.icon must be a string or NULL', data);
      return false;
    }
    if (typeof data.key !== 'string' && data.key !== null) {
      console.error('IGroupPlugin.key must be a string or NULL', data);
      return false;
    }
    if (typeof data.entity_id !== 'number') {
      console.error('IGroupPlugin.entity_id must be a number', data);
      return false;
    }
    // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
    if (typeof data.created_at !== 'string') {
      console.error('IGroupPlugin.created_at must be a string', data);
      return false;
    }
    // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
    if (typeof data.updated_at !== 'string') {
      console.error('IGroupPlugin.updated_at must be a string', data);
      return false;
    }
    return true;
  }

  /**
   * List all group plugins.
   *
   * @param {number} group_id
   * @param {IGroupPluginListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IGroupPlugin>>>}
   *
   * @memberof GroupPluginEndpoint
   */
  list(group_id: number, params?: IGroupPluginListParams): Observable<AxiosResponse<ICollection<IGroupPlugin>>> {
    // console.log('list(group_id, params?)', group_id, params);
    const url: string = this.replacePrefixSegments({ group_id });
    // console.log('url', url);
    return this.getCollection<IGroupPlugin>(url, params, this.isGroupPlugin);
  }

  /**
   * Create new group plugin.
   *
   * @param {number} group_id
   * @param {IGroupPluginSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IGroupPlugin>>>}
   *
   * @memberof GroupPluginEndpoint
   */
  create(group_id: number, specs: IGroupPluginSpecs): Observable<AxiosResponse<IContainer<IGroupPlugin>>> {
    // console.log('create(group_id, specs)', group_id, specs);
    const url: string = this.replacePrefixSegments({ group_id });
    // console.log('url', url);
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IGroupPlugin>(promise, this.isGroupPlugin);
  }

  /**
   * Get group plugin by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {IGroupPluginGetParams} [params]
   * @returns {Observable<AxiosResponse<IContainer<IGroupPlugin>>>}
   *
   * @memberof GroupPluginEndpoint
   */
  get(group_id: number, id: number, params?: IGroupPluginGetParams): Observable<AxiosResponse<IContainer<IGroupPlugin>>> {
    // console.log('get(group_id, id, params?)', group_id, id, params);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    // console.log('url', url);
    const promise = this.api.http.get(url, { params });
    return this.getContainer<IGroupPlugin>(promise, this.isGroupPlugin);
  }

  /**
   * Update group plugin by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {IGroupPluginUpdateSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IGroupPlugin>>>}
   *
   * @memberof GroupPluginEndpoint
   */
  update(group_id: number, id: number, specs: IGroupPluginUpdateSpecs): Observable<AxiosResponse<IContainer<IGroupPlugin>>> {
    // console.log('update(group_id, id, specs)', group_id, id, specs);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    // console.log('url', url);
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IGroupPlugin>(promise, this.isGroupPlugin);
  }

  /**
   * Delete group plugin by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof GroupPluginEndpoint
   */
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('get(group_id, id)', group_id, id);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    // console.log('url', url);
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

}
