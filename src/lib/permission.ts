import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, IPermission, IPermissionSpecs, IPermissionEndpoint } from './common/interfaces';
import { isPermission } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * Permission endpoint
 *
 * @class PermissionEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IPermissionEndpoint}
 */
export class PermissionEndpoint extends Endpoint implements IPermissionEndpoint {

  /**
   * Creates an instance of PermissionEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof PermissionEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/permissions');
  }

  /**
   * Create new permission.
   *
   * @param {IPermissionSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IPermission>>>}
   *
   * @memberof PermissionEndpoint
   */
  create(specs: IPermissionSpecs): Observable<AxiosResponse<IContainer<IPermission>>> {
    // console.log('create(specs: IPermissionSpecs)', specs);
    const url: string = this.prefix;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IPermission>(promise, isPermission);
  }

  /**
   * Delete permission by id.
   *
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof PermissionEndpoint
   */
  delete(id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('delete(id: number)', id);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

  /**
   * List all permissions.
   *
   * @param {ICollectionParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IPermission>>>}
   *
   * @memberof PermissionEndpoint
   */
  list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IPermission>>> {
    // console.log('list(params?: ICollectionParams)', params);
    const url: string = this.prefix;
    // console.log('url', url);
    return this.getCollection<IPermission>(url, params, isPermission);
  }

  /**
   * Get permission by id.
   *
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<IPermission>>>}
   *
   * @memberof PermissionEndpoint
   */
  get(id: number): Observable<AxiosResponse<IContainer<IPermission>>> {
    // console.log('get(id: number)', id);
    const url = `${this.prefix}/${id}`;
    // console.log('url', url);
    const promise = this.api.http.get(url);
    return this.getContainer<IPermission>(promise, isPermission);
  }

  /**
   * Update permission by id.
   *
   * @param {number} id
   * @param {IPermissionSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IPermission>>>}
   *
   * @memberof PermissionEndpoint
   */
  update(id: number, specs: IPermissionSpecs): Observable<AxiosResponse<IContainer<IPermission>>> {
    // console.log('update(id: number, specs: IPermissionSpecs)', id, specs);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IPermission>(promise, isPermission);
  }

}
