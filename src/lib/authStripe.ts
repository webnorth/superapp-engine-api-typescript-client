import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { ICollection, IContainer, IPaymentMethod, IPaymentIntent, IStripeDeposit, IPaymentMethodSpecs, IStripeDepositSpecs, IAuthStripeEndpoint } from './common/interfaces';
import { isPaymentMethod, isPaymentIntent, isStripeDeposit } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * Stripe endpoint
 *
 * @class AuthStripeEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IAuthStripeEndpoint}
 */
export class AuthStripeEndpoint extends Endpoint implements IAuthStripeEndpoint {

  /**
   * Creates an instance of AuthStripeEndpoint.
   * 
   * @param {SuperappEngine} superappEngine
   *
   * @memberof AuthStripeEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine)', superappEngine);
    super(superappEngine, '/auth/stripe');
  }

  /**
   * List stripe payment methods.
   * 
   * @returns {Observable<AxiosResponse<ICollection<IPaymentMethod>>>}
   *
   * @memberof AuthStripeEndpoint
   */
  list(): Observable<AxiosResponse<ICollection<IPaymentMethod>>> {
    // console.log('list()');
    const url: string = this.prefix + '/method'
    return this.getCollection<IPaymentMethod>(url, null, isPaymentMethod);
  }

  /**
   * Create new stripe payment method.
   *
   * @param {IPaymentMethodSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IPaymentMethod>>>}
   *
   * @memberof AuthStripeEndpoint
   */
  create(specs: IPaymentMethodSpecs): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('create(specs)', specs);
    const url: string = this.prefix + '/method'
    const promise = this.api.http.post(url, specs);
    return this.getContainer<void>(promise);
  }

  /**
   * Add money.
   *
   * @param {IStripeDepositSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IStripeDeposit>>>}
   *
   * @memberof AuthStripeEndpoint
   */
  deposit(specs: IStripeDepositSpecs): Observable<AxiosResponse<IContainer<IStripeDeposit>>> {
    // console.log('deposit(specs)', specs);
    const url: string = this.prefix + '/deposit'
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IStripeDeposit>(promise, isStripeDeposit);
  }

  /**
   * Get stripe payment intent.
   *
   * @returns {Observable<AxiosResponse<IContainer<IPaymentIntent>>>}
   *
   * @memberof AuthStripeEndpoint
   */
  intent(): Observable<AxiosResponse<IContainer<IPaymentIntent>>> {
    // console.log('intent()');
    const url: string = this.prefix + '/intent'
    // console.log('url', url);
    const promise = this.api.http.get(url);
    return this.getContainer<IPaymentIntent>(promise, isPaymentIntent);
  }

  /**
   * Delete stripe payment methods.
   *
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof AuthStripeEndpoint
   */
  delete(): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('delete()');
    const url: string = this.prefix + '/method'
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

}
