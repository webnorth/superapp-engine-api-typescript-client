import { AxiosError, AxiosResponse } from 'axios';
import API from './common/api';
import { IAPISpecs, IError } from './common/interfaces';

/**
 * Superapp Engine API
 *
 * @export
 * @class SuperappEngine
 * @extends {API}
 */
class SuperappEngine extends API {

  /**
   * error fn handler interceptor
   *
   * @private
   * @static
   *
   * @memberof SuperappEngine
   */
  private static errorHandler(error: AxiosError) {
    const res: AxiosResponse<IError> = error.response;
    if (!res) return Promise.reject(error);

    error.message = [
      res.status || '',
      res.statusText || '',
    ].join(' - ');

    if (res.data && res.data.error && res.data.error.message) {
      error.message = [error.message, res.data.error.message].join(' - ');
    }
    return Promise.reject(error);
  }

  /**
   * Creates an instance of SuperappEngine.
   * 
   * @param {IAPISpecs} apiSpecs
   *
   * @memberof SuperappEngine
   */
  constructor(apiSpecs: IAPISpecs) {
    // console.log('constructor(apiSpecs IAPISpecs)', apiSpecs);
    const defaultSpecs: IAPISpecs = {
      headers: {
        'Content-Type': 'application/json',
      },
      host: 'api.appsup.eu',
      invalidResponse: new Error('Invalid API response.'),
      prefix: '/api',
      protocol: 'https',
      timeout: 5000,
    };

    super({
      headers: apiSpecs.headers || defaultSpecs.headers,
      host: apiSpecs.host || defaultSpecs.host,
      invalidResponse: apiSpecs.invalidResponse || defaultSpecs.invalidResponse,
      prefix: apiSpecs.prefix || defaultSpecs.prefix,
      protocol: apiSpecs.protocol || defaultSpecs.protocol,
      timeout: apiSpecs.timeout || defaultSpecs.timeout,
    });
    this.http.interceptors.response.use(null, SuperappEngine.errorHandler);
  }
}

export default SuperappEngine;
export { SuperappEngine };
