import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, IPushToken, IPushTokenSpecs, IPushTokenEndpoint } from './common/interfaces';
import { isPushToken } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * PushToken endpoint
 *
 * @class PushTokenEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IPushTokenEndpoint}
 */
export class PushTokenEndpoint extends Endpoint implements IPushTokenEndpoint {

  /**
   * Creates an instance of PushTokenEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof PushTokenEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/auth/push_tokens');
  }

  /**
   * List all push tokens.
   *
   * @param {ICollectionParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IPushToken>>>}
   *
   * @memberof PushTokenEndpoint
   */
  list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IPushToken>>> {
    // console.log('list(params?: ICollectionParams)', params);
    const url: string = this.prefix;
    return this.getCollection<IPushToken>(url, params, isPushToken);
  }

  /**
   * Create new push token.
   *
   * @param {IPushTokenSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IPushToken>>>}
   *
   * @memberof PushTokenEndpoint
   */
  create(specs: IPushTokenSpecs): Observable<AxiosResponse<IContainer<IPushToken>>> {
    // console.log('create(specs: IPushTokenSpecs)', specs);
    const url: string = this.prefix;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IPushToken>(promise, isPushToken);
  }

  /**
   * Get push token by id.
   *
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<IPushToken>>>}
   *
   * @memberof PushTokenEndpoint
   */
  get(id: number): Observable<AxiosResponse<IContainer<IPushToken>>> {
    // console.log('get(id: number)', id);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.get(url);
    return this.getContainer<IPushToken>(promise, isPushToken);
  }

  /**
   * Update push token by id.
   *
   * @param {number} id
   * @param {IPushTokenSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IPushToken>>>}
   *
   * @memberof PushTokenEndpoint
   */
  update(id: number, specs: IPushTokenSpecs): Observable<AxiosResponse<IContainer<IPushToken>>> {
    // console.log('update(id: number, specs: IPushTokenSpecs)', id, specs);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IPushToken>(promise, isPushToken);
  }

  /**
   * Delete push token by id.
   *
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof PushTokenEndpoint
   */
  delete(id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('delete(id: number)', id);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

}
