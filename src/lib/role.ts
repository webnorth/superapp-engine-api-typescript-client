import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, IRole, IRoleSpecs, IRoleEndpoint } from './common/interfaces';
import { isRole } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * Role endpoint
 *
 * @class RoleEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IRoleEndpoint}
 */
export class RoleEndpoint extends Endpoint implements IRoleEndpoint {

  /**
   * Creates an instance of RoleEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof RoleEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/roles');
  }

  /**
   * List all roles.
   *
   * @param {ICollectionParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IRole>>>}
   *
   * @memberof RoleEndpoint
   */
  list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IRole>>> {
    // console.log('list(params?: ICollectionParams)', params);
    const url: string = this.prefix;
    return this.getCollection<IRole>(url, params, isRole);
  }

  /**
   * Create new role.
   *
   * @param {IRoleSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IRole>>>}
   *
   * @memberof RoleEndpoint
   */
  create(specs: IRoleSpecs): Observable<AxiosResponse<IContainer<IRole>>> {
    // console.log('create(specs: IRoleSpecs)', specs);
    const url: string = this.prefix;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IRole>(promise, isRole);
  }

  /**
   * Get role by id.
   *
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<IRole>>>}
   *
   * @memberof RoleEndpoint
   */
  get(id: number): Observable<AxiosResponse<IContainer<IRole>>> {
    // console.log('get(id: number)', id);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.get(url);
    return this.getContainer<IRole>(promise, isRole);
  }

  /**
   * Update role by id.
   *
   * @param {number} id
   * @param {IRoleSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IRole>>>}
   *
   * @memberof RoleEndpoint
   */
  update(id: number, specs: IRoleSpecs): Observable<AxiosResponse<IContainer<IRole>>> {
    // console.log('update(id: number, specs: IRoleSpecs)', id, specs);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IRole>(promise, isRole);
  }

  /**
   * Delete role by id.
   *
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof RoleEndpoint
   */
  delete(id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('delete(id: number)', id);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

}
