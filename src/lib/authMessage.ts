import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, ICollection, IMessageLike, IAuthMessageListParams, IAuthMessageEndpoint } from './common/interfaces';
import { isMessageLike } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * Message endpoint
 *
 * @class AuthMessageEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IAuthMessageEndpoint}
 */
export class AuthMessageEndpoint extends Endpoint implements IAuthMessageEndpoint {

  /**
   * Creates an instance of AuthMessageEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof AuthMessageEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/auth/messages');
  }

  /**
   * List all auth messages.
   *
   * @param {IAuthMessageListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IMessageLike>>>}
   *
   * @memberof AuthMessageEndpoint
   */
  list(params?: IAuthMessageListParams): Observable<AxiosResponse<ICollection<IMessageLike>>> {
    // console.log('list(params?: IAuthMessageListParams)', params);
    const url: string = this.prefix
    return this.getCollection<IMessageLike>(url, params, isMessageLike);
  }

  /**
   * Touch auth message by id.
   *
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<IMessageLike>>>}
   *
   * @memberof AuthMessageEndpoint
   */
  touch(id: number): Observable<AxiosResponse<IContainer<IMessageLike>>> {
    // console.log('touch(id: number)', id);
    const url = `${this.prefix}/${id}/touch`;
    const promise = this.api.http.get(url);
    return this.getContainer<IMessageLike>(promise, isMessageLike);
  }

}
