import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import {
  IContainer,
  ICollection,
  ICollectionParams,
  IAttributeValues,
  ILangParam,
} from './common/interfaces';
import { isAttributeValues } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * TicketTemplate raw object
 *
 * @export
 * @interface ITicketTemplate
 */
export interface ITicketTemplate {
  id: number;
  name: string;
  description: string;
  body: string;
  ticket_header_image: number;
  nametag_html_template: string;
  group_plugin_id: number;
  attributes?: IAttributeValues;
}

/**
 * TicketTemplate get params
 *
 * @export
 * @interface ITicketTemplateGetParams
 */
export interface ITicketTemplateGetParams extends ILangParam {
  group_plugin_id?: number;
  with_attributes?: 0 | 1;
}

/**
 * TicketTemplate list params
 *
 * @export
 * @interface ITicketTemplateListParams
 */
export interface ITicketTemplateListParams extends ICollectionParams, ITicketTemplateGetParams {

}

/**
 * TicketTemplate specs
 *
 * @export
 * @interface ITicketTemplateSpecs
 */
export interface ITicketTemplateSpecs extends IAttributeValues, ILangParam {
  name: string;
  description?: string;
  body: string;
  ticket_header_image?: number;
  nametag_html_template?: string;
  group_plugin_id?: number;
  // 
  with_attributes?: 0 | 1;
}

/**
 * TicketTemplate update specs
 *
 * @export
 * @interface ITicketTemplateUpdateSpecs
 */
export interface ITicketTemplateUpdateSpecs extends IAttributeValues, ILangParam {
  name?: string;
  description?: string;
  body?: string;
  ticket_header_image?: number;
  nametag_html_template?: string;
  // 
  with_attributes?: 0 | 1;
}

/**
 * TicketTemplate endpoint methods
 *
 * @export
 * @interface ITicketTemplateEndpoint
 */
export interface ITicketTemplateEndpoint {
  list(group_id: number, params?: ITicketTemplateListParams): Observable<AxiosResponse<ICollection<ITicketTemplate>>>;
  create(group_id: number, specs: ITicketTemplateSpecs): Observable<AxiosResponse<IContainer<ITicketTemplate>>>;
  get(group_id: number, id: number, params?: ITicketTemplateGetParams): Observable<AxiosResponse<IContainer<ITicketTemplate>>>;
  update(group_id: number, id: number, specs: ITicketTemplateUpdateSpecs): Observable<AxiosResponse<IContainer<ITicketTemplate>>>;
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}

/**
 * Guard of ITicketTemplate.
 *
 * @export
 * @param {any} data
 * @returns {data is ITicketTemplate}
 */
export function isTicketTemplate(data: any): data is ITicketTemplate {
  if (data === null || typeof data !== 'object') {
    console.error('ITicketTemplate must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('ITicketTemplate.id must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('ITicketTemplate.name must be a string', data);
    return false;
  }
  if (typeof data.description !== 'string' && data.description !== null) {
    console.error('ITicketTemplate.description must be a string or NULL', data);
    return false;
  }
  if (typeof data.body !== 'string') {
    console.error('ITicketTemplate.body must be a string', data);
    return false;
  }
  if (typeof data.nametag_html_template !== 'string' && data.nametag_html_template !== null) {
    console.error('ITicketTemplate.nametag_html_template must be a string or NULL', data);
    return false;
  }
  if (typeof data.ticket_header_image !== 'number' && data.ticket_header_image !== null) {
    console.error('ITicketTemplate.ticket_header_image must be a number or NULL', data);
    return false;
  }
  if (typeof data.group_plugin_id !== 'number' && data.group_plugin_id !== null) {
    console.error('ITicketTemplate.group_plugin_id must be a number or NULL', data);
    return false;
  }
  if (data.attributes && !isAttributeValues(data.attributes)) {
    return false;
  }
  return true;
}

/**
 * TicketTemplate endpoint
 *
 * @class TicketTemplateEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {ITicketTemplateEndpoint}
 */
export class TicketTemplateEndpoint extends Endpoint implements ITicketTemplateEndpoint {

  /**
   * Creates an instance of TicketTemplateEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof TicketTemplateEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/groups/:group_id/ticket_templates');
  }

  /**
   * List all ticket templates.
   *
   * @param {number} group_id
   * @param {ITicketTemplateListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<ITicketTemplate>>>}
   *
   * @memberof TicketTemplateEndpoint
   */
  list(group_id: number, params?: ITicketTemplateListParams): Observable<AxiosResponse<ICollection<ITicketTemplate>>> {
    // console.log('list(group_id, params?)', group_id, params);
    const url: string = this.replacePrefixSegments({ group_id });
    // console.log('url', url);
    return this.getCollection<ITicketTemplate>(url, params, isTicketTemplate);
  }

  /**
   * Create new ticket template.
   *
   * @param {number} group_id
   * @param {ITicketTemplateSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<ITicketTemplate>>>}
   *
   * @memberof TicketTemplateEndpoint
   */
  create(group_id: number, specs: ITicketTemplateSpecs): Observable<AxiosResponse<IContainer<ITicketTemplate>>> {
    // console.log('create(group_id, specs)', group_id, specs);
    const url: string = this.replacePrefixSegments({ group_id });
    // console.log('url', url);
    const promise = this.api.http.post(url, specs);
    return this.getContainer<ITicketTemplate>(promise, isTicketTemplate);
  }

  /**
   * Get ticket template by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {ITicketTemplateGetParams} [params]
   * @returns {Observable<AxiosResponse<IContainer<ITicketTemplate>>>}
   *
   * @memberof TicketTemplateEndpoint
   */
  get(group_id: number, id: number, params?: ITicketTemplateGetParams): Observable<AxiosResponse<IContainer<ITicketTemplate>>> {
    // console.log('get(group_id, id, params?)', group_id, id, params);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    // console.log('url', url);
    const promise = this.api.http.get(url, { params });
    return this.getContainer<ITicketTemplate>(promise, isTicketTemplate);
  }

  /**
   * Update ticket template by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {ITicketTemplateUpdateSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<ITicketTemplate>>>}
   *
   * @memberof TicketTemplateEndpoint
   */
  update(group_id: number, id: number, specs: ITicketTemplateUpdateSpecs): Observable<AxiosResponse<IContainer<ITicketTemplate>>> {
    // console.log('update(group_id, id, specs)', group_id, id, specs);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    // console.log('url', url);
    const promise = this.api.http.post(url, specs);
    return this.getContainer<ITicketTemplate>(promise, isTicketTemplate);
  }

  /**
   * Delete ticket template by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof TicketTemplateEndpoint
   */
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('get(group_id, id)', group_id, id);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    // console.log('url', url);
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

}
