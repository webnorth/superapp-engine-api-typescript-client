import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, ILangParam, IDestination, IDestinationSpecs, } from './common/interfaces';
import { isDestination } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * Menu raw object
 *
 * @export
 * @interface IMenu
 */
export interface IMenu {
  id: number;
  name: string;
  slug: string;
  enabled: boolean;
  created_at: string;
  updated_at: string;
  destinations: IDestination[];
}

/**
 * Menu specs
 *
 * @export
 * @interface IMenuSpecs
 */
export interface IMenuSpecs extends ILangParam {
  name: string;
  slug?: string;
  enabled?: boolean;
  destinations?: IDestinationSpecs[];
}

/**
 * Menu get params
 *
 * @export
 * @interface IMenuGetParams
 */
export interface IMenuGetParams extends ILangParam {
}

/**
 * Menu list params
 *
 * @export
 * @interface IMenuListParams
 */
export interface IMenuListParams extends ICollectionParams, IMenuGetParams {
}

/**
 * Menu endpoint methods
 *
 * @export
 * @interface IMenuEndpoint
 */
export interface IMenuEndpoint {
  list(group_id: number, params?: IMenuListParams): Observable<AxiosResponse<ICollection<IMenu>>>;
  create(group_id: number, specs: IMenuSpecs): Observable<AxiosResponse<IContainer<IMenu>>>;
  get(group_id: number, id: number, params?: IMenuGetParams): Observable<AxiosResponse<IContainer<IMenu>>>;
  update(group_id: number, id: number, specs: IMenuSpecs): Observable<AxiosResponse<IContainer<IMenu>>>;
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}

/**
 * Guard of IMenu.
 *
 * @export
 * @param {*} data
 * @returns {data is IMenu}
 */
export function isMenu(data): data is IMenu {
  if (data === null || typeof data !== 'object') {
    console.error('IMenu must be an object', data);
    return false;
  }
  if (typeof data.id !== 'number') {
    console.error('IMenu.id must be a number', data);
    return false;
  }
  if (typeof data.name !== 'string') {
    console.error('IMenu.name must be a string', data);
    return false;
  }
  if (typeof data.slug !== 'string') {
    console.error('IMenu.slug must be a string', data);
    return false;
  }
  if (typeof data.enabled !== 'boolean') {
    console.error('IMenu.enabled must be a boolean', data);
    return false;
  }
  // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
  if (typeof data.created_at !== 'string') {
    console.error('IMenu.created_at must be a string', data);
    return false;
  }
  // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
  if (typeof data.updated_at !== 'string') {
    console.error('IMenu.updated_at must be a string', data);
    return false;
  }
  if (!Array.isArray(data.destinations)) {
    console.error('IMenu.destinations must be an array', data);
    return false;
  }
  if (!data.destinations.every(isDestination)) return false;
  return true;
}

/**
 * Menu endpoint
 *
 * @class MenuEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IMenuEndpoint}
 */
export class MenuEndpoint extends Endpoint implements IMenuEndpoint {

  /**
   * Creates an instance of MenuEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof MenuEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine)', superappEngine);
    super(superappEngine, '/groups/:group_id/menus');
  }

  /**
   * Create new menu.
   *
   * @param {number} group_id
   * @param {IMenuSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IMenu>>>}
   *
   * @memberof MenuEndpoint
   */
  create(group_id: number, specs: IMenuSpecs): Observable<AxiosResponse<IContainer<IMenu>>> {
    // console.log('create(group_id, specs)', group_id, specs);
    const url: string = this.replacePrefixSegments({ group_id });
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IMenu>(promise, isMenu);
  }

  /**
   * Delete menu by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof MenuEndpoint
   */
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('get(group_id, id)', group_id, id);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

  /**
   * List all menus.
   *
   * @param {number} group_id
   * @param {IMenuListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IMenu>>>}
   *
   * @memberof MenuEndpoint
   */
  list(group_id: number, params?: IMenuListParams): Observable<AxiosResponse<ICollection<IMenu>>> {
    // console.log('list(group_id, params?)', group_id, params);
    const url: string = this.replacePrefixSegments({ group_id });
    // console.log('url', url);
    return this.getCollection<IMenu>(url, params, isMenu);
  }

  /**
   * Get menu by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {IMenuGetParams} [params]
   * @returns {Observable<AxiosResponse<IContainer<IMenu>>>}
   *
   * @memberof MenuEndpoint
   */
  get(group_id: number, id: number, params?: IMenuGetParams): Observable<AxiosResponse<IContainer<IMenu>>> {
    // console.log('get(group_id, id, params?)', group_id, id, params);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    // console.log('url', url);
    const promise = this.api.http.get(url);
    return this.getContainer<IMenu>(promise, isMenu);
  }

  /**
   * Update menu by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {IMenuSpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<IMenu>>>}
   *
   * @memberof MenuEndpoint
   */
  update(group_id: number, id: number, specs: IMenuSpecs): Observable<AxiosResponse<IContainer<IMenu>>> {
    // console.log('update(group_id, id, specs)', group_id, id, specs);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<IMenu>(promise, isMenu);
  }

}
