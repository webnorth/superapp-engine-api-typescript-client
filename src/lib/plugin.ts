import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, IPlugin, IPluginEndpoint } from './common/interfaces';
import { isPlugin } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * Plugin endpoint
 *
 * @class PluginEndpoint
 * @extends {Endpoint}
 * @implements {IPluginEndpoint}
 */
export class PluginEndpoint extends Endpoint implements IPluginEndpoint {

  /**
   * Creates an instance of PluginEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof PluginEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/plugins');
  }

  /**
   * List all plugins.
   *
   * @param {ICollectionParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<IPlugin>>>}
   *
   * @memberof PluginEndpoint
   */
  list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IPlugin>>> {
    // console.log('list(params?: ICollectionParams)', params);
    const url: string = this.prefix;
    return this.getCollection<IPlugin>(url, params, isPlugin);
  }

  /**
   * Get plugin by id.
   *
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<IPlugin>>>}
   *
   * @memberof PluginEndpoint
   */
  get(id: number): Observable<AxiosResponse<IContainer<IPlugin>>> {
    // console.log('get(id: number)', id);
    const url = `${this.prefix}/${id}`;
    const promise = this.api.http.get(url);
    return this.getContainer<IPlugin>(promise, isPlugin);
  }

}
