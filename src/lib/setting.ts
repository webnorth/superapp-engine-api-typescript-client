import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * Setting object
 *
 * @export
 * @interface ISetting
 */
export interface ISetting {
  value: string | null;
}

/**
 * Setting endpoint methods
 *
 * @export
 * @interface ISettingEndpoint
 */
export interface ISettingEndpoint {
  public(key: string): Observable<AxiosResponse<IContainer<ISetting>>>;
}

/**
 * Setting endpoint
 *
 * @class SettingEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {ISettingEndpoint}
 */
export class SettingEndpoint extends Endpoint implements ISettingEndpoint {

  /**
   * Creates an instance of SettingEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof SettingEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/settings');
  }

  /**
   * Guard of ISetting.
   *
   * @param {*} data
   * @returns {data is ISetting}
   */
  isSetting(data): data is ISetting {
    if (data === null || typeof data !== 'object') {
      console.error('ISetting must be an object', data);
      return false;
    }
    if (typeof data.value !== 'string' && data.value !== null) {
      console.error('ISetting.value must be a string or NULL', data);
      return false;
    }
    return true;
  }

  /**
   * Get permission by key.
   *
   * @param {string} key
   * @returns {Observable<AxiosResponse<IContainer<ISetting>>>}
   *
   * @memberof SettingEndpoint
   */
  public(key: string): Observable<AxiosResponse<IContainer<ISetting>>> {
    // console.log('public(key: string)', key);
    const url = `${this.prefix}/public/${key}`;
    // console.log('url', url);
    const promise = this.api.http.get(url);
    return this.getContainer<ISetting>(promise, this.isSetting);
  }

}
