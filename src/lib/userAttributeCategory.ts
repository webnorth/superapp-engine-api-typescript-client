import { Observable } from 'rxjs';

import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICategory, ICategorySpecs, ICategoryGetParams, ICategoryListParams, } from './common/interfaces';
import { isCategory } from './common/type.guards';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';

/**
 * UserAttributeCategory endpoint methods
 *
 * @export
 * @interface IUserAttributeCategoryEndpoint
 */
export interface IUserAttributeCategoryEndpoint {
  list(group_id: number, params?: ICategoryListParams): Observable<AxiosResponse<ICollection<ICategory>>>;
  create(group_id: number, specs: ICategorySpecs): Observable<AxiosResponse<IContainer<ICategory>>>;
  get(group_id: number, id: number, params?: ICategoryGetParams): Observable<AxiosResponse<IContainer<ICategory>>>;
  update(group_id: number, id: number, specs: ICategorySpecs): Observable<AxiosResponse<IContainer<ICategory>>>;
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}

/**
 * UserAttributeCategory endpoint
 *
 * @class UserAttributeCategoryEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IUserAttributeCategoryEndpoint}
 */
export class UserAttributeCategoryEndpoint extends Endpoint implements IUserAttributeCategoryEndpoint {

  /**
   * Creates an instance of UserAttributeCategoryEndpoint.
   * @param {SuperappEngine} superappEngine
   *
   * @memberof UserAttributeCategoryEndpoint
   */
  constructor(superappEngine: SuperappEngine) {
    // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
    super(superappEngine, '/groups/:group_id/users/attributes/categories');
  }

  /**
   * List all user attribute categories.
   *
   * @param {number} group_id
   * @param {ICategoryListParams} [params]
   * @returns {Observable<AxiosResponse<ICollection<ICategory>>>}
   *
   * @memberof UserAttributeCategoryEndpoint
   */
  list(group_id: number, params?: ICategoryListParams): Observable<AxiosResponse<ICollection<ICategory>>> {
    // console.log('list(group_id, params?)', group_id, params);
    const url: string = this.replacePrefixSegments({ group_id });
    // console.log('url', url);
    return this.getCollection<ICategory>(url, params, isCategory);
  }

  /**
   * Create new user attribute category.
   *
   * @param {number} group_id
   * @param {ICategorySpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<ICategory>>>}
   *
   * @memberof UserAttributeCategoryEndpoint
   */
  create(group_id: number, specs: ICategorySpecs): Observable<AxiosResponse<IContainer<ICategory>>> {
    // console.log('create(group_id, specs)', group_id, specs);
    const url: string = this.replacePrefixSegments({ group_id });
    const promise = this.api.http.post(url, specs);
    return this.getContainer<ICategory>(promise, isCategory);
  }

  /**
   * Get user attribute category by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {ICategoryGetParams} [params]
   * @returns {Observable<AxiosResponse<IContainer<ICategory>>>}
   *
   * @memberof UserAttributeCategoryEndpoint
   */
  get(group_id: number, id: number, params?: ICategoryGetParams): Observable<AxiosResponse<IContainer<ICategory>>> {
    // console.log('get(group_id, id, params?)', group_id, id, params);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    // console.log('url', url);
    const promise = this.api.http.get(url, { params });
    return this.getContainer<ICategory>(promise, isCategory);
  }

  /**
   * Get public user attribute category by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {ICategoryGetParams} [params]
   * @returns {Observable<AxiosResponse<IContainer<ICategory>>>}
   *
   * @memberof UserAttributeCategoryEndpoint
   */
  public(group_id: number, id: number, params?: ICategoryGetParams): Observable<AxiosResponse<IContainer<ICategory>>> {
    // console.log('public(group_id, id, params?)', group_id, id, params);
    const url: string = this.replacePrefixSegments({ group_id }) + '/public/' + id;
    // console.log('url', url);
    const promise = this.api.http.get(url, { params });
    return this.getContainer<ICategory>(promise, isCategory);
  }

  /**
   * Update user attribute category by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @param {ICategorySpecs} specs
   * @returns {Observable<AxiosResponse<IContainer<ICategory>>>}
   *
   * @memberof UserAttributeCategoryEndpoint
   */
  update(group_id: number, id: number, specs: ICategorySpecs): Observable<AxiosResponse<IContainer<ICategory>>> {
    // console.log('update(group_id, id, specs)', group_id, id, specs);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    const promise = this.api.http.post(url, specs);
    return this.getContainer<ICategory>(promise, isCategory);
  }

  /**
   * Delete user attribute category by id.
   *
   * @param {number} group_id
   * @param {number} id
   * @returns {Observable<AxiosResponse<IContainer<void>>>}
   *
   * @memberof UserAttributeCategoryEndpoint
   */
  delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>> {
    // console.log('delete(group_id, id)', group_id, id);
    const url: string = this.replacePrefixSegments({ group_id }) + '/' + id;
    const promise = this.api.http.delete(url);
    return this.getContainer<void>(promise);
  }

}
