"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Auth endpoint
 *
 * @class AuthEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IAuthEndpoint}
 */
var AuthEndpoint = /** @class */ (function (_super) {
    __extends(AuthEndpoint, _super);
    /**
     * Creates an instance of AuthEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof AuthEndpoint
     */
    function AuthEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/auth') || this;
    }
    /**
     * Register.
     *
     * @param {IAuthRegisterSpecs|IAuthGuestRegisterSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IAuthUser>>>}
     *
     * @memberof AuthEndpoint
     */
    AuthEndpoint.prototype.register = function (specs) {
        // console.log('register(specs)', specs);
        var url = this.prefix + "/register";
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isAuthUser);
    };
    /**
     * Request verification code.
     *
     * @param {IAuthReminderParams} params
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    AuthEndpoint.prototype.reminder = function (params) {
        // console.log('reminder(params)', params);
        var url = this.prefix + "/reminder";
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise);
    };
    /**
     * Log in.
     *
     * @param {IAuthLoginParams} params
     * @returns {Observable<AxiosResponse<IContainer<IAuthUser>>>}
     *
     * @memberof AuthEndpoint
     */
    AuthEndpoint.prototype.login = function (params) {
        // console.log('login(params)', params);
        var url = this.prefix + "/login";
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, type_guards_1.isAuthUser);
    };
    /**
     * Profile.
     *
     * @param {IAuthMeParams} params
     * @returns {Observable<AxiosResponse<IContainer<IAuthUser>>>}
     *
     * @memberof AuthEndpoint
     */
    AuthEndpoint.prototype.me = function (params) {
        // console.log('me(params)', params);
        var url = this.prefix + "/me";
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, type_guards_1.isAuthUser);
    };
    /**
     * Update.
     *
     * @param {IAuthUpdateSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IAuthUser>>>}
     *
     * @memberof AuthEndpoint
     */
    AuthEndpoint.prototype.update = function (specs) {
        // console.log('update(specs)', specs);
        var url = this.prefix + "/update";
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isAuthUser);
    };
    /**
     * Quick update.
     *
     * @param {IAuthUpdateSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    AuthEndpoint.prototype.quick_update = function (specs) {
        // console.log('quick_update(specs)', specs);
        var url = this.prefix + "/quick_update";
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise);
    };
    /**
     * Subscribe.
     *
     * @param {IAuthSubscribeParams} params
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    AuthEndpoint.prototype.subscribe = function (params) {
        // console.log('subscribe(params)', params);
        var url = this.prefix + "/subscribe";
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise);
    };
    /**
     * Unsubscribe.
     *
     * @param {IAuthSubscribeParams} params
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    AuthEndpoint.prototype.unsubscribe = function (params) {
        // console.log('unsubscribe(params)', params);
        var url = this.prefix + "/unsubscribe";
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise);
    };
    /**
     * Log out.
     *
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    AuthEndpoint.prototype.logout = function () {
        // console.log('logout()');
        var url = this.prefix + "/logout";
        var promise = this.api.http.get(url);
        return this.getContainer(promise);
    };
    /**
     * Request deletion.
     *
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    AuthEndpoint.prototype.request_deletion = function () {
        // console.log('request_deletion()');
        var url = this.prefix + "/request_deletion";
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    /**
     * Download.
     *
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    AuthEndpoint.prototype.download = function () {
        // console.log('download()');
        var url = this.prefix + "/download";
        var promise = this.api.http.get(url);
        return this.getContainer(promise);
    };
    /**
     * Buy.
     *
     * @param {IAuthBuySpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    AuthEndpoint.prototype.buy = function (specs) {
        // console.log('buy(specs)', specs);
        var url = this.prefix + "/buy";
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise);
    };
    /**
     * Check if product is bought.
     *
     * @param {IAuthIsBoughtParams} params
     * @returns {Observable<AxiosResponse<IContainer<IAuthIsBought>>>}
     *
     * @memberof AuthEndpoint
     */
    AuthEndpoint.prototype.is_bought = function (params) {
        // console.log('is_bought(params)', params);
        var url = this.prefix + "/is_bought";
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, type_guards_1.isAuthPaid);
    };
    return AuthEndpoint;
}(endpoint_1.default));
exports.AuthEndpoint = AuthEndpoint;
//# sourceMappingURL=auth.js.map