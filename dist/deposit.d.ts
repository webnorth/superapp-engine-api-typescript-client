import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, IDeposit, IDepositListParams, IAuthDepositListParams, IDepositSpecs, IDepositUpdateSpecs, IDepositEndpoint } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Deposit endpoint
 *
 * @class DepositEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IDepositEndpoint}
 */
export declare class DepositEndpoint extends Endpoint implements IDepositEndpoint {
    /**
     * Creates an instance of DepositEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof DepositEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List auth deposits.
     *
     * @param {IAuthDepositListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IDeposit>>>}
     *
     * @memberof DepositEndpoint
     */
    authList(params?: IAuthDepositListParams): Observable<AxiosResponse<ICollection<IDeposit>>>;
    /**
     * List all deposits.
     *
     * @param {number} group_id
     * @param {IDepositListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IDeposit>>>}
     *
     * @memberof DepositEndpoint
     */
    list(group_id: number, params?: IDepositListParams): Observable<AxiosResponse<ICollection<IDeposit>>>;
    /**
     * Create new deposit.
     *
     * @param {number} group_id
     * @param {IDepositSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IDeposit>>>}
     *
     * @memberof DepositEndpoint
     */
    create(group_id: number, specs: IDepositSpecs): Observable<AxiosResponse<IContainer<IDeposit>>>;
    /**
     * Update deposit by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IDepositUpdateSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IDeposit>>>}
     *
     * @memberof DepositEndpoint
     */
    update(group_id: number, id: number, specs: IDepositUpdateSpecs): Observable<AxiosResponse<IContainer<IDeposit>>>;
    /**
     * Delete deposit by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof DepositEndpoint
     */
    delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}
