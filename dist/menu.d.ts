import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, ILangParam, IDestination, IDestinationSpecs } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Menu raw object
 *
 * @export
 * @interface IMenu
 */
export interface IMenu {
    id: number;
    name: string;
    slug: string;
    enabled: boolean;
    created_at: string;
    updated_at: string;
    destinations: IDestination[];
}
/**
 * Menu specs
 *
 * @export
 * @interface IMenuSpecs
 */
export interface IMenuSpecs extends ILangParam {
    name: string;
    slug?: string;
    enabled?: boolean;
    destinations?: IDestinationSpecs[];
}
/**
 * Menu get params
 *
 * @export
 * @interface IMenuGetParams
 */
export interface IMenuGetParams extends ILangParam {
}
/**
 * Menu list params
 *
 * @export
 * @interface IMenuListParams
 */
export interface IMenuListParams extends ICollectionParams, IMenuGetParams {
}
/**
 * Menu endpoint methods
 *
 * @export
 * @interface IMenuEndpoint
 */
export interface IMenuEndpoint {
    list(group_id: number, params?: IMenuListParams): Observable<AxiosResponse<ICollection<IMenu>>>;
    create(group_id: number, specs: IMenuSpecs): Observable<AxiosResponse<IContainer<IMenu>>>;
    get(group_id: number, id: number, params?: IMenuGetParams): Observable<AxiosResponse<IContainer<IMenu>>>;
    update(group_id: number, id: number, specs: IMenuSpecs): Observable<AxiosResponse<IContainer<IMenu>>>;
    delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}
/**
 * Guard of IMenu.
 *
 * @export
 * @param {*} data
 * @returns {data is IMenu}
 */
export declare function isMenu(data: any): data is IMenu;
/**
 * Menu endpoint
 *
 * @class MenuEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IMenuEndpoint}
 */
export declare class MenuEndpoint extends Endpoint implements IMenuEndpoint {
    /**
     * Creates an instance of MenuEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof MenuEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * Create new menu.
     *
     * @param {number} group_id
     * @param {IMenuSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IMenu>>>}
     *
     * @memberof MenuEndpoint
     */
    create(group_id: number, specs: IMenuSpecs): Observable<AxiosResponse<IContainer<IMenu>>>;
    /**
     * Delete menu by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof MenuEndpoint
     */
    delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
    /**
     * List all menus.
     *
     * @param {number} group_id
     * @param {IMenuListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IMenu>>>}
     *
     * @memberof MenuEndpoint
     */
    list(group_id: number, params?: IMenuListParams): Observable<AxiosResponse<ICollection<IMenu>>>;
    /**
     * Get menu by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IMenuGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<IMenu>>>}
     *
     * @memberof MenuEndpoint
     */
    get(group_id: number, id: number, params?: IMenuGetParams): Observable<AxiosResponse<IContainer<IMenu>>>;
    /**
     * Update menu by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IMenuSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IMenu>>>}
     *
     * @memberof MenuEndpoint
     */
    update(group_id: number, id: number, specs: IMenuSpecs): Observable<AxiosResponse<IContainer<IMenu>>>;
}
