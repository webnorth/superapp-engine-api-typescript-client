"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * User endpoint
 *
 * @class UserEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IUserEndpoint}
 */
var UserEndpoint = /** @class */ (function (_super) {
    __extends(UserEndpoint, _super);
    /**
     * Creates an instance of UserEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof UserEndpoint
     */
    function UserEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/users') || this;
    }
    /**
     * Create new user.
     *
     * @param {IUserSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IUser>>>}
     *
     * @memberof UserEndpoint
     */
    UserEndpoint.prototype.create = function (specs) {
        // console.log('create(specs: IUserSpecs)', specs);
        var url = this.prefix;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isUser);
    };
    /**
     * Delete user by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof UserEndpoint
     */
    UserEndpoint.prototype.delete = function (id) {
        // console.log('delete(id: number)', id);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    /**
     * List all users.
     *
     * @param {ICollectionParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IUser>>>}
     *
     * @memberof UserEndpoint
     */
    UserEndpoint.prototype.list = function (params) {
        // console.log('list(params?: ICollectionParams)', params);
        var url = this.prefix;
        return this.getCollection(url, params, type_guards_1.isUser);
    };
    /**
     * Get user by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<IUser>>>}
     *
     * @memberof UserEndpoint
     */
    UserEndpoint.prototype.get = function (id) {
        // console.log('get(id: number)', id);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.get(url);
        return this.getContainer(promise, type_guards_1.isUser);
    };
    /**
     * Update user by id.
     *
     * @param {number} id
     * @param {IUserSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IUser>>>}
     *
     * @memberof UserEndpoint
     */
    UserEndpoint.prototype.update = function (id, specs) {
        // console.log('update(id: number, specs: IUserSpecs)', id, specs);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isUser);
    };
    return UserEndpoint;
}(endpoint_1.default));
exports.UserEndpoint = UserEndpoint;
//# sourceMappingURL=user.js.map