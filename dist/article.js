"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Guard of IArticle.
 *
 * @export
 * @param {*} data
 * @returns {data is IArticle}
 */
function isArticle(data) {
    if (data === null || typeof data !== 'object') {
        console.error('IArticle must be an object', data);
        return false;
    }
    if (typeof data.id !== 'number') {
        console.error('IArticle.id must be a number', data);
        return false;
    }
    if (typeof data.name !== 'string') {
        console.error('IArticle.name must be a string', data);
        return false;
    }
    if (typeof data.description !== 'string' && data.description !== null) {
        console.error('IArticle.description must be a string or NULL', data);
        return false;
    }
    if (typeof data.body !== 'string' && data.body !== null) {
        console.error('IArticle.body must be a string or NULL', data);
        return false;
    }
    if (typeof data.sort_order !== 'number') {
        console.error('IArticle.sort_order must be a number', data);
        return false;
    }
    if (!Array.isArray(data.categories)) {
        console.error('IArticle.categories must be an array', data);
        return false;
    }
    if (!data.categories.every(type_guards_1.isCategory)) {
        return false;
    }
    // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
    if (typeof data.publish_at !== 'string' && data.publish_at !== null) {
        console.error('IArticle.publish_at must be a string or NULL', data);
        return false;
    }
    // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
    if (typeof data.publish_until !== 'string' && data.publish_until !== null) {
        console.error('IArticle.publish_until must be a string or NULL', data);
        return false;
    }
    if (data.attributes && !type_guards_1.isAttributeValues(data.attributes)) {
        return false;
    }
    return true;
}
exports.isArticle = isArticle;
/**
 * Article endpoint
 *
 * @class ArticleEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IArticleEndpoint}
 */
var ArticleEndpoint = /** @class */ (function (_super) {
    __extends(ArticleEndpoint, _super);
    /**
     * Creates an instance of ArticleEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof ArticleEndpoint
     */
    function ArticleEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/groups/:group_id/articles') || this;
    }
    /**
     * List all articles.
     *
     * @param {number} group_id
     * @param {IArticleListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IArticle>>>}
     *
     * @memberof ArticleEndpoint
     */
    ArticleEndpoint.prototype.list = function (group_id, params) {
        // console.log('list(group_id, params?)', group_id, params);
        var url = this.replacePrefixSegments({ group_id: group_id });
        // console.log('url', url);
        return this.getCollection(url, params, isArticle);
    };
    /**
     * Create new article.
     *
     * @param {number} group_id
     * @param {IArticleSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IArticle>>>}
     *
     * @memberof ArticleEndpoint
     */
    ArticleEndpoint.prototype.create = function (group_id, specs) {
        // console.log('create(group_id, specs)', group_id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id });
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, isArticle);
    };
    /**
     * Get article by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IArticleGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<IArticle>>>}
     *
     * @memberof ArticleEndpoint
     */
    ArticleEndpoint.prototype.get = function (group_id, id, params) {
        // console.log('get(group_id, id, params?)', group_id, id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, isArticle);
    };
    /**
     * Update article by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IArticleSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IArticle>>>}
     *
     * @memberof ArticleEndpoint
     */
    ArticleEndpoint.prototype.update = function (group_id, id, specs) {
        // console.log('update(group_id, id, specs)', group_id, id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, isArticle);
    };
    /**
     * Delete article by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof ArticleEndpoint
     */
    ArticleEndpoint.prototype.delete = function (group_id, id) {
        // console.log('get(group_id, id)', group_id, id);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    return ArticleEndpoint;
}(endpoint_1.default));
exports.ArticleEndpoint = ArticleEndpoint;
//# sourceMappingURL=article.js.map