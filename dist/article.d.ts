import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, ILangParam, IAttributeValues, ICategory } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Article raw object
 *
 * @export
 * @interface IArticle
 */
export interface IArticle {
    id: number;
    name: string;
    description: string;
    body: string;
    sort_order: number;
    categories: ICategory[];
    publish_at: string;
    publish_until: string;
    attributes?: IAttributeValues;
}
/**
 * Article specs
 *
 * @export
 * @interface IArticleSpecs
 */
export interface IArticleSpecs extends ILangParam {
    name: string;
    description?: string;
    body?: string;
    categories?: string;
    publish_at?: string;
    publish_until?: string;
}
/**
 * Article get params
 *
 * @export
 * @interface IArticleGetParams
 */
export interface IArticleGetParams extends ILangParam {
    with_attributes?: 0 | 1;
    expand_attributes?: 0 | 1;
}
/**
 * Article list params
 *
 * @export
 * @interface IArticleListParams
 */
export interface IArticleListParams extends ICollectionParams, IArticleGetParams {
    group_plugin_id?: number;
    categories?: string;
    upcoming?: 0 | 1;
    sort?: string;
}
/**
 * Article endpoint methods
 *
 * @export
 * @interface IArticleEndpoint
 */
export interface IArticleEndpoint {
    list(group_id: number, params?: IArticleListParams): Observable<AxiosResponse<ICollection<IArticle>>>;
    create(group_id: number, specs: IArticleSpecs): Observable<AxiosResponse<IContainer<IArticle>>>;
    get(group_id: number, id: number, params?: IArticleGetParams): Observable<AxiosResponse<IContainer<IArticle>>>;
    update(group_id: number, id: number, specs: IArticleSpecs): Observable<AxiosResponse<IContainer<IArticle>>>;
    delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}
/**
 * Guard of IArticle.
 *
 * @export
 * @param {*} data
 * @returns {data is IArticle}
 */
export declare function isArticle(data: any): data is IArticle;
/**
 * Article endpoint
 *
 * @class ArticleEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IArticleEndpoint}
 */
export declare class ArticleEndpoint extends Endpoint implements IArticleEndpoint {
    /**
     * Creates an instance of ArticleEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof ArticleEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List all articles.
     *
     * @param {number} group_id
     * @param {IArticleListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IArticle>>>}
     *
     * @memberof ArticleEndpoint
     */
    list(group_id: number, params?: IArticleListParams): Observable<AxiosResponse<ICollection<IArticle>>>;
    /**
     * Create new article.
     *
     * @param {number} group_id
     * @param {IArticleSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IArticle>>>}
     *
     * @memberof ArticleEndpoint
     */
    create(group_id: number, specs: IArticleSpecs): Observable<AxiosResponse<IContainer<IArticle>>>;
    /**
     * Get article by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IArticleGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<IArticle>>>}
     *
     * @memberof ArticleEndpoint
     */
    get(group_id: number, id: number, params?: IArticleGetParams): Observable<AxiosResponse<IContainer<IArticle>>>;
    /**
     * Update article by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IArticleSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IArticle>>>}
     *
     * @memberof ArticleEndpoint
     */
    update(group_id: number, id: number, specs: IArticleSpecs): Observable<AxiosResponse<IContainer<IArticle>>>;
    /**
     * Delete article by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof ArticleEndpoint
     */
    delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}
