import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, ILangParam, IAttributeValues } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Group object
 *
 * @export
 * @interface IGroup
 */
export interface IGroup {
    id: number;
    name: string;
    domain: string | null;
    subdomain: string | null;
    short_description: string;
    description: string;
    published: 0 | 1;
    public: 0 | 1;
    protected: 0 | 1;
    attributes?: IAttributeValues;
}
/**
 * Group specs
 *
 * @export
 * @interface IGroupSpecs
 */
export interface IGroupSpecs extends ILangParam {
    name: string;
    domain: string;
    subdomain: string;
    short_description?: string;
    description?: string;
    published?: 0 | 1;
    protected?: 0 | 1;
    public?: 0 | 1;
}
/**
 * Group get params
 *
 * @export
 * @interface IGroupGetParams
 */
export interface IGroupGetParams extends ILangParam {
    with_attributes?: 0 | 1;
    expand_attributes?: 0 | 1;
}
/**
 * Group list params
 *
 * @export
 * @interface IGroupListParams
 */
export interface IGroupListParams extends ICollectionParams, IGroupGetParams {
    managed?: 0 | 1;
    subscribed?: 0 | 1;
    public?: 0 | 1;
}
/**
 * Group endpoint methods
 *
 * @export
 * @interface IGroupEndpoint
 */
export interface IGroupEndpoint {
    list(params?: IGroupListParams): Observable<AxiosResponse<ICollection<IGroup>>>;
    create(specs: IGroupSpecs): Observable<AxiosResponse<IContainer<IGroup>>>;
    get(id: number, params?: IGroupGetParams): Observable<AxiosResponse<IContainer<IGroup>>>;
    update(id: number, specs: IGroupSpecs): Observable<AxiosResponse<IContainer<IGroup>>>;
    delete(id: number): Observable<AxiosResponse<IContainer<void>>>;
}
/**
 * Group endpoint
 *
 * @class GroupEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IGroupEndpoint}
 */
export declare class GroupEndpoint extends Endpoint implements IGroupEndpoint {
    /**
     * Creates an instance of GroupEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof GroupEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * Create new group.
     *
     * @param {IGroupSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IGroup>>>}
     *
     * @memberof GroupEndpoint
     */
    create(specs: IGroupSpecs): Observable<AxiosResponse<IContainer<IGroup>>>;
    /**
     * Delete group by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof GroupEndpoint
     */
    delete(id: number): Observable<AxiosResponse<IContainer<void>>>;
    /**
     * List all groups.
     *
     * @param {IGroupListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IGroup>>>}
     *
     * @memberof GroupEndpoint
     */
    list(params?: IGroupListParams): Observable<AxiosResponse<ICollection<IGroup>>>;
    /**
     * Get group by id.
     *
     * @param {number} id
     * @param {IGroupGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<IGroup>>>}
     *
     * @memberof GroupEndpoint
     */
    get(id: number, params?: IGroupGetParams): Observable<AxiosResponse<IContainer<IGroup>>>;
    /**
     * Update group by id.
     *
     * @param {number} id
     * @param {IGroupSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IGroup>>>}
     *
     * @memberof GroupEndpoint
     */
    update(id: number, specs: IGroupSpecs): Observable<AxiosResponse<IContainer<IGroup>>>;
}
