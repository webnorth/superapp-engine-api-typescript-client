import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, IUser, IUserSpecs, IUserEndpoint } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * User endpoint
 *
 * @class UserEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IUserEndpoint}
 */
export declare class UserEndpoint extends Endpoint implements IUserEndpoint {
    /**
     * Creates an instance of UserEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof UserEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * Create new user.
     *
     * @param {IUserSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IUser>>>}
     *
     * @memberof UserEndpoint
     */
    create(specs: IUserSpecs): Observable<AxiosResponse<IContainer<IUser>>>;
    /**
     * Delete user by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof UserEndpoint
     */
    delete(id: number): Observable<AxiosResponse<IContainer<void>>>;
    /**
     * List all users.
     *
     * @param {ICollectionParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IUser>>>}
     *
     * @memberof UserEndpoint
     */
    list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IUser>>>;
    /**
     * Get user by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<IUser>>>}
     *
     * @memberof UserEndpoint
     */
    get(id: number): Observable<AxiosResponse<IContainer<IUser>>>;
    /**
     * Update user by id.
     *
     * @param {number} id
     * @param {IUserSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IUser>>>}
     *
     * @memberof UserEndpoint
     */
    update(id: number, specs: IUserSpecs): Observable<AxiosResponse<IContainer<IUser>>>;
}
