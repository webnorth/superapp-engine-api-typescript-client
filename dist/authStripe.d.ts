import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { ICollection, IContainer, IPaymentMethod, IPaymentIntent, IStripeDeposit, IPaymentMethodSpecs, IStripeDepositSpecs, IAuthStripeEndpoint } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Stripe endpoint
 *
 * @class AuthStripeEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IAuthStripeEndpoint}
 */
export declare class AuthStripeEndpoint extends Endpoint implements IAuthStripeEndpoint {
    /**
     * Creates an instance of AuthStripeEndpoint.
     *
     * @param {SuperappEngine} superappEngine
     *
     * @memberof AuthStripeEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List stripe payment methods.
     *
     * @returns {Observable<AxiosResponse<ICollection<IPaymentMethod>>>}
     *
     * @memberof AuthStripeEndpoint
     */
    list(): Observable<AxiosResponse<ICollection<IPaymentMethod>>>;
    /**
     * Create new stripe payment method.
     *
     * @param {IPaymentMethodSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IPaymentMethod>>>}
     *
     * @memberof AuthStripeEndpoint
     */
    create(specs: IPaymentMethodSpecs): Observable<AxiosResponse<IContainer<void>>>;
    /**
     * Add money.
     *
     * @param {IStripeDepositSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IStripeDeposit>>>}
     *
     * @memberof AuthStripeEndpoint
     */
    deposit(specs: IStripeDepositSpecs): Observable<AxiosResponse<IContainer<IStripeDeposit>>>;
    /**
     * Get stripe payment intent.
     *
     * @returns {Observable<AxiosResponse<IContainer<IPaymentIntent>>>}
     *
     * @memberof AuthStripeEndpoint
     */
    intent(): Observable<AxiosResponse<IContainer<IPaymentIntent>>>;
    /**
     * Delete stripe payment methods.
     *
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthStripeEndpoint
     */
    delete(): Observable<AxiosResponse<IContainer<void>>>;
}
