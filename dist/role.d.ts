import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, IRole, IRoleSpecs, IRoleEndpoint } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Role endpoint
 *
 * @class RoleEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IRoleEndpoint}
 */
export declare class RoleEndpoint extends Endpoint implements IRoleEndpoint {
    /**
     * Creates an instance of RoleEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof RoleEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List all roles.
     *
     * @param {ICollectionParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IRole>>>}
     *
     * @memberof RoleEndpoint
     */
    list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IRole>>>;
    /**
     * Create new role.
     *
     * @param {IRoleSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IRole>>>}
     *
     * @memberof RoleEndpoint
     */
    create(specs: IRoleSpecs): Observable<AxiosResponse<IContainer<IRole>>>;
    /**
     * Get role by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<IRole>>>}
     *
     * @memberof RoleEndpoint
     */
    get(id: number): Observable<AxiosResponse<IContainer<IRole>>>;
    /**
     * Update role by id.
     *
     * @param {number} id
     * @param {IRoleSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IRole>>>}
     *
     * @memberof RoleEndpoint
     */
    update(id: number, specs: IRoleSpecs): Observable<AxiosResponse<IContainer<IRole>>>;
    /**
     * Delete role by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof RoleEndpoint
     */
    delete(id: number): Observable<AxiosResponse<IContainer<void>>>;
}
