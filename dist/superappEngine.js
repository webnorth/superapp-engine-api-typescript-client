"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var api_1 = require("./common/api");
/**
 * Superapp Engine API
 *
 * @export
 * @class SuperappEngine
 * @extends {API}
 */
var SuperappEngine = /** @class */ (function (_super) {
    __extends(SuperappEngine, _super);
    /**
     * Creates an instance of SuperappEngine.
     *
     * @param {IAPISpecs} apiSpecs
     *
     * @memberof SuperappEngine
     */
    function SuperappEngine(apiSpecs) {
        var _this = this;
        // console.log('constructor(apiSpecs IAPISpecs)', apiSpecs);
        var defaultSpecs = {
            headers: {
                'Content-Type': 'application/json',
            },
            host: 'api.appsup.eu',
            invalidResponse: new Error('Invalid API response.'),
            prefix: '/api',
            protocol: 'https',
            timeout: 5000,
        };
        _this = _super.call(this, {
            headers: apiSpecs.headers || defaultSpecs.headers,
            host: apiSpecs.host || defaultSpecs.host,
            invalidResponse: apiSpecs.invalidResponse || defaultSpecs.invalidResponse,
            prefix: apiSpecs.prefix || defaultSpecs.prefix,
            protocol: apiSpecs.protocol || defaultSpecs.protocol,
            timeout: apiSpecs.timeout || defaultSpecs.timeout,
        }) || this;
        _this.http.interceptors.response.use(null, SuperappEngine.errorHandler);
        return _this;
    }
    /**
     * error fn handler interceptor
     *
     * @private
     * @static
     *
     * @memberof SuperappEngine
     */
    SuperappEngine.errorHandler = function (error) {
        var res = error.response;
        if (!res)
            return Promise.reject(error);
        error.message = [
            res.status || '',
            res.statusText || '',
        ].join(' - ');
        if (res.data && res.data.error && res.data.error.message) {
            error.message = [error.message, res.data.error.message].join(' - ');
        }
        return Promise.reject(error);
    };
    return SuperappEngine;
}(api_1.default));
exports.SuperappEngine = SuperappEngine;
exports.default = SuperappEngine;
//# sourceMappingURL=superappEngine.js.map