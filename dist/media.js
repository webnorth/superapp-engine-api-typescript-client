"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Media endpoint
 *
 * @class MediaEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IMediaEndpoint}
 */
var MediaEndpoint = /** @class */ (function (_super) {
    __extends(MediaEndpoint, _super);
    /**
     * Creates an instance of MediaEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof MediaEndpoint
     */
    function MediaEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/media') || this;
    }
    /**
     * List all media files.
     *
     * @param {IMediaListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IMediaLike>>>}
     *
     * @memberof MediaEndpoint
     */
    MediaEndpoint.prototype.list = function (params) {
        // console.log('list(params?: IMediaListParams)', params);
        var url = this.prefix;
        return this.getCollection(url, params, type_guards_1.isMediaLike);
    };
    /**
     * Upload new media file.
     *
     * @param {IMediaUploadSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IMediaLike>>>}
     *
     * @memberof MediaEndpoint
     */
    MediaEndpoint.prototype.upload = function (specs) {
        // console.log('upload(specs: IMediaUploadSpecs)', specs);
        var url = this.prefix;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isMediaLike);
    };
    /**
     * Get media file by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<IMediaLike>>>}
     *
     * @memberof MediaEndpoint
     */
    MediaEndpoint.prototype.get = function (id) {
        // console.log('get(id: number)', id);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.get(url);
        return this.getContainer(promise, type_guards_1.isMediaLike);
    };
    /**
     * Update media file by id.
     *
     * @param {number} id
     * @param {IMediaSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IMediaLike>>>}
     *
     * @memberof MediaEndpoint
     */
    MediaEndpoint.prototype.update = function (id, specs) {
        // console.log('update(id: number, specs: IMediaSpecs)', id, specs);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isMediaLike);
    };
    /**
     * Delete media file by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof MediaEndpoint
     */
    MediaEndpoint.prototype.delete = function (id) {
        // console.log('delete(id: number)', id);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    return MediaEndpoint;
}(endpoint_1.default));
exports.MediaEndpoint = MediaEndpoint;
//# sourceMappingURL=media.js.map