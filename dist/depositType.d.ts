import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, IDepositType, IDepositTypeSpecs, IDepositTypeGetParams, IDepositTypeListParams, IDepositTypeUpdateSpecs, IDepositTypeEndpoint } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * DepositType endpoint
 *
 * @class DepositTypeEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IDepositTypeEndpoint}
 */
export declare class DepositTypeEndpoint extends Endpoint implements IDepositTypeEndpoint {
    /**
     * Creates an instance of DepositTypeEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof DepositTypeEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List all deposit types.
     *
     * @param {number} group_id
     * @param {IDepositTypeListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IDepositType>>>}
     *
     * @memberof DepositTypeEndpoint
     */
    list(group_id: number, params?: IDepositTypeListParams): Observable<AxiosResponse<ICollection<IDepositType>>>;
    /**
     * Create new deposit type.
     *
     * @param {number} group_id
     * @param {IDepositTypeSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IDepositType>>>}
     *
     * @memberof DepositTypeEndpoint
     */
    create(group_id: number, specs: IDepositTypeSpecs): Observable<AxiosResponse<IContainer<IDepositType>>>;
    /**
     * Get deposit type by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IDepositTypeGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<IDepositType>>>}
     *
     * @memberof DepositTypeEndpoint
     */
    get(group_id: number, id: number, params?: IDepositTypeGetParams): Observable<AxiosResponse<IContainer<IDepositType>>>;
    /**
     * Update deposit type by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IDepositTypeUpdateSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IDepositType>>>}
     *
     * @memberof DepositTypeEndpoint
     */
    update(group_id: number, id: number, specs: IDepositTypeUpdateSpecs): Observable<AxiosResponse<IContainer<IDepositType>>>;
    /**
     * Delete deposit type by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof DepositTypeEndpoint
     */
    delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}
