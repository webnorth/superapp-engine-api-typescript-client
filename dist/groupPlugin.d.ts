import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, IAttributeValues, ILangParam } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * GroupPlugin raw object
 *
 * @export
 * @interface IGroupPlugin
 */
export interface IGroupPlugin {
    id: number;
    plugin_id: number | null;
    name: string;
    description: string;
    icon: string;
    key: string | null;
    entity_id: number;
    prefix: string;
    attributes?: IAttributeValues;
    created_at: string;
    updated_at: string;
}
/**
 * GroupPlugin get params
 *
 * @export
 * @interface IGroupPluginGetParams
 */
export interface IGroupPluginGetParams extends ILangParam {
    with_attributes?: 0 | 1;
}
/**
 * GroupPlugin list params
 *
 * @export
 * @interface IGroupPluginListParams
 */
export interface IGroupPluginListParams extends ICollectionParams, IGroupPluginGetParams {
    prefix?: string;
}
/**
 * GroupPlugin specs
 *
 * @export
 * @interface IGroupPluginSpecs
 */
export interface IGroupPluginSpecs extends IGroupPluginGetParams {
    plugin_id?: number;
    name: string;
    description?: string;
    icon?: string;
    prefix?: string;
}
/**
 * GroupPlugin update specs
 *
 * @export
 * @interface IGroupPluginUpdateSpecs
 */
export interface IGroupPluginUpdateSpecs extends IGroupPluginGetParams {
    name?: string;
    description?: string;
    icon?: string;
    prefix?: string;
}
/**
 * GroupPlugin endpoint methods
 *
 * @export
 * @interface IGroupPluginEndpoint
 */
export interface IGroupPluginEndpoint {
    isGroupPlugin(data: any): data is IGroupPlugin;
    list(group_id: number, params?: IGroupPluginListParams): Observable<AxiosResponse<ICollection<IGroupPlugin>>>;
    create(group_id: number, specs: IGroupPluginSpecs): Observable<AxiosResponse<IContainer<IGroupPlugin>>>;
    get(group_id: number, id: number, params?: IGroupPluginGetParams): Observable<AxiosResponse<IContainer<IGroupPlugin>>>;
    update(group_id: number, id: number, specs: IGroupPluginUpdateSpecs): Observable<AxiosResponse<IContainer<IGroupPlugin>>>;
    delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}
/**
 * GroupPlugin endpoint
 *
 * @class GroupPluginEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IGroupPluginEndpoint}
 */
export declare class GroupPluginEndpoint extends Endpoint implements IGroupPluginEndpoint {
    /**
     * Creates an instance of GroupPluginEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof GroupPluginEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * Guard of IGroupPlugin.
     *
     * @param {*} data
     * @returns {data is IGroupPlugin}
     */
    isGroupPlugin(data: any): data is IGroupPlugin;
    /**
     * List all group plugins.
     *
     * @param {number} group_id
     * @param {IGroupPluginListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IGroupPlugin>>>}
     *
     * @memberof GroupPluginEndpoint
     */
    list(group_id: number, params?: IGroupPluginListParams): Observable<AxiosResponse<ICollection<IGroupPlugin>>>;
    /**
     * Create new group plugin.
     *
     * @param {number} group_id
     * @param {IGroupPluginSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IGroupPlugin>>>}
     *
     * @memberof GroupPluginEndpoint
     */
    create(group_id: number, specs: IGroupPluginSpecs): Observable<AxiosResponse<IContainer<IGroupPlugin>>>;
    /**
     * Get group plugin by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IGroupPluginGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<IGroupPlugin>>>}
     *
     * @memberof GroupPluginEndpoint
     */
    get(group_id: number, id: number, params?: IGroupPluginGetParams): Observable<AxiosResponse<IContainer<IGroupPlugin>>>;
    /**
     * Update group plugin by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IGroupPluginUpdateSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IGroupPlugin>>>}
     *
     * @memberof GroupPluginEndpoint
     */
    update(group_id: number, id: number, specs: IGroupPluginUpdateSpecs): Observable<AxiosResponse<IContainer<IGroupPlugin>>>;
    /**
     * Delete group plugin by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof GroupPluginEndpoint
     */
    delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}
