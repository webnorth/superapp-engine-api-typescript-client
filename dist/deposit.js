"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Deposit endpoint
 *
 * @class DepositEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IDepositEndpoint}
 */
var DepositEndpoint = /** @class */ (function (_super) {
    __extends(DepositEndpoint, _super);
    /**
     * Creates an instance of DepositEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof DepositEndpoint
     */
    function DepositEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        // TODO: The correct prefix
        return _super.call(this, superappEngine, '/groups/:group_id/deposits') || this;
    }
    /**
     * List auth deposits.
     *
     * @param {IAuthDepositListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IDeposit>>>}
     *
     * @memberof DepositEndpoint
     */
    DepositEndpoint.prototype.authList = function (params) {
        // console.log('list(params?: IAuthDepositListParams)', params);
        var url = '/auth/deposits';
        // console.log('url', url);
        return this.getCollection(url, params, type_guards_1.isDeposit);
    };
    /**
     * List all deposits.
     *
     * @param {number} group_id
     * @param {IDepositListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IDeposit>>>}
     *
     * @memberof DepositEndpoint
     */
    DepositEndpoint.prototype.list = function (group_id, params) {
        // console.log('list(group_id: number, params?: IDepositListParams)', group_id, params);
        var url = this.replacePrefixSegments({ group_id: group_id });
        // console.log('url', url);
        return this.getCollection(url, params, type_guards_1.isDeposit);
    };
    /**
     * Create new deposit.
     *
     * @param {number} group_id
     * @param {IDepositSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IDeposit>>>}
     *
     * @memberof DepositEndpoint
     */
    DepositEndpoint.prototype.create = function (group_id, specs) {
        // console.log('create(group_id: number, specs: IDepositSpecs)', group_id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id });
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isDeposit);
    };
    /**
     * Update deposit by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IDepositUpdateSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IDeposit>>>}
     *
     * @memberof DepositEndpoint
     */
    DepositEndpoint.prototype.update = function (group_id, id, specs) {
        // console.log('update(group_id: number, id: number, specs: IDepositUpdateSpecs)', group_id, id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isDeposit);
    };
    /**
     * Delete deposit by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof DepositEndpoint
     */
    DepositEndpoint.prototype.delete = function (group_id, id) {
        // console.log('get(group_id: number, id: number)', group_id, id);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    return DepositEndpoint;
}(endpoint_1.default));
exports.DepositEndpoint = DepositEndpoint;
//# sourceMappingURL=deposit.js.map