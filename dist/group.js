"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Group endpoint
 *
 * @class GroupEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IGroupEndpoint}
 */
var GroupEndpoint = /** @class */ (function (_super) {
    __extends(GroupEndpoint, _super);
    /**
     * Creates an instance of GroupEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof GroupEndpoint
     */
    function GroupEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/groups') || this;
    }
    /**
     * Create new group.
     *
     * @param {IGroupSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IGroup>>>}
     *
     * @memberof GroupEndpoint
     */
    GroupEndpoint.prototype.create = function (specs) {
        // console.log('create(specs)', specs);
        var url = this.prefix;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isGroup);
    };
    /**
     * Delete group by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof GroupEndpoint
     */
    GroupEndpoint.prototype.delete = function (id) {
        // console.log('delete(id)', id);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    /**
     * List all groups.
     *
     * @param {IGroupListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IGroup>>>}
     *
     * @memberof GroupEndpoint
     */
    GroupEndpoint.prototype.list = function (params) {
        // console.log('list(params?)', params);
        var url = this.prefix;
        return this.getCollection(url, params, type_guards_1.isGroup);
    };
    /**
     * Get group by id.
     *
     * @param {number} id
     * @param {IGroupGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<IGroup>>>}
     *
     * @memberof GroupEndpoint
     */
    GroupEndpoint.prototype.get = function (id, params) {
        // console.log('get(id, params?)', id, params);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, type_guards_1.isGroup);
    };
    /**
     * Update group by id.
     *
     * @param {number} id
     * @param {IGroupSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IGroup>>>}
     *
     * @memberof GroupEndpoint
     */
    GroupEndpoint.prototype.update = function (id, specs) {
        // console.log('update(id, specs)', id, specs);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isGroup);
    };
    return GroupEndpoint;
}(endpoint_1.default));
exports.GroupEndpoint = GroupEndpoint;
//# sourceMappingURL=group.js.map