import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Setting object
 *
 * @export
 * @interface ISetting
 */
export interface ISetting {
    value: string | null;
}
/**
 * Setting endpoint methods
 *
 * @export
 * @interface ISettingEndpoint
 */
export interface ISettingEndpoint {
    public(key: string): Observable<AxiosResponse<IContainer<ISetting>>>;
}
/**
 * Setting endpoint
 *
 * @class SettingEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {ISettingEndpoint}
 */
export declare class SettingEndpoint extends Endpoint implements ISettingEndpoint {
    /**
     * Creates an instance of SettingEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof SettingEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * Guard of ISetting.
     *
     * @param {*} data
     * @returns {data is ISetting}
     */
    isSetting(data: any): data is ISetting;
    /**
     * Get permission by key.
     *
     * @param {string} key
     * @returns {Observable<AxiosResponse<IContainer<ISetting>>>}
     *
     * @memberof SettingEndpoint
     */
    public(key: string): Observable<AxiosResponse<IContainer<ISetting>>>;
}
