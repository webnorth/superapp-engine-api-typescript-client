"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Role endpoint
 *
 * @class RoleEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IRoleEndpoint}
 */
var RoleEndpoint = /** @class */ (function (_super) {
    __extends(RoleEndpoint, _super);
    /**
     * Creates an instance of RoleEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof RoleEndpoint
     */
    function RoleEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/roles') || this;
    }
    /**
     * List all roles.
     *
     * @param {ICollectionParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IRole>>>}
     *
     * @memberof RoleEndpoint
     */
    RoleEndpoint.prototype.list = function (params) {
        // console.log('list(params?: ICollectionParams)', params);
        var url = this.prefix;
        return this.getCollection(url, params, type_guards_1.isRole);
    };
    /**
     * Create new role.
     *
     * @param {IRoleSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IRole>>>}
     *
     * @memberof RoleEndpoint
     */
    RoleEndpoint.prototype.create = function (specs) {
        // console.log('create(specs: IRoleSpecs)', specs);
        var url = this.prefix;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isRole);
    };
    /**
     * Get role by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<IRole>>>}
     *
     * @memberof RoleEndpoint
     */
    RoleEndpoint.prototype.get = function (id) {
        // console.log('get(id: number)', id);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.get(url);
        return this.getContainer(promise, type_guards_1.isRole);
    };
    /**
     * Update role by id.
     *
     * @param {number} id
     * @param {IRoleSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IRole>>>}
     *
     * @memberof RoleEndpoint
     */
    RoleEndpoint.prototype.update = function (id, specs) {
        // console.log('update(id: number, specs: IRoleSpecs)', id, specs);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isRole);
    };
    /**
     * Delete role by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof RoleEndpoint
     */
    RoleEndpoint.prototype.delete = function (id) {
        // console.log('delete(id: number)', id);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    return RoleEndpoint;
}(endpoint_1.default));
exports.RoleEndpoint = RoleEndpoint;
//# sourceMappingURL=role.js.map