import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, IPlugin, IPluginEndpoint } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Plugin endpoint
 *
 * @class PluginEndpoint
 * @extends {Endpoint}
 * @implements {IPluginEndpoint}
 */
export declare class PluginEndpoint extends Endpoint implements IPluginEndpoint {
    /**
     * Creates an instance of PluginEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof PluginEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List all plugins.
     *
     * @param {ICollectionParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IPlugin>>>}
     *
     * @memberof PluginEndpoint
     */
    list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IPlugin>>>;
    /**
     * Get plugin by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<IPlugin>>>}
     *
     * @memberof PluginEndpoint
     */
    get(id: number): Observable<AxiosResponse<IContainer<IPlugin>>>;
}
