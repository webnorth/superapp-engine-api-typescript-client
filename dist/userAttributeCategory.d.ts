import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICategory, ICategorySpecs, ICategoryGetParams, ICategoryListParams } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * UserAttributeCategory endpoint methods
 *
 * @export
 * @interface IUserAttributeCategoryEndpoint
 */
export interface IUserAttributeCategoryEndpoint {
    list(group_id: number, params?: ICategoryListParams): Observable<AxiosResponse<ICollection<ICategory>>>;
    create(group_id: number, specs: ICategorySpecs): Observable<AxiosResponse<IContainer<ICategory>>>;
    get(group_id: number, id: number, params?: ICategoryGetParams): Observable<AxiosResponse<IContainer<ICategory>>>;
    update(group_id: number, id: number, specs: ICategorySpecs): Observable<AxiosResponse<IContainer<ICategory>>>;
    delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}
/**
 * UserAttributeCategory endpoint
 *
 * @class UserAttributeCategoryEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IUserAttributeCategoryEndpoint}
 */
export declare class UserAttributeCategoryEndpoint extends Endpoint implements IUserAttributeCategoryEndpoint {
    /**
     * Creates an instance of UserAttributeCategoryEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof UserAttributeCategoryEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List all user attribute categories.
     *
     * @param {number} group_id
     * @param {ICategoryListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<ICategory>>>}
     *
     * @memberof UserAttributeCategoryEndpoint
     */
    list(group_id: number, params?: ICategoryListParams): Observable<AxiosResponse<ICollection<ICategory>>>;
    /**
     * Create new user attribute category.
     *
     * @param {number} group_id
     * @param {ICategorySpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<ICategory>>>}
     *
     * @memberof UserAttributeCategoryEndpoint
     */
    create(group_id: number, specs: ICategorySpecs): Observable<AxiosResponse<IContainer<ICategory>>>;
    /**
     * Get user attribute category by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ICategoryGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<ICategory>>>}
     *
     * @memberof UserAttributeCategoryEndpoint
     */
    get(group_id: number, id: number, params?: ICategoryGetParams): Observable<AxiosResponse<IContainer<ICategory>>>;
    /**
     * Get public user attribute category by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ICategoryGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<ICategory>>>}
     *
     * @memberof UserAttributeCategoryEndpoint
     */
    public(group_id: number, id: number, params?: ICategoryGetParams): Observable<AxiosResponse<IContainer<ICategory>>>;
    /**
     * Update user attribute category by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ICategorySpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<ICategory>>>}
     *
     * @memberof UserAttributeCategoryEndpoint
     */
    update(group_id: number, id: number, specs: ICategorySpecs): Observable<AxiosResponse<IContainer<ICategory>>>;
    /**
     * Delete user attribute category by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof UserAttributeCategoryEndpoint
     */
    delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}
