import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, IPushToken, IPushTokenSpecs, IPushTokenEndpoint } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * PushToken endpoint
 *
 * @class PushTokenEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IPushTokenEndpoint}
 */
export declare class PushTokenEndpoint extends Endpoint implements IPushTokenEndpoint {
    /**
     * Creates an instance of PushTokenEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof PushTokenEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List all push tokens.
     *
     * @param {ICollectionParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IPushToken>>>}
     *
     * @memberof PushTokenEndpoint
     */
    list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IPushToken>>>;
    /**
     * Create new push token.
     *
     * @param {IPushTokenSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IPushToken>>>}
     *
     * @memberof PushTokenEndpoint
     */
    create(specs: IPushTokenSpecs): Observable<AxiosResponse<IContainer<IPushToken>>>;
    /**
     * Get push token by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<IPushToken>>>}
     *
     * @memberof PushTokenEndpoint
     */
    get(id: number): Observable<AxiosResponse<IContainer<IPushToken>>>;
    /**
     * Update push token by id.
     *
     * @param {number} id
     * @param {IPushTokenSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IPushToken>>>}
     *
     * @memberof PushTokenEndpoint
     */
    update(id: number, specs: IPushTokenSpecs): Observable<AxiosResponse<IContainer<IPushToken>>>;
    /**
     * Delete push token by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof PushTokenEndpoint
     */
    delete(id: number): Observable<AxiosResponse<IContainer<void>>>;
}
