import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, IMediaLike, IMediaListParams, IMediaUploadSpecs, IMediaSpecs, IMediaEndpoint } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Media endpoint
 *
 * @class MediaEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IMediaEndpoint}
 */
export declare class MediaEndpoint extends Endpoint implements IMediaEndpoint {
    /**
     * Creates an instance of MediaEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof MediaEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List all media files.
     *
     * @param {IMediaListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IMediaLike>>>}
     *
     * @memberof MediaEndpoint
     */
    list(params?: IMediaListParams): Observable<AxiosResponse<ICollection<IMediaLike>>>;
    /**
     * Upload new media file.
     *
     * @param {IMediaUploadSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IMediaLike>>>}
     *
     * @memberof MediaEndpoint
     */
    upload(specs: IMediaUploadSpecs): Observable<AxiosResponse<IContainer<IMediaLike>>>;
    /**
     * Get media file by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<IMediaLike>>>}
     *
     * @memberof MediaEndpoint
     */
    get(id: number): Observable<AxiosResponse<IContainer<IMediaLike>>>;
    /**
     * Update media file by id.
     *
     * @param {number} id
     * @param {IMediaSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IMediaLike>>>}
     *
     * @memberof MediaEndpoint
     */
    update(id: number, specs: IMediaSpecs): Observable<AxiosResponse<IContainer<IMediaLike>>>;
    /**
     * Delete media file by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof MediaEndpoint
     */
    delete(id: number): Observable<AxiosResponse<IContainer<void>>>;
}
