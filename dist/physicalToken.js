"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Guard of IPhysicalTokenType.
 *
 * @export
 * @param {*} data
 * @returns {data is IPhysicalTokenType}
 */
function isPhysicalTokenType(data) {
    var types = ['rfid'];
    if (types.indexOf(data) < 0) {
        console.error('IPhysicalTokenType must be one of ' + types.join(', '), data);
        return false;
    }
    return true;
}
exports.isPhysicalTokenType = isPhysicalTokenType;
/**
 * Guard of IPhysicalToken.
 *
 * @export
 * @param {*} data
 * @returns {data is IPhysicalToken}
 */
function isPhysicalToken(data) {
    if (data === null || typeof data !== 'object') {
        console.error('IPhysicalToken must be an object', data);
        return false;
    }
    if (typeof data.id !== 'number') {
        console.error('IPhysicalToken.id must be a number', data);
        return false;
    }
    if (typeof data.token !== 'string') {
        console.error('IPhysicalToken.token must be a string', data);
        return false;
    }
    if (!isPhysicalTokenType(data.type)) {
        return false;
    }
    if (typeof data.user_id !== 'number') {
        console.error('IPhysicalToken.user_id must be a number', data);
        return false;
    }
    return true;
}
exports.isPhysicalToken = isPhysicalToken;
/**
 * PhysicalToken endpoint
 *
 * @class PhysicalTokenEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IPhysicalTokenEndpoint}
 */
var PhysicalTokenEndpoint = /** @class */ (function (_super) {
    __extends(PhysicalTokenEndpoint, _super);
    /**
     * Creates an instance of PhysicalTokenEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof PhysicalTokenEndpoint
     */
    function PhysicalTokenEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/groups/:group_id/users/:user_id_and_token_type/tokens') || this;
    }
    /**
     * List all deposit tokens.
     *
     * @param {number} group_id
     * @param {number} user_id
     * @param {IPhysicalTokenListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IPhysicalToken>>>}
     *
     * @memberof PhysicalTokenEndpoint
     */
    PhysicalTokenEndpoint.prototype.list = function (group_id, user_id, token_type, params) {
        // console.log('list(group_id, user_id, token_type, params)', group_id, user_id, token_type, params);
        var url = this.replacePrefixSegments({ group_id: group_id, 'user_id_and_token_type': user_id + '/' + token_type });
        // console.log('url', url);
        return this.getCollection(url, params, isPhysicalToken);
    };
    /**
     * Create new physical token.
     *
     * @param {number} group_id
     * @param {number} user_id
     * @param {IPhysicalTokenSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IPhysicalToken>>>}
     *
     * @memberof PhysicalTokenEndpoint
     */
    PhysicalTokenEndpoint.prototype.create = function (group_id, user_id, token_type, specs) {
        // console.log('create(group_id, user_id, specs)', group_id, user_id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id, 'user_id_and_token_type': user_id + '/' + token_type });
        // console.log('url', url);
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, isPhysicalToken);
    };
    /**
     * Get user by token string.
     *
     * @param {number} group_id
     * @param {IPhysicalTokenType} token_type
     * @param {string} token
     * @returns {Observable<AxiosResponse<IContainer<IPhysicalToken>>>}
     *
     * @memberof PhysicalTokenEndpoint
     */
    PhysicalTokenEndpoint.prototype.get_user = function (group_id, token_type, token) {
        // console.log('get(group_id, token_type, token)', group_id, token_type, token);
        var url = this.replacePrefixSegments({ group_id: group_id, 'user_id_and_token_type': token_type }) + '/' + token;
        // console.log('url', url);
        var promise = this.api.http.get(url);
        return this.getContainer(promise, type_guards_1.isUserStub);
    };
    /**
     * Delete physical token by token string.
     *
     * @param {number} group_id
     * @param {number} user_id
     * @param {IPhysicalTokenType} token_type
     * @param {string} token
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof PhysicalTokenEndpoint
     */
    PhysicalTokenEndpoint.prototype.delete = function (group_id, user_id, token_type, token) {
        // console.log('get(group_id, user_id, token_type, token)', group_id, user_id, token_type, token);
        var url = this.replacePrefixSegments({ group_id: group_id, 'user_id_and_token_type': user_id + '/' + token_type }) + '/' + token;
        // console.log('url', url);
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    return PhysicalTokenEndpoint;
}(endpoint_1.default));
exports.PhysicalTokenEndpoint = PhysicalTokenEndpoint;
//# sourceMappingURL=physicalToken.js.map