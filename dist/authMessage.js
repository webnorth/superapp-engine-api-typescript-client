"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Message endpoint
 *
 * @class AuthMessageEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IAuthMessageEndpoint}
 */
var AuthMessageEndpoint = /** @class */ (function (_super) {
    __extends(AuthMessageEndpoint, _super);
    /**
     * Creates an instance of AuthMessageEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof AuthMessageEndpoint
     */
    function AuthMessageEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/auth/messages') || this;
    }
    /**
     * List all auth messages.
     *
     * @param {IAuthMessageListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IMessageLike>>>}
     *
     * @memberof AuthMessageEndpoint
     */
    AuthMessageEndpoint.prototype.list = function (params) {
        // console.log('list(params?: IAuthMessageListParams)', params);
        var url = this.prefix;
        return this.getCollection(url, params, type_guards_1.isMessageLike);
    };
    /**
     * Touch auth message by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<IMessageLike>>>}
     *
     * @memberof AuthMessageEndpoint
     */
    AuthMessageEndpoint.prototype.touch = function (id) {
        // console.log('touch(id: number)', id);
        var url = this.prefix + "/" + id + "/touch";
        var promise = this.api.http.get(url);
        return this.getContainer(promise, type_guards_1.isMessageLike);
    };
    return AuthMessageEndpoint;
}(endpoint_1.default));
exports.AuthMessageEndpoint = AuthMessageEndpoint;
//# sourceMappingURL=authMessage.js.map