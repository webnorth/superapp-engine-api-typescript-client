"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Ticket endpoint
 *
 * @class TicketEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {ITicketEndpoint}
 */
var TicketEndpoint = /** @class */ (function (_super) {
    __extends(TicketEndpoint, _super);
    /**
     * Creates an instance of TicketEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof TicketEndpoint
     */
    function TicketEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        // TODO: The correct prefix
        return _super.call(this, superappEngine, '/groups/:group_id/tickets') || this;
    }
    /**
     * List auth tickets.
     *
     * @param {IAuthTicketListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    TicketEndpoint.prototype.authList = function (params) {
        // console.log('list(params?: IAuthTicketListParams)', params);
        var url = '/auth/tickets';
        // console.log('url', url);
        return this.getCollection(url, params, type_guards_1.isTicket);
    };
    /**
     * List all tickets.
     *
     * @param {number} group_id
     * @param {ITicketListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    TicketEndpoint.prototype.list = function (group_id, params) {
        // console.log('list(group_id: number, params?: ITicketListParams)', group_id, params);
        var url = this.replacePrefixSegments({ group_id: group_id });
        // console.log('url', url);
        return this.getCollection(url, params, type_guards_1.isTicket);
    };
    /**
     * Create new ticket.
     *
     * @param {number} group_id
     * @param {ITicketSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    TicketEndpoint.prototype.create = function (group_id, specs) {
        // console.log('create(group_id: number, specs: ITicketSpecs)', group_id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id });
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isTicket);
    };
    /**
     * Check ticket by code.
     *
     * @param {number} group_id
     * @param {ITicketCheckParams} params
     * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    TicketEndpoint.prototype.check = function (group_id, params) {
        // console.log('check(group_id: number, params: ITicketCheckParams)', group_id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/check';
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, type_guards_1.isTicket);
    };
    /**
     * Checkin ticket by code.
     *
     * @param {number} group_id
     * @param {ITicketCheckParams} params
     * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    TicketEndpoint.prototype.checkin = function (group_id, params) {
        // console.log('checkin(group_id: number, params: ITicketCheckParams)', group_id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/checkin';
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, type_guards_1.isTicket);
    };
    /**
     * Get ticket by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ITicketGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    TicketEndpoint.prototype.get = function (group_id, id, params) {
        // console.log('get(group_id: number, id: number, params?: ITicketGetParams)', group_id, id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, type_guards_1.isTicket);
    };
    /**
     * Checkout ticket by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ITicketGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    TicketEndpoint.prototype.checkout = function (group_id, id, params) {
        // console.log('checkout(group_id: number, id: number, params?: ITicketGetParams)', group_id, id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id + '/checkout';
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, type_guards_1.isTicket);
    };
    /**
     * Invalidate ticket by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ITicketGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    TicketEndpoint.prototype.invalidate = function (group_id, id, params) {
        // console.log('invalidate(group_id: number, id: number, params?: ITicketGetParams)', group_id, id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id + '/invalidate';
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, type_guards_1.isTicket);
    };
    /**
     * Delete ticket by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof TicketEndpoint
     */
    TicketEndpoint.prototype.delete_pdf = function (group_id, id) {
        // console.log('get(group_id: number, id: number)', group_id, id);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id + '/delete_pdf';
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    return TicketEndpoint;
}(endpoint_1.default));
exports.TicketEndpoint = TicketEndpoint;
//# sourceMappingURL=ticket.js.map