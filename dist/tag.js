"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Tag endpoint
 *
 * @class TagEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {ITagEndpoint}
 */
var TagEndpoint = /** @class */ (function (_super) {
    __extends(TagEndpoint, _super);
    /**
     * Creates an instance of TagEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof TagEndpoint
     */
    function TagEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/groups/:group_id/users/attributes') || this;
    }
    /**
     * List all group tags.
     *
     * @param {number} group_id
     * @param {ITagsParams} params
     * @returns {Observable<AxiosResponse<ICollection<ITag>>>}
     *
     * @memberof TagEndpoint
     */
    TagEndpoint.prototype.list = function (group_id, params) {
        // console.log('list(group_id, params)', group_id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/tags';
        // console.log('url', url);
        return this.getCollection(url, params, type_guards_1.isTag);
    };
    return TagEndpoint;
}(endpoint_1.default));
exports.TagEndpoint = TagEndpoint;
//# sourceMappingURL=tag.js.map