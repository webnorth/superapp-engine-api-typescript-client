"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * UserAttributeCategory endpoint
 *
 * @class UserAttributeCategoryEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IUserAttributeCategoryEndpoint}
 */
var UserAttributeCategoryEndpoint = /** @class */ (function (_super) {
    __extends(UserAttributeCategoryEndpoint, _super);
    /**
     * Creates an instance of UserAttributeCategoryEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof UserAttributeCategoryEndpoint
     */
    function UserAttributeCategoryEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/groups/:group_id/users/attributes/categories') || this;
    }
    /**
     * List all user attribute categories.
     *
     * @param {number} group_id
     * @param {ICategoryListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<ICategory>>>}
     *
     * @memberof UserAttributeCategoryEndpoint
     */
    UserAttributeCategoryEndpoint.prototype.list = function (group_id, params) {
        // console.log('list(group_id, params?)', group_id, params);
        var url = this.replacePrefixSegments({ group_id: group_id });
        // console.log('url', url);
        return this.getCollection(url, params, type_guards_1.isCategory);
    };
    /**
     * Create new user attribute category.
     *
     * @param {number} group_id
     * @param {ICategorySpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<ICategory>>>}
     *
     * @memberof UserAttributeCategoryEndpoint
     */
    UserAttributeCategoryEndpoint.prototype.create = function (group_id, specs) {
        // console.log('create(group_id, specs)', group_id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id });
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isCategory);
    };
    /**
     * Get user attribute category by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ICategoryGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<ICategory>>>}
     *
     * @memberof UserAttributeCategoryEndpoint
     */
    UserAttributeCategoryEndpoint.prototype.get = function (group_id, id, params) {
        // console.log('get(group_id, id, params?)', group_id, id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        // console.log('url', url);
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, type_guards_1.isCategory);
    };
    /**
     * Get public user attribute category by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ICategoryGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<ICategory>>>}
     *
     * @memberof UserAttributeCategoryEndpoint
     */
    UserAttributeCategoryEndpoint.prototype.public = function (group_id, id, params) {
        // console.log('public(group_id, id, params?)', group_id, id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/public/' + id;
        // console.log('url', url);
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, type_guards_1.isCategory);
    };
    /**
     * Update user attribute category by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ICategorySpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<ICategory>>>}
     *
     * @memberof UserAttributeCategoryEndpoint
     */
    UserAttributeCategoryEndpoint.prototype.update = function (group_id, id, specs) {
        // console.log('update(group_id, id, specs)', group_id, id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isCategory);
    };
    /**
     * Delete user attribute category by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof UserAttributeCategoryEndpoint
     */
    UserAttributeCategoryEndpoint.prototype.delete = function (group_id, id) {
        // console.log('delete(group_id, id)', group_id, id);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    return UserAttributeCategoryEndpoint;
}(endpoint_1.default));
exports.UserAttributeCategoryEndpoint = UserAttributeCategoryEndpoint;
//# sourceMappingURL=userAttributeCategory.js.map