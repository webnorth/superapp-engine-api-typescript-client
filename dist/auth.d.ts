import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, IAuthUser, IAuthRegisterSpecs, IAuthGuestRegisterSpecs, IAuthReminderParams, IAuthLoginParams, IAuthMeParams, IAuthUpdateSpecs, IAuthSubscribeParams, IAuthBuySpecs, IAuthIsBought, IAuthIsBoughtParams, IAuthEndpoint } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Auth endpoint
 *
 * @class AuthEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IAuthEndpoint}
 */
export declare class AuthEndpoint extends Endpoint implements IAuthEndpoint {
    /**
     * Creates an instance of AuthEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof AuthEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * Register.
     *
     * @param {IAuthRegisterSpecs|IAuthGuestRegisterSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IAuthUser>>>}
     *
     * @memberof AuthEndpoint
     */
    register(specs: IAuthRegisterSpecs | IAuthGuestRegisterSpecs): Observable<AxiosResponse<IContainer<IAuthUser>>>;
    /**
     * Request verification code.
     *
     * @param {IAuthReminderParams} params
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    reminder(params: IAuthReminderParams): Observable<AxiosResponse<IContainer<void>>>;
    /**
     * Log in.
     *
     * @param {IAuthLoginParams} params
     * @returns {Observable<AxiosResponse<IContainer<IAuthUser>>>}
     *
     * @memberof AuthEndpoint
     */
    login(params: IAuthLoginParams): Observable<AxiosResponse<IContainer<IAuthUser>>>;
    /**
     * Profile.
     *
     * @param {IAuthMeParams} params
     * @returns {Observable<AxiosResponse<IContainer<IAuthUser>>>}
     *
     * @memberof AuthEndpoint
     */
    me(params: IAuthMeParams): Observable<AxiosResponse<IContainer<IAuthUser>>>;
    /**
     * Update.
     *
     * @param {IAuthUpdateSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IAuthUser>>>}
     *
     * @memberof AuthEndpoint
     */
    update(specs: IAuthUpdateSpecs): Observable<AxiosResponse<IContainer<IAuthUser>>>;
    /**
     * Quick update.
     *
     * @param {IAuthUpdateSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    quick_update(specs: IAuthUpdateSpecs): Observable<AxiosResponse<IContainer<void>>>;
    /**
     * Subscribe.
     *
     * @param {IAuthSubscribeParams} params
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    subscribe(params: IAuthSubscribeParams): Observable<AxiosResponse<IContainer<void>>>;
    /**
     * Unsubscribe.
     *
     * @param {IAuthSubscribeParams} params
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    unsubscribe(params: IAuthSubscribeParams): Observable<AxiosResponse<IContainer<void>>>;
    /**
     * Log out.
     *
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    logout(): Observable<AxiosResponse<IContainer<void>>>;
    /**
     * Request deletion.
     *
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    request_deletion(): Observable<AxiosResponse<IContainer<void>>>;
    /**
     * Download.
     *
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    download(): Observable<AxiosResponse<IContainer<void>>>;
    /**
     * Buy.
     *
     * @param {IAuthBuySpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthEndpoint
     */
    buy(specs: IAuthBuySpecs): Observable<AxiosResponse<IContainer<void>>>;
    /**
     * Check if product is bought.
     *
     * @param {IAuthIsBoughtParams} params
     * @returns {Observable<AxiosResponse<IContainer<IAuthIsBought>>>}
     *
     * @memberof AuthEndpoint
     */
    is_bought(params: IAuthIsBoughtParams): Observable<AxiosResponse<IContainer<IAuthIsBought>>>;
}
