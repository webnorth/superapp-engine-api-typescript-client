import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, IPermission, IPermissionSpecs, IPermissionEndpoint } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Permission endpoint
 *
 * @class PermissionEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IPermissionEndpoint}
 */
export declare class PermissionEndpoint extends Endpoint implements IPermissionEndpoint {
    /**
     * Creates an instance of PermissionEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof PermissionEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * Create new permission.
     *
     * @param {IPermissionSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IPermission>>>}
     *
     * @memberof PermissionEndpoint
     */
    create(specs: IPermissionSpecs): Observable<AxiosResponse<IContainer<IPermission>>>;
    /**
     * Delete permission by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof PermissionEndpoint
     */
    delete(id: number): Observable<AxiosResponse<IContainer<void>>>;
    /**
     * List all permissions.
     *
     * @param {ICollectionParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IPermission>>>}
     *
     * @memberof PermissionEndpoint
     */
    list(params?: ICollectionParams): Observable<AxiosResponse<ICollection<IPermission>>>;
    /**
     * Get permission by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<IPermission>>>}
     *
     * @memberof PermissionEndpoint
     */
    get(id: number): Observable<AxiosResponse<IContainer<IPermission>>>;
    /**
     * Update permission by id.
     *
     * @param {number} id
     * @param {IPermissionSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IPermission>>>}
     *
     * @memberof PermissionEndpoint
     */
    update(id: number, specs: IPermissionSpecs): Observable<AxiosResponse<IContainer<IPermission>>>;
}
