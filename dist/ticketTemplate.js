"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Guard of ITicketTemplate.
 *
 * @export
 * @param {any} data
 * @returns {data is ITicketTemplate}
 */
function isTicketTemplate(data) {
    if (data === null || typeof data !== 'object') {
        console.error('ITicketTemplate must be an object', data);
        return false;
    }
    if (typeof data.id !== 'number') {
        console.error('ITicketTemplate.id must be a number', data);
        return false;
    }
    if (typeof data.name !== 'string') {
        console.error('ITicketTemplate.name must be a string', data);
        return false;
    }
    if (typeof data.description !== 'string' && data.description !== null) {
        console.error('ITicketTemplate.description must be a string or NULL', data);
        return false;
    }
    if (typeof data.body !== 'string') {
        console.error('ITicketTemplate.body must be a string', data);
        return false;
    }
    if (typeof data.nametag_html_template !== 'string' && data.nametag_html_template !== null) {
        console.error('ITicketTemplate.nametag_html_template must be a string or NULL', data);
        return false;
    }
    if (typeof data.ticket_header_image !== 'number' && data.ticket_header_image !== null) {
        console.error('ITicketTemplate.ticket_header_image must be a number or NULL', data);
        return false;
    }
    if (typeof data.group_plugin_id !== 'number' && data.group_plugin_id !== null) {
        console.error('ITicketTemplate.group_plugin_id must be a number or NULL', data);
        return false;
    }
    if (data.attributes && !type_guards_1.isAttributeValues(data.attributes)) {
        return false;
    }
    return true;
}
exports.isTicketTemplate = isTicketTemplate;
/**
 * TicketTemplate endpoint
 *
 * @class TicketTemplateEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {ITicketTemplateEndpoint}
 */
var TicketTemplateEndpoint = /** @class */ (function (_super) {
    __extends(TicketTemplateEndpoint, _super);
    /**
     * Creates an instance of TicketTemplateEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof TicketTemplateEndpoint
     */
    function TicketTemplateEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/groups/:group_id/ticket_templates') || this;
    }
    /**
     * List all ticket templates.
     *
     * @param {number} group_id
     * @param {ITicketTemplateListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<ITicketTemplate>>>}
     *
     * @memberof TicketTemplateEndpoint
     */
    TicketTemplateEndpoint.prototype.list = function (group_id, params) {
        // console.log('list(group_id, params?)', group_id, params);
        var url = this.replacePrefixSegments({ group_id: group_id });
        // console.log('url', url);
        return this.getCollection(url, params, isTicketTemplate);
    };
    /**
     * Create new ticket template.
     *
     * @param {number} group_id
     * @param {ITicketTemplateSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<ITicketTemplate>>>}
     *
     * @memberof TicketTemplateEndpoint
     */
    TicketTemplateEndpoint.prototype.create = function (group_id, specs) {
        // console.log('create(group_id, specs)', group_id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id });
        // console.log('url', url);
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, isTicketTemplate);
    };
    /**
     * Get ticket template by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ITicketTemplateGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<ITicketTemplate>>>}
     *
     * @memberof TicketTemplateEndpoint
     */
    TicketTemplateEndpoint.prototype.get = function (group_id, id, params) {
        // console.log('get(group_id, id, params?)', group_id, id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        // console.log('url', url);
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, isTicketTemplate);
    };
    /**
     * Update ticket template by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ITicketTemplateUpdateSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<ITicketTemplate>>>}
     *
     * @memberof TicketTemplateEndpoint
     */
    TicketTemplateEndpoint.prototype.update = function (group_id, id, specs) {
        // console.log('update(group_id, id, specs)', group_id, id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        // console.log('url', url);
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, isTicketTemplate);
    };
    /**
     * Delete ticket template by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof TicketTemplateEndpoint
     */
    TicketTemplateEndpoint.prototype.delete = function (group_id, id) {
        // console.log('get(group_id, id)', group_id, id);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        // console.log('url', url);
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    return TicketTemplateEndpoint;
}(endpoint_1.default));
exports.TicketTemplateEndpoint = TicketTemplateEndpoint;
//# sourceMappingURL=ticketTemplate.js.map