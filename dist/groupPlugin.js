"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
/**
 * GroupPlugin endpoint
 *
 * @class GroupPluginEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IGroupPluginEndpoint}
 */
var GroupPluginEndpoint = /** @class */ (function (_super) {
    __extends(GroupPluginEndpoint, _super);
    /**
     * Creates an instance of GroupPluginEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof GroupPluginEndpoint
     */
    function GroupPluginEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/groups/:group_id/plugins') || this;
    }
    /**
     * Guard of IGroupPlugin.
     *
     * @param {*} data
     * @returns {data is IGroupPlugin}
     */
    GroupPluginEndpoint.prototype.isGroupPlugin = function (data) {
        if (data === null || typeof data !== 'object') {
            console.error('IGroupPlugin must be an object', data);
            return false;
        }
        if (typeof data.id !== 'number') {
            console.error('IGroupPlugin.id must be a number', data);
            return false;
        }
        if (typeof data.plugin_id !== 'number' && data.plugin_id !== null) {
            console.error('IGroupPlugin.plugin_id must be a number or NULL', data);
            return false;
        }
        if (typeof data.name !== 'string') {
            console.error('IGroupPlugin.name must be a string', data);
            return false;
        }
        if (typeof data.icon !== 'string' && data.icon !== null) {
            console.error('IGroupPlugin.icon must be a string or NULL', data);
            return false;
        }
        if (typeof data.key !== 'string' && data.key !== null) {
            console.error('IGroupPlugin.key must be a string or NULL', data);
            return false;
        }
        if (typeof data.entity_id !== 'number') {
            console.error('IGroupPlugin.entity_id must be a number', data);
            return false;
        }
        // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
        if (typeof data.created_at !== 'string') {
            console.error('IGroupPlugin.created_at must be a string', data);
            return false;
        }
        // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
        if (typeof data.updated_at !== 'string') {
            console.error('IGroupPlugin.updated_at must be a string', data);
            return false;
        }
        return true;
    };
    /**
     * List all group plugins.
     *
     * @param {number} group_id
     * @param {IGroupPluginListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IGroupPlugin>>>}
     *
     * @memberof GroupPluginEndpoint
     */
    GroupPluginEndpoint.prototype.list = function (group_id, params) {
        // console.log('list(group_id, params?)', group_id, params);
        var url = this.replacePrefixSegments({ group_id: group_id });
        // console.log('url', url);
        return this.getCollection(url, params, this.isGroupPlugin);
    };
    /**
     * Create new group plugin.
     *
     * @param {number} group_id
     * @param {IGroupPluginSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IGroupPlugin>>>}
     *
     * @memberof GroupPluginEndpoint
     */
    GroupPluginEndpoint.prototype.create = function (group_id, specs) {
        // console.log('create(group_id, specs)', group_id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id });
        // console.log('url', url);
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, this.isGroupPlugin);
    };
    /**
     * Get group plugin by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IGroupPluginGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<IGroupPlugin>>>}
     *
     * @memberof GroupPluginEndpoint
     */
    GroupPluginEndpoint.prototype.get = function (group_id, id, params) {
        // console.log('get(group_id, id, params?)', group_id, id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        // console.log('url', url);
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, this.isGroupPlugin);
    };
    /**
     * Update group plugin by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IGroupPluginUpdateSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IGroupPlugin>>>}
     *
     * @memberof GroupPluginEndpoint
     */
    GroupPluginEndpoint.prototype.update = function (group_id, id, specs) {
        // console.log('update(group_id, id, specs)', group_id, id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        // console.log('url', url);
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, this.isGroupPlugin);
    };
    /**
     * Delete group plugin by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof GroupPluginEndpoint
     */
    GroupPluginEndpoint.prototype.delete = function (group_id, id) {
        // console.log('get(group_id, id)', group_id, id);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        // console.log('url', url);
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    return GroupPluginEndpoint;
}(endpoint_1.default));
exports.GroupPluginEndpoint = GroupPluginEndpoint;
//# sourceMappingURL=groupPlugin.js.map