"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * DepositType endpoint
 *
 * @class DepositTypeEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IDepositTypeEndpoint}
 */
var DepositTypeEndpoint = /** @class */ (function (_super) {
    __extends(DepositTypeEndpoint, _super);
    /**
     * Creates an instance of DepositTypeEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof DepositTypeEndpoint
     */
    function DepositTypeEndpoint(superappEngine) {
        // console.log('constructor(superappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/groups/:group_id/deposit_types') || this;
    }
    /**
     * List all deposit types.
     *
     * @param {number} group_id
     * @param {IDepositTypeListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IDepositType>>>}
     *
     * @memberof DepositTypeEndpoint
     */
    DepositTypeEndpoint.prototype.list = function (group_id, params) {
        // console.log('list(group_id, params?)', group_id, params);
        var url = this.replacePrefixSegments({ group_id: group_id });
        // console.log('url', url);
        return this.getCollection(url, params, type_guards_1.isDepositType);
    };
    /**
     * Create new deposit type.
     *
     * @param {number} group_id
     * @param {IDepositTypeSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IDepositType>>>}
     *
     * @memberof DepositTypeEndpoint
     */
    DepositTypeEndpoint.prototype.create = function (group_id, specs) {
        // console.log('create(group_id, specs)', group_id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id });
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isDepositType);
    };
    /**
     * Get deposit type by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IDepositTypeGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<IDepositType>>>}
     *
     * @memberof DepositTypeEndpoint
     */
    DepositTypeEndpoint.prototype.get = function (group_id, id, params) {
        // console.log('get(group_id, id)', group_id, id);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        // console.log('url', url);
        var promise = this.api.http.get(url);
        return this.getContainer(promise, type_guards_1.isDepositType);
    };
    /**
     * Update deposit type by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IDepositTypeUpdateSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IDepositType>>>}
     *
     * @memberof DepositTypeEndpoint
     */
    DepositTypeEndpoint.prototype.update = function (group_id, id, specs) {
        // console.log('update(group_id, id, specs)', group_id, id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isDepositType);
    };
    /**
     * Delete deposit type by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof DepositTypeEndpoint
     */
    DepositTypeEndpoint.prototype.delete = function (group_id, id) {
        // console.log('get(group_id, id)', group_id, id);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    return DepositTypeEndpoint;
}(endpoint_1.default));
exports.DepositTypeEndpoint = DepositTypeEndpoint;
//# sourceMappingURL=depositType.js.map