import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, IAttributeValues, IAttribute, IAttributeSpecs, IAttributeGetParams, IAttributeListParams } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * UserAttribute list_related() params
 *
 * @export
 * @interface IUserAttributeListRelatedParams
 */
export interface IUserAttributeListRelatedParams extends IAttributeListParams {
    category_id: number;
    answers?: IAttributeValues;
}
/**
 * UserAttribute next() params
 *
 * @export
 * @interface IUserAttributeNextParams
 */
export interface IUserAttributeNextParams extends IAttributeGetParams {
    category_id: number;
    previous_id?: number;
    only_empty?: 0 | 1;
}
/**
 * UserAttribute endpoint methods
 *
 * @export
 * @interface IUserAttributeEndpoint
 */
export interface IUserAttributeEndpoint {
    list(group_id: number, params?: IAttributeListParams): Observable<AxiosResponse<ICollection<IAttribute>>>;
    create(group_id: number, specs: IAttributeSpecs): Observable<AxiosResponse<IContainer<IAttribute>>>;
    get(group_id: number, id: number, params?: IAttributeGetParams): Observable<AxiosResponse<IContainer<IAttribute>>>;
    list_related(group_id: number, params: IUserAttributeListRelatedParams): Observable<AxiosResponse<ICollection<IAttribute>>>;
    next(group_id: number, params: IUserAttributeNextParams): Observable<AxiosResponse<IContainer<IAttribute | void>>>;
    update(group_id: number, id: number, specs: IAttributeSpecs): Observable<AxiosResponse<IContainer<IAttribute>>>;
    delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}
/**
 * UserAttribute endpoint
 *
 * @class UserAttributeEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IUserAttributeEndpoint}
 */
export declare class UserAttributeEndpoint extends Endpoint implements IUserAttributeEndpoint {
    /**
     * Creates an instance of UserAttributeEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof UserAttributeEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List all group user attributes.
     *
     * @param {number} group_id
     * @param {IAttributeListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IAttribute>>>}
     *
     * @memberof UserAttributeEndpoint
     */
    list(group_id: number, params?: IAttributeListParams): Observable<AxiosResponse<ICollection<IAttribute>>>;
    /**
     * Create new group user attribute.
     *
     * @param {number} group_id
     * @param {IAttributeSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IAttribute>>>}
     *
     * @memberof UserAttributeEndpoint
     */
    create(group_id: number, specs: IAttributeSpecs): Observable<AxiosResponse<IContainer<IAttribute>>>;
    /**
     * Get group user attribute by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IAttributeGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<IAttribute>>>}
     *
     * @memberof UserAttributeEndpoint
     */
    get(group_id: number, id: number, params?: IAttributeGetParams): Observable<AxiosResponse<IContainer<IAttribute>>>;
    /**
     * List all group user attributes list_related.
     *
     * @param {number} group_id
     * @param {IUserAttributeListRelatedParams} params
     * @returns {Observable<AxiosResponse<ICollection<IAttribute>>>}
     *
     * @memberof UserAttributeEndpoint
     */
    list_related(group_id: number, params: IUserAttributeListRelatedParams): Observable<AxiosResponse<ICollection<IAttribute>>>;
    /**
     * Get group user attribute based on - category; previous attribute; user value status.
     *
     * @param {number} group_id
     * @param {IUserAttributeNextParams} params
     * @returns {Observable<AxiosResponse<IContainer<IAttribute>>>}
     *
     * @memberof UserAttributeEndpoint
     */
    next(group_id: number, params: IUserAttributeNextParams): Observable<AxiosResponse<IContainer<IAttribute | void>>>;
    /**
     * Update group user attribute by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IAttributeSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IAttribute>>>}
     *
     * @memberof UserAttributeEndpoint
     */
    update(group_id: number, id: number, specs: IAttributeSpecs): Observable<AxiosResponse<IContainer<IAttribute>>>;
    /**
     * Delete group user attribute by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof UserAttributeEndpoint
     */
    delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}
