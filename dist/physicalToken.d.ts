import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, IUserStub } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Supported types of PhysicalToken
 *
 * @export
 * @interface IPhysicalTokenType
 */
export declare type IPhysicalTokenType = 'rfid';
/**
 * PhysicalToken raw object
 *
 * @export
 * @interface IPhysicalToken
 */
export interface IPhysicalToken {
    id: number;
    token: string;
    type: IPhysicalTokenType;
    user_id: number;
}
/**
 * PhysicalToken specs
 *
 * @export
 * @interface IPhysicalTokenSpecs
 */
export interface IPhysicalTokenSpecs {
    token: string;
    type: IPhysicalTokenType;
}
/**
 * PhysicalToken list() params
 *
 * @export
 * @interface IPhysicalTokenListParams
 */
export interface IPhysicalTokenListParams extends ICollectionParams {
    sort?: string;
}
/**
 * PhysicalToken endpoint methods
 *
 * @export
 * @interface IPhysicalTokenEndpoint
 */
export interface IPhysicalTokenEndpoint {
    list(group_id: number, user_id: number, token_type: IPhysicalTokenType, params?: IPhysicalTokenListParams): Observable<AxiosResponse<ICollection<IPhysicalToken>>>;
    create(group_id: number, user_id: number, token_type: IPhysicalTokenType, specs: IPhysicalTokenSpecs): Observable<AxiosResponse<IContainer<IPhysicalToken>>>;
    get_user(group_id: number, token_type: IPhysicalTokenType, token: string): Observable<AxiosResponse<IContainer<IUserStub>>>;
    delete(group_id: number, user_id: number, token_type: IPhysicalTokenType, token: string): Observable<AxiosResponse<IContainer<void>>>;
}
/**
 * Guard of IPhysicalTokenType.
 *
 * @export
 * @param {*} data
 * @returns {data is IPhysicalTokenType}
 */
export declare function isPhysicalTokenType(data: any): data is IPhysicalTokenType;
/**
 * Guard of IPhysicalToken.
 *
 * @export
 * @param {*} data
 * @returns {data is IPhysicalToken}
 */
export declare function isPhysicalToken(data: any): data is IPhysicalToken;
/**
 * PhysicalToken endpoint
 *
 * @class PhysicalTokenEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IPhysicalTokenEndpoint}
 */
export declare class PhysicalTokenEndpoint extends Endpoint implements IPhysicalTokenEndpoint {
    /**
     * Creates an instance of PhysicalTokenEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof PhysicalTokenEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List all deposit tokens.
     *
     * @param {number} group_id
     * @param {number} user_id
     * @param {IPhysicalTokenListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IPhysicalToken>>>}
     *
     * @memberof PhysicalTokenEndpoint
     */
    list(group_id: number, user_id: number, token_type: IPhysicalTokenType, params?: IPhysicalTokenListParams): Observable<AxiosResponse<ICollection<IPhysicalToken>>>;
    /**
     * Create new physical token.
     *
     * @param {number} group_id
     * @param {number} user_id
     * @param {IPhysicalTokenSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IPhysicalToken>>>}
     *
     * @memberof PhysicalTokenEndpoint
     */
    create(group_id: number, user_id: number, token_type: IPhysicalTokenType, specs: IPhysicalTokenSpecs): Observable<AxiosResponse<IContainer<IPhysicalToken>>>;
    /**
     * Get user by token string.
     *
     * @param {number} group_id
     * @param {IPhysicalTokenType} token_type
     * @param {string} token
     * @returns {Observable<AxiosResponse<IContainer<IPhysicalToken>>>}
     *
     * @memberof PhysicalTokenEndpoint
     */
    get_user(group_id: number, token_type: IPhysicalTokenType, token: string): Observable<AxiosResponse<IContainer<IUserStub>>>;
    /**
     * Delete physical token by token string.
     *
     * @param {number} group_id
     * @param {number} user_id
     * @param {IPhysicalTokenType} token_type
     * @param {string} token
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof PhysicalTokenEndpoint
     */
    delete(group_id: number, user_id: number, token_type: IPhysicalTokenType, token: string): Observable<AxiosResponse<IContainer<void>>>;
}
