"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * PushToken endpoint
 *
 * @class PushTokenEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IPushTokenEndpoint}
 */
var PushTokenEndpoint = /** @class */ (function (_super) {
    __extends(PushTokenEndpoint, _super);
    /**
     * Creates an instance of PushTokenEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof PushTokenEndpoint
     */
    function PushTokenEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/auth/push_tokens') || this;
    }
    /**
     * List all push tokens.
     *
     * @param {ICollectionParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IPushToken>>>}
     *
     * @memberof PushTokenEndpoint
     */
    PushTokenEndpoint.prototype.list = function (params) {
        // console.log('list(params?: ICollectionParams)', params);
        var url = this.prefix;
        return this.getCollection(url, params, type_guards_1.isPushToken);
    };
    /**
     * Create new push token.
     *
     * @param {IPushTokenSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IPushToken>>>}
     *
     * @memberof PushTokenEndpoint
     */
    PushTokenEndpoint.prototype.create = function (specs) {
        // console.log('create(specs: IPushTokenSpecs)', specs);
        var url = this.prefix;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isPushToken);
    };
    /**
     * Get push token by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<IPushToken>>>}
     *
     * @memberof PushTokenEndpoint
     */
    PushTokenEndpoint.prototype.get = function (id) {
        // console.log('get(id: number)', id);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.get(url);
        return this.getContainer(promise, type_guards_1.isPushToken);
    };
    /**
     * Update push token by id.
     *
     * @param {number} id
     * @param {IPushTokenSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IPushToken>>>}
     *
     * @memberof PushTokenEndpoint
     */
    PushTokenEndpoint.prototype.update = function (id, specs) {
        // console.log('update(id: number, specs: IPushTokenSpecs)', id, specs);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isPushToken);
    };
    /**
     * Delete push token by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof PushTokenEndpoint
     */
    PushTokenEndpoint.prototype.delete = function (id) {
        // console.log('delete(id: number)', id);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    return PushTokenEndpoint;
}(endpoint_1.default));
exports.PushTokenEndpoint = PushTokenEndpoint;
//# sourceMappingURL=pushToken.js.map