"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Permission endpoint
 *
 * @class PermissionEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IPermissionEndpoint}
 */
var PermissionEndpoint = /** @class */ (function (_super) {
    __extends(PermissionEndpoint, _super);
    /**
     * Creates an instance of PermissionEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof PermissionEndpoint
     */
    function PermissionEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/permissions') || this;
    }
    /**
     * Create new permission.
     *
     * @param {IPermissionSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IPermission>>>}
     *
     * @memberof PermissionEndpoint
     */
    PermissionEndpoint.prototype.create = function (specs) {
        // console.log('create(specs: IPermissionSpecs)', specs);
        var url = this.prefix;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isPermission);
    };
    /**
     * Delete permission by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof PermissionEndpoint
     */
    PermissionEndpoint.prototype.delete = function (id) {
        // console.log('delete(id: number)', id);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    /**
     * List all permissions.
     *
     * @param {ICollectionParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IPermission>>>}
     *
     * @memberof PermissionEndpoint
     */
    PermissionEndpoint.prototype.list = function (params) {
        // console.log('list(params?: ICollectionParams)', params);
        var url = this.prefix;
        // console.log('url', url);
        return this.getCollection(url, params, type_guards_1.isPermission);
    };
    /**
     * Get permission by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<IPermission>>>}
     *
     * @memberof PermissionEndpoint
     */
    PermissionEndpoint.prototype.get = function (id) {
        // console.log('get(id: number)', id);
        var url = this.prefix + "/" + id;
        // console.log('url', url);
        var promise = this.api.http.get(url);
        return this.getContainer(promise, type_guards_1.isPermission);
    };
    /**
     * Update permission by id.
     *
     * @param {number} id
     * @param {IPermissionSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IPermission>>>}
     *
     * @memberof PermissionEndpoint
     */
    PermissionEndpoint.prototype.update = function (id, specs) {
        // console.log('update(id: number, specs: IPermissionSpecs)', id, specs);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isPermission);
    };
    return PermissionEndpoint;
}(endpoint_1.default));
exports.PermissionEndpoint = PermissionEndpoint;
//# sourceMappingURL=permission.js.map