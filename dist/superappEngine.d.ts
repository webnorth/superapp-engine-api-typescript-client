import API from './common/api';
import { IAPISpecs } from './common/interfaces';
/**
 * Superapp Engine API
 *
 * @export
 * @class SuperappEngine
 * @extends {API}
 */
declare class SuperappEngine extends API {
    /**
     * error fn handler interceptor
     *
     * @private
     * @static
     *
     * @memberof SuperappEngine
     */
    private static errorHandler;
    /**
     * Creates an instance of SuperappEngine.
     *
     * @param {IAPISpecs} apiSpecs
     *
     * @memberof SuperappEngine
     */
    constructor(apiSpecs: IAPISpecs);
}
export default SuperappEngine;
export { SuperappEngine };
