import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, ITicket, IAuthTicketListParams, ITicketListParams, ITicketCheckParams, ITicketGetParams, ITicketSpecs, ITicketEndpoint } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Ticket endpoint
 *
 * @class TicketEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {ITicketEndpoint}
 */
export declare class TicketEndpoint extends Endpoint implements ITicketEndpoint {
    /**
     * Creates an instance of TicketEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof TicketEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List auth tickets.
     *
     * @param {IAuthTicketListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    authList(params?: IAuthTicketListParams): Observable<AxiosResponse<ICollection<ITicket>>>;
    /**
     * List all tickets.
     *
     * @param {number} group_id
     * @param {ITicketListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    list(group_id: number, params?: ITicketListParams): Observable<AxiosResponse<ICollection<ITicket>>>;
    /**
     * Create new ticket.
     *
     * @param {number} group_id
     * @param {ITicketSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    create(group_id: number, specs: ITicketSpecs): Observable<AxiosResponse<IContainer<ITicket>>>;
    /**
     * Check ticket by code.
     *
     * @param {number} group_id
     * @param {ITicketCheckParams} params
     * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    check(group_id: number, params: ITicketCheckParams): Observable<AxiosResponse<IContainer<ITicket>>>;
    /**
     * Checkin ticket by code.
     *
     * @param {number} group_id
     * @param {ITicketCheckParams} params
     * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    checkin(group_id: number, params: ITicketCheckParams): Observable<AxiosResponse<IContainer<ITicket>>>;
    /**
     * Get ticket by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ITicketGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    get(group_id: number, id: number, params?: ITicketGetParams): Observable<AxiosResponse<IContainer<ITicket>>>;
    /**
     * Checkout ticket by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ITicketGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    checkout(group_id: number, id: number, params?: ITicketGetParams): Observable<AxiosResponse<IContainer<ITicket>>>;
    /**
     * Invalidate ticket by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ITicketGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<ITicket>>>}
     *
     * @memberof TicketEndpoint
     */
    invalidate(group_id: number, id: number, params?: ITicketGetParams): Observable<AxiosResponse<IContainer<ITicket>>>;
    /**
     * Delete ticket by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof TicketEndpoint
     */
    delete_pdf(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}
