"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Guard of IMenu.
 *
 * @export
 * @param {*} data
 * @returns {data is IMenu}
 */
function isMenu(data) {
    if (data === null || typeof data !== 'object') {
        console.error('IMenu must be an object', data);
        return false;
    }
    if (typeof data.id !== 'number') {
        console.error('IMenu.id must be a number', data);
        return false;
    }
    if (typeof data.name !== 'string') {
        console.error('IMenu.name must be a string', data);
        return false;
    }
    if (typeof data.slug !== 'string') {
        console.error('IMenu.slug must be a string', data);
        return false;
    }
    if (typeof data.enabled !== 'boolean') {
        console.error('IMenu.enabled must be a boolean', data);
        return false;
    }
    // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
    if (typeof data.created_at !== 'string') {
        console.error('IMenu.created_at must be a string', data);
        return false;
    }
    // TODO: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/
    if (typeof data.updated_at !== 'string') {
        console.error('IMenu.updated_at must be a string', data);
        return false;
    }
    if (!Array.isArray(data.destinations)) {
        console.error('IMenu.destinations must be an array', data);
        return false;
    }
    if (!data.destinations.every(type_guards_1.isDestination))
        return false;
    return true;
}
exports.isMenu = isMenu;
/**
 * Menu endpoint
 *
 * @class MenuEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IMenuEndpoint}
 */
var MenuEndpoint = /** @class */ (function (_super) {
    __extends(MenuEndpoint, _super);
    /**
     * Creates an instance of MenuEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof MenuEndpoint
     */
    function MenuEndpoint(superappEngine) {
        // console.log('constructor(superappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/groups/:group_id/menus') || this;
    }
    /**
     * Create new menu.
     *
     * @param {number} group_id
     * @param {IMenuSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IMenu>>>}
     *
     * @memberof MenuEndpoint
     */
    MenuEndpoint.prototype.create = function (group_id, specs) {
        // console.log('create(group_id, specs)', group_id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id });
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, isMenu);
    };
    /**
     * Delete menu by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof MenuEndpoint
     */
    MenuEndpoint.prototype.delete = function (group_id, id) {
        // console.log('get(group_id, id)', group_id, id);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    /**
     * List all menus.
     *
     * @param {number} group_id
     * @param {IMenuListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IMenu>>>}
     *
     * @memberof MenuEndpoint
     */
    MenuEndpoint.prototype.list = function (group_id, params) {
        // console.log('list(group_id, params?)', group_id, params);
        var url = this.replacePrefixSegments({ group_id: group_id });
        // console.log('url', url);
        return this.getCollection(url, params, isMenu);
    };
    /**
     * Get menu by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IMenuGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<IMenu>>>}
     *
     * @memberof MenuEndpoint
     */
    MenuEndpoint.prototype.get = function (group_id, id, params) {
        // console.log('get(group_id, id, params?)', group_id, id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        // console.log('url', url);
        var promise = this.api.http.get(url);
        return this.getContainer(promise, isMenu);
    };
    /**
     * Update menu by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IMenuSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IMenu>>>}
     *
     * @memberof MenuEndpoint
     */
    MenuEndpoint.prototype.update = function (group_id, id, specs) {
        // console.log('update(group_id, id, specs)', group_id, id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, isMenu);
    };
    return MenuEndpoint;
}(endpoint_1.default));
exports.MenuEndpoint = MenuEndpoint;
//# sourceMappingURL=menu.js.map