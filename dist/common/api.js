"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var setting_1 = require("../setting");
var auth_1 = require("../auth");
var pushToken_1 = require("../pushToken");
var physicalToken_1 = require("../physicalToken");
var authMessage_1 = require("../authMessage");
var authStripe_1 = require("../authStripe");
var media_1 = require("../media");
var user_1 = require("../user");
var role_1 = require("../role");
var permission_1 = require("../permission");
var plugin_1 = require("../plugin");
var group_1 = require("../group");
var groupPlugin_1 = require("../groupPlugin");
var menu_1 = require("../menu");
var userAttribute_1 = require("../userAttribute");
var userAttributeCategory_1 = require("../userAttributeCategory");
var article_1 = require("../article");
var deposit_1 = require("../deposit");
var depositType_1 = require("../depositType");
var ticketTemplate_1 = require("../ticketTemplate");
var ticket_1 = require("../ticket");
var tag_1 = require("../tag");
/**
 * Generic API client
 *
 * @export
 * @abstract
 * @class API
 */
var API = /** @class */ (function () {
    /**
     * Creates an instance of API.
     *
     * @param {IAPISpecs} specs
     *
     * @memberof API
     */
    function API(specs) {
        // console.log('constructor(specs: IAPISpecs)', specs);
        this.headers = specs.headers;
        this.host = specs.host;
        this.invalidResponse = specs.invalidResponse;
        this.prefix = specs.prefix || '/';
        this.protocol = specs.protocol;
        this.timeout = specs.timeout;
        this.http = axios_1.default.create(this.axiosConfig);
        this.loadEndpoints();
    }
    Object.defineProperty(API.prototype, "baseUrl", {
        /**
         *  $protocol://$host$prefix
         *
         * @readonly
         * @private
         * @type {string}
         * @memberof API
         */
        get: function () {
            // console.log('get baseUrl()');
            return this.protocol + "://" + this.host + this.prefix;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * add headers
     *
     * @readonly
     * @public
     *
     * @memberof API
     */
    API.prototype.addHeaders = function (headers) {
        // console.log('addHeaders(headers)', headers);
        this.headers = Object.assign({}, this.headers, headers);
        // update axios instance
        this.http.defaults.headers.common = this.headers;
        // console.log('this.http.defaults.headers', this.http.defaults.headers);
    };
    Object.defineProperty(API.prototype, "axiosConfig", {
        /**
         * axios config object
         *
         * @readonly
         * @private
         * @type {AxiosRequestConfig}
         * @memberof API
         */
        get: function () {
            // console.log('get axiosConfig()');
            return { baseURL: this.baseUrl, headers: this.headers, timeout: this.timeout };
        },
        enumerable: true,
        configurable: true
    });
    /**
     * load all endpoints instances
     *
     * @private
     *
     * @memberof SuperappEngine
     */
    API.prototype.loadEndpoints = function () {
        // console.log('loadEndpoints()');
        this.Setting = new setting_1.SettingEndpoint(this);
        this.Auth = new auth_1.AuthEndpoint(this);
        this.PushToken = new pushToken_1.PushTokenEndpoint(this);
        this.PhysicalToken = new physicalToken_1.PhysicalTokenEndpoint(this);
        this.AuthMessage = new authMessage_1.AuthMessageEndpoint(this);
        this.AuthStripe = new authStripe_1.AuthStripeEndpoint(this);
        this.Media = new media_1.MediaEndpoint(this);
        this.User = new user_1.UserEndpoint(this);
        this.Role = new role_1.RoleEndpoint(this);
        this.Permission = new permission_1.PermissionEndpoint(this);
        this.Plugin = new plugin_1.PluginEndpoint(this);
        this.Group = new group_1.GroupEndpoint(this);
        this.GroupPlugin = new groupPlugin_1.GroupPluginEndpoint(this);
        this.Menu = new menu_1.MenuEndpoint(this);
        this.Tag = new tag_1.TagEndpoint(this);
        this.UserAttribute = new userAttribute_1.UserAttributeEndpoint(this);
        this.UserAttributeCategory = new userAttributeCategory_1.UserAttributeCategoryEndpoint(this);
        this.Article = new article_1.ArticleEndpoint(this);
        this.Deposit = new deposit_1.DepositEndpoint(this);
        this.DepositType = new depositType_1.DepositTypeEndpoint(this);
        this.TicketTemplate = new ticketTemplate_1.TicketTemplateEndpoint(this);
        this.Ticket = new ticket_1.TicketEndpoint(this);
    };
    return API;
}());
exports.default = API;
//# sourceMappingURL=api.js.map