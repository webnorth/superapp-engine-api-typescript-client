import { AxiosInstance } from 'axios';
import { SettingEndpoint } from '../setting';
import { AuthEndpoint } from '../auth';
import { PushTokenEndpoint } from '../pushToken';
import { PhysicalTokenEndpoint } from '../physicalToken';
import { AuthMessageEndpoint } from '../authMessage';
import { AuthStripeEndpoint } from '../authStripe';
import { MediaEndpoint } from '../media';
import { UserEndpoint } from '../user';
import { RoleEndpoint } from '../role';
import { PermissionEndpoint } from '../permission';
import { PluginEndpoint } from '../plugin';
import { GroupEndpoint } from '../group';
import { GroupPluginEndpoint } from '../groupPlugin';
import { MenuEndpoint } from '../menu';
import { UserAttributeEndpoint } from '../userAttribute';
import { UserAttributeCategoryEndpoint } from '../userAttributeCategory';
import { ArticleEndpoint } from '../article';
import { DepositEndpoint } from '../deposit';
import { DepositTypeEndpoint } from '../depositType';
import { TicketTemplateEndpoint } from '../ticketTemplate';
import { TicketEndpoint } from '../ticket';
import { IAPISpecs } from './interfaces';
import { TagEndpoint } from '../tag';
/**
 * Generic API client
 *
 * @export
 * @abstract
 * @class API
 */
export default abstract class API {
    /**
     * Setting endpoint
     *
     * @type {SettingEndpoint}
     * @memberof SuperappEngine
     */
    Setting: SettingEndpoint;
    /**
     * Auth endpoint
     *
     * @type {AuthEndpoint}
     * @memberof SuperappEngine
     */
    Auth: AuthEndpoint;
    /**
     * PushToken endpoint
     *
     * @type {PushTokenEndpoint}
     * @memberof SuperappEngine
     */
    PushToken: PushTokenEndpoint;
    /**
     * PhysicalToken endpoint
     *
     * @type {PhysicalTokenEndpoint}
     * @memberof SuperappEngine
     */
    PhysicalToken: PhysicalTokenEndpoint;
    /**
     * AuthMessage endpoint
     *
     * @type {AuthMessageEndpoint}
     * @memberof SuperappEngine
     */
    AuthMessage: AuthMessageEndpoint;
    /**
     * AuthStripe endpoint
     *
     * @type {AuthStripeEndpoint}
     * @memberof SuperappEngine
     */
    AuthStripe: AuthStripeEndpoint;
    /**
     * Media endpoint
     *
     * @type {MediaEndpoint}
     * @memberof SuperappEngine
     */
    Media: MediaEndpoint;
    /**
     * User endpoint
     *
     * @type {UserEndpoint}
     * @memberof SuperappEngine
     */
    User: UserEndpoint;
    /**
     * Role endpoint
     *
     * @type {RoleEndpoint}
     * @memberof SuperappEngine
     */
    Role: RoleEndpoint;
    /**
     * Permission endpoint
     *
     * @type {PermissionEndpoint}
     * @memberof SuperappEngine
     */
    Permission: PermissionEndpoint;
    /**
     * Plugin endpoint
     *
     * @type {PluginEndpoint}
     * @memberof SuperappEngine
     */
    Plugin: PluginEndpoint;
    /**
     * Group endpoint
     *
     * @type {GroupEndpoint}
     * @memberof SuperappEngine
     */
    Group: GroupEndpoint;
    /**
     * GroupPlugin endpoint
     *
     * @type {GroupPluginEndpoint}
     * @memberof SuperappEngine
     */
    GroupPlugin: GroupPluginEndpoint;
    /**
     * Menu endpoint
     *
     * @type {MenuEndpoint}
     * @memberof SuperappEngine
     */
    Menu: MenuEndpoint;
    /**
     * Tag endpoint
     *
     * @type {TagEndpoint}
     * @memberof SuperappEngine
     */
    Tag: TagEndpoint;
    /**
     * UserAttribute endpoint
     *
     * @type {UserAttributeEndpoint}
     * @memberof SuperappEngine
     */
    UserAttribute: UserAttributeEndpoint;
    /**
     * UserAttributeCategory endpoint
     *
     * @type {UserAttributeCategoryEndpoint}
     * @memberof SuperappEngine
     */
    UserAttributeCategory: UserAttributeCategoryEndpoint;
    /**
     * Article endpoint
     *
     * @type {ArticleEndpoint}
     * @memberof SuperappEngine
     */
    Article: ArticleEndpoint;
    /**
     * Deposit endpoint
     *
     * @type {DepositEndpoint}
     * @memberof SuperappEngine
     */
    Deposit: DepositEndpoint;
    /**
     * DepositType endpoint
     *
     * @type {DepositTypeEndpoint}
     * @memberof SuperappEngine
     */
    DepositType: DepositTypeEndpoint;
    /**
     * TicketTemplate endpoint
     *
     * @type {TicketTemplateEndpoint}
     * @memberof SuperappEngine
     */
    TicketTemplate: TicketTemplateEndpoint;
    /**
     * Ticket endpoint
     *
     * @type {TicketEndpoint}
     * @memberof SuperappEngine
     */
    Ticket: TicketEndpoint;
    /**
     * Default invalid response throwable error
     *
     * @public
     * @type {Error}
     * @memberof API
     */
    invalidResponse: Error;
    /**
     * request timeout
     *
     * @public
     * @type {number}
     * @memberof API
     */
    timeout: number;
    /**
     * default request headers
     *
     * @protected
     * @type {{ [key: string]: string }}
     * @memberof API
     */
    protected headers: {
        [key: string]: string;
    };
    /**
     * host $protocol://$host$prefix
     *
     * @protected
     * @type {string}
     * @memberof API
     */
    protected host: string;
    /**
     * axios client
     *
     * @public
     * @type {AxiosInstance}
     * @memberof API
     */
    http: AxiosInstance;
    /**
     * prefix $protocol://$host$prefix
     *
     * @protected
     * @type {string}
     * @memberof API
     */
    protected prefix: string;
    /**
     * protocol $protocol://$host$prefix
     *
     * @protected
     * @type {('http'|'https')}
     * @memberof API
     */
    protected protocol: 'http' | 'https';
    /**
     *  $protocol://$host$prefix
     *
     * @readonly
     * @private
     * @type {string}
     * @memberof API
     */
    private readonly baseUrl;
    /**
     * add headers
     *
     * @readonly
     * @public
     *
     * @memberof API
     */
    addHeaders(headers: {
        [key: string]: string;
    }): void;
    /**
     * axios config object
     *
     * @readonly
     * @private
     * @type {AxiosRequestConfig}
     * @memberof API
     */
    private readonly axiosConfig;
    /**
     * Creates an instance of API.
     *
     * @param {IAPISpecs} specs
     *
     * @memberof API
     */
    constructor(specs: IAPISpecs);
    /**
     * load all endpoints instances
     *
     * @private
     *
     * @memberof SuperappEngine
     */
    private loadEndpoints;
}
