"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var type_guards_1 = require("./type.guards");
/**
 * Endpoint belongs to APIs
 *
 * @abstract
 * @class Endpoint
 * @template API
 */
var Endpoint = /** @class */ (function () {
    /**
     * Creates an instance of Endpoint.
     *
     * @param {API} api
     * @param {string} prefix
     *
     * @memberof Endpoint
     */
    function Endpoint(api, prefix) {
        // console.log('constructor(api: API, prefix: string)', api, prefix);
        this.api = api;
        this.prefix = prefix;
    }
    /**
     * Default function to get and process pagination responses
     *
     * @protected
     * @template C
     * @param {string} url
     * @param {ICollectionParams} params
     * @param {string} property
     * @param {any} [guard]
     * @returns {Observable<AxiosResponse<ICollection<C>>>}
     * @memberof Endpoint
     */
    Endpoint.prototype.getCollection = function (url, params, guard) {
        var _this = this;
        // console.log('getCollection(url, params, guard?)', url, params, guard);
        params = this.getCollectionParams(params || {});
        var observable = rxjs_1.Observable.from(this.api.http.get(url, { params: params }));
        return observable.pipe(operators_1.tap(function (res) {
            // console.log('validate res.data', res);
            if (!type_guards_1.isCollection(res.data, guard)) {
                // console.log('throw error');
                throw _this.api.invalidResponse;
            }
        }));
    };
    /**
     * Default function to get item
     *
     * @protected
     * @template C
     * @param {string} url
     * @param {IContainerParams} params
     * @param {string} property
     * @param {any} [guard]
     * @returns {Observable<AxiosResponse<IContainer<C>>>}
     * @memberof Endpoint
     */
    Endpoint.prototype.getContainer = function (promise, guard) {
        var _this = this;
        // console.log('getContainer(url, params, guard?)', url, params, guard);
        var observable = rxjs_1.Observable.from(promise);
        return observable.pipe(operators_1.tap(function (res) {
            // console.log('validate res.data.data', res);
            if (guard && !guard(res.data.data)) {
                // console.log('throw error');
                throw _this.api.invalidResponse;
            }
        }));
    };
    /**
     * Get object with collection params
     *
     * @private
     * @param {ICollectionParams} params
     * @returns {ICollectionParams}
     *
     * @memberof Endpoint
     */
    Endpoint.prototype.getCollectionParams = function (params) {
        // console.log('getCollectionParams(params: ICollectionParams)', params);
        params.per_page = params.per_page || 15;
        params.page = params.page || 1;
        return params;
    };
    /**
     * Replace segments in URL
     *
     * @protected
     * @param {IStringsMap} values
     * @returns {string}
     *
     * @memberof Endpoint
     */
    Endpoint.prototype.replacePrefixSegments = function (values) {
        // console.log('replacePrefixSegments(values)', values);
        var parts = this.prefix.split('/');
        var segments = [];
        parts.forEach(function (part) {
            // console.log('part', part);
            var pos = part.indexOf(':');
            if (pos < 0) {
                segments.push(part);
            }
            else if (pos > 0) {
                console.warn('Unexpected URL segment syntax');
                segments.push(part);
            }
            else {
                var key = part.substring(1);
                segments.push(String(values[key]));
            }
        });
        // console.log('segments', segments);
        return segments.join('/');
    };
    return Endpoint;
}());
exports.default = Endpoint;
//# sourceMappingURL=endpoint.js.map