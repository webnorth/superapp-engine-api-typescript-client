import { AxiosPromise, AxiosResponse } from 'axios';
import { Observable } from 'rxjs';
import API from './api';
import { IContainer, ICollection, ICollectionParams, IStringsMap } from './interfaces';
export declare type predicate<T> = (value: T, index?: number) => boolean;
/**
 * Endpoint belongs to APIs
 *
 * @abstract
 * @class Endpoint
 * @template API
 */
export default abstract class Endpoint {
    /**
     * reference to API instance
     *
     * @protected
     * @type {API}
     * @memberof Endpoint
     */
    protected readonly api: API;
    /**
     * url prefix
     *
     * @protected
     * @type {string}
     * @memberof Endpoint
     */
    protected readonly prefix: string;
    /**
     * Creates an instance of Endpoint.
     *
     * @param {API} api
     * @param {string} prefix
     *
     * @memberof Endpoint
     */
    constructor(api: API, prefix: string);
    /**
     * Default function to get and process pagination responses
     *
     * @protected
     * @template C
     * @param {string} url
     * @param {ICollectionParams} params
     * @param {string} property
     * @param {any} [guard]
     * @returns {Observable<AxiosResponse<ICollection<C>>>}
     * @memberof Endpoint
     */
    protected getCollection<C>(url: string, params: ICollectionParams, guard: (data: any) => data is C): Observable<AxiosResponse<ICollection<C>>>;
    /**
     * Default function to get item
     *
     * @protected
     * @template C
     * @param {string} url
     * @param {IContainerParams} params
     * @param {string} property
     * @param {any} [guard]
     * @returns {Observable<AxiosResponse<IContainer<C>>>}
     * @memberof Endpoint
     */
    protected getContainer<C>(promise: AxiosPromise<IContainer<C>>, guard?: (data: any) => data is C): Observable<AxiosResponse<IContainer<C>>>;
    /**
     * Get object with collection params
     *
     * @private
     * @param {ICollectionParams} params
     * @returns {ICollectionParams}
     *
     * @memberof Endpoint
     */
    private getCollectionParams;
    /**
     * Replace segments in URL
     *
     * @protected
     * @param {IStringsMap} values
     * @returns {string}
     *
     * @memberof Endpoint
     */
    protected replacePrefixSegments(values: IStringsMap): string;
}
