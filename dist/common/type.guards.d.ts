import { IAttributeValues, IUserStub, IUserLike, IAuthUser, IAuthIsBought, IUser, IPushToken, IMessageType, IMessageLike, IPaymentMethod, IPaymentIntent, IStripeDeposit, IMediaLike, IImageType, IMediaImage, IRole, IPermissionCategory, IPermission, IPlugin, ICategory, IOption, IAttributeType, IAttribute, IGroup, IDestination, IDepositType, IDeposit, ITicket, ICollection, IPagination, ITag } from './interfaces';
/**
 * Guard of string[];
 *
 * @export
 * @param {*} data
 * @returns {data is string[]}
 */
export declare function isArrayOfString(data: any): data is string[];
/**
 * Guard of number[];
 *
 * @export
 * @param {*} data
 * @returns {data is number[]}
 */
export declare function isArrayOfNumber(data: any): data is number[];
/**
 * Guard of IAttributeValues.
 *
 * @export
 * @param {*} data
 * @returns {data is IAttributeValues}
 */
export declare function isAttributeValues(data: any): data is IAttributeValues;
/**
 * Guard of IUserStub.
 *
 * @export
 * @param {*} data
 * @returns {data is IUserStub}
 */
export declare function isUserStub(data: any): data is IUserStub;
/**
 * Guard of IUserLike.
 *
 * @export
 * @param {*} data
 * @returns {data is IUserLike}
 */
export declare function isUserLike(data: any): data is IUserLike;
/**
 * Guard of IAuthUser.
 *
 * @export
 * @param {*} data
 * @returns {data is IAuthUser}
 */
export declare function isAuthUser(data: any): data is IAuthUser;
/**
 * Guard of IUser.
 *
 * @export
 * @param {*} data
 * @returns {data is IUser}
 */
export declare function isUser(data: any): data is IUser;
/**
 * Guard of IAuthIsBought.
 *
 * @export
 * @param {*} data
 * @returns {data is IAuthIsBought}
 */
export declare function isAuthPaid(data: any): data is IAuthIsBought;
/**
 * Guard of IPushToken.
 *
 * @export
 * @param {*} data
 * @returns {data is IPushToken}
 */
export declare function isPushToken(data: any): data is IPushToken;
/**
 * Guard of IMessageType.
 *
 * @export
 * @param {*} data
 * @returns {data is IMessageType}
 */
export declare function isMessageType(data: any): data is IMessageType;
/**
 * Guard of IMessageLike.
 *
 * @export
 * @param {*} data
 * @returns {data is IMessageLike}
 */
export declare function isMessageLike(data: any): data is IMessageLike;
/**
 * Guard of IPaymentMethod.
 *
 * @export
 * @param {*} data
 * @returns {data is IPaymentMethod}
 */
export declare function isPaymentMethod(data: any): data is IPaymentMethod;
/**
 * Guard of IPaymentMethod.
 *
 * @export
 * @param {*} data
 * @returns {data is IPaymentIntent}
 */
export declare function isPaymentIntent(data: any): data is IPaymentIntent;
/**
 * Guard of IStripeDeposit.
 *
 * @export
 * @param {*} data
 * @returns {data is IStripeDeposit}
 */
export declare function isStripeDeposit(data: any): data is IStripeDeposit;
/**
 * Guard of IMediaLike.
 *
 * @export
 * @param {*} data
 * @returns {data is IMediaLike}
 */
export declare function isMediaLike(data: any): data is IMediaLike;
/**
 * Guard of IImageType.
 *
 * @export
 * @param {*} data
 * @returns {data is IImageType}
 */
export declare function isImageType(data: any): data is IImageType;
/**
 * Guard of IMediaImage.
 *
 * @export
 * @param {*} data
 * @returns {data is IMediaImage}
 */
export declare function isMediaImage(data: any): data is IMediaImage;
/**
 * Guard of IRole.
 *
 * @export
 * @param {*} data
 * @returns {data is IRole}
 */
export declare function isRole(data: any): data is IRole;
/**
 * Guard of IPermissionCategory.
 *
 * @export
 * @param {*} data
 * @returns {data is IPermissionCategory}
 */
export declare function isPermissionCategory(data: any): data is IPermissionCategory;
/**
 * Guard of IPermission.
 *
 * @export
 * @param {*} data
 * @returns {data is IPermission}
 */
export declare function isPermission(data: any): data is IPermission;
/**
 * Guard of IPlugin.
 *
 * @export
 * @param {*} data
 * @returns {data is IPlugin}
 */
export declare function isPlugin(data: any): data is IPlugin;
/**
 * Guard of ICategory.
 *
 * @export
 * @param {*} data
 * @returns {data is ICategory}
 */
export declare function isCategory(data: any): data is ICategory;
/**
 * Guard of IOption.
 *
 * @export
 * @param {*} data
 * @returns {data is IOption}
 */
export declare function isOption(data: any): data is IOption;
/**
 * Guard of IAttributeType.
 *
 * @export
 * @param {*} data
 * @returns {data is IAttributeType}
 */
export declare function isAttributeType(data: any): data is IAttributeType;
/**
 * Guard of IAttribute.
 *
 * @export
 * @param {*} data
 * @returns {data is IAttribute | void}
 */
export declare function isAttributeOrVoid(data: any): data is IAttribute | void;
/**
 * Guard of IAttribute.
 *
 * @export
 * @param {*} data
 * @returns {data is IAttribute}
 */
export declare function isAttribute(data: any): data is IAttribute;
/**
 * Guard of IGroup.
 *
 * @export
 * @param {*} data
 * @returns {data is IGroup}
 */
export declare function isGroup(data: any): data is IGroup;
/**
 * Guard of ITag.
 *
 * @export
 * @param {*} data
 * @returns {data is ITag}
 */
export declare function isTag(data: ITag): data is ITag;
/**
 * Guard of IDestination.
 *
 * @export
 * @param {*} data
 * @returns {data is IDestination}
 */
export declare function isDestination(data: any): data is IDestination;
/**
 * Guard of IDepositType.
 *
 * @export
 * @param {*} data
 * @returns {data is IDepositType}
 */
export declare function isDepositType(data: any): data is IDepositType;
/**
 * Guard of IDeposit.
 *
 * @export
 * @param {*} data
 * @returns {data is IDeposit}
 */
export declare function isDeposit(data: any): data is IDeposit;
/**
 * Guard of ITicket.
 *
 * @export
 * @param {*} data
 * @returns {data is ITicket}
 */
export declare function isTicket(data: any): data is ITicket;
/**
 * Guard of ICollection<any>
 *
 * @export
 * @param {*} coll
 * @returns {coll is ICollection<any>}
 */
export declare function isCollection<T>(coll: any, guard: (d: any) => d is T): coll is ICollection<T>;
/**
 * Guard of IPagination.
 *
 * @export
 * @param {*} data
 * @returns {data is IPagination}
 */
export declare function isPagination(data: any): data is IPagination;
