"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
/**
 * Setting endpoint
 *
 * @class SettingEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {ISettingEndpoint}
 */
var SettingEndpoint = /** @class */ (function (_super) {
    __extends(SettingEndpoint, _super);
    /**
     * Creates an instance of SettingEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof SettingEndpoint
     */
    function SettingEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/settings') || this;
    }
    /**
     * Guard of ISetting.
     *
     * @param {*} data
     * @returns {data is ISetting}
     */
    SettingEndpoint.prototype.isSetting = function (data) {
        if (data === null || typeof data !== 'object') {
            console.error('ISetting must be an object', data);
            return false;
        }
        if (typeof data.value !== 'string' && data.value !== null) {
            console.error('ISetting.value must be a string or NULL', data);
            return false;
        }
        return true;
    };
    /**
     * Get permission by key.
     *
     * @param {string} key
     * @returns {Observable<AxiosResponse<IContainer<ISetting>>>}
     *
     * @memberof SettingEndpoint
     */
    SettingEndpoint.prototype.public = function (key) {
        // console.log('public(key: string)', key);
        var url = this.prefix + "/public/" + key;
        // console.log('url', url);
        var promise = this.api.http.get(url);
        return this.getContainer(promise, this.isSetting);
    };
    return SettingEndpoint;
}(endpoint_1.default));
exports.SettingEndpoint = SettingEndpoint;
//# sourceMappingURL=setting.js.map