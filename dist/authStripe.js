"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Stripe endpoint
 *
 * @class AuthStripeEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IAuthStripeEndpoint}
 */
var AuthStripeEndpoint = /** @class */ (function (_super) {
    __extends(AuthStripeEndpoint, _super);
    /**
     * Creates an instance of AuthStripeEndpoint.
     *
     * @param {SuperappEngine} superappEngine
     *
     * @memberof AuthStripeEndpoint
     */
    function AuthStripeEndpoint(superappEngine) {
        // console.log('constructor(superappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/auth/stripe') || this;
    }
    /**
     * List stripe payment methods.
     *
     * @returns {Observable<AxiosResponse<ICollection<IPaymentMethod>>>}
     *
     * @memberof AuthStripeEndpoint
     */
    AuthStripeEndpoint.prototype.list = function () {
        // console.log('list()');
        var url = this.prefix + '/method';
        return this.getCollection(url, null, type_guards_1.isPaymentMethod);
    };
    /**
     * Create new stripe payment method.
     *
     * @param {IPaymentMethodSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IPaymentMethod>>>}
     *
     * @memberof AuthStripeEndpoint
     */
    AuthStripeEndpoint.prototype.create = function (specs) {
        // console.log('create(specs)', specs);
        var url = this.prefix + '/method';
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise);
    };
    /**
     * Add money.
     *
     * @param {IStripeDepositSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IStripeDeposit>>>}
     *
     * @memberof AuthStripeEndpoint
     */
    AuthStripeEndpoint.prototype.deposit = function (specs) {
        // console.log('deposit(specs)', specs);
        var url = this.prefix + '/deposit';
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isStripeDeposit);
    };
    /**
     * Get stripe payment intent.
     *
     * @returns {Observable<AxiosResponse<IContainer<IPaymentIntent>>>}
     *
     * @memberof AuthStripeEndpoint
     */
    AuthStripeEndpoint.prototype.intent = function () {
        // console.log('intent()');
        var url = this.prefix + '/intent';
        // console.log('url', url);
        var promise = this.api.http.get(url);
        return this.getContainer(promise, type_guards_1.isPaymentIntent);
    };
    /**
     * Delete stripe payment methods.
     *
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof AuthStripeEndpoint
     */
    AuthStripeEndpoint.prototype.delete = function () {
        // console.log('delete()');
        var url = this.prefix + '/method';
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    return AuthStripeEndpoint;
}(endpoint_1.default));
exports.AuthStripeEndpoint = AuthStripeEndpoint;
//# sourceMappingURL=authStripe.js.map