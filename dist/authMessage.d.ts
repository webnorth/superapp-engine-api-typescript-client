import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, IMessageLike, IAuthMessageListParams, IAuthMessageEndpoint } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Message endpoint
 *
 * @class AuthMessageEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IAuthMessageEndpoint}
 */
export declare class AuthMessageEndpoint extends Endpoint implements IAuthMessageEndpoint {
    /**
     * Creates an instance of AuthMessageEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof AuthMessageEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List all auth messages.
     *
     * @param {IAuthMessageListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IMessageLike>>>}
     *
     * @memberof AuthMessageEndpoint
     */
    list(params?: IAuthMessageListParams): Observable<AxiosResponse<ICollection<IMessageLike>>>;
    /**
     * Touch auth message by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<IMessageLike>>>}
     *
     * @memberof AuthMessageEndpoint
     */
    touch(id: number): Observable<AxiosResponse<IContainer<IMessageLike>>>;
}
