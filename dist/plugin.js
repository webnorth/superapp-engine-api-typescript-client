"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * Plugin endpoint
 *
 * @class PluginEndpoint
 * @extends {Endpoint}
 * @implements {IPluginEndpoint}
 */
var PluginEndpoint = /** @class */ (function (_super) {
    __extends(PluginEndpoint, _super);
    /**
     * Creates an instance of PluginEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof PluginEndpoint
     */
    function PluginEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/plugins') || this;
    }
    /**
     * List all plugins.
     *
     * @param {ICollectionParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IPlugin>>>}
     *
     * @memberof PluginEndpoint
     */
    PluginEndpoint.prototype.list = function (params) {
        // console.log('list(params?: ICollectionParams)', params);
        var url = this.prefix;
        return this.getCollection(url, params, type_guards_1.isPlugin);
    };
    /**
     * Get plugin by id.
     *
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<IPlugin>>>}
     *
     * @memberof PluginEndpoint
     */
    PluginEndpoint.prototype.get = function (id) {
        // console.log('get(id: number)', id);
        var url = this.prefix + "/" + id;
        var promise = this.api.http.get(url);
        return this.getContainer(promise, type_guards_1.isPlugin);
    };
    return PluginEndpoint;
}(endpoint_1.default));
exports.PluginEndpoint = PluginEndpoint;
//# sourceMappingURL=plugin.js.map