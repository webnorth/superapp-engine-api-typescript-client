import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IContainer, ICollection, ICollectionParams, IAttributeValues, ILangParam } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * TicketTemplate raw object
 *
 * @export
 * @interface ITicketTemplate
 */
export interface ITicketTemplate {
    id: number;
    name: string;
    description: string;
    body: string;
    ticket_header_image: number;
    nametag_html_template: string;
    group_plugin_id: number;
    attributes?: IAttributeValues;
}
/**
 * TicketTemplate get params
 *
 * @export
 * @interface ITicketTemplateGetParams
 */
export interface ITicketTemplateGetParams extends ILangParam {
    group_plugin_id?: number;
    with_attributes?: 0 | 1;
}
/**
 * TicketTemplate list params
 *
 * @export
 * @interface ITicketTemplateListParams
 */
export interface ITicketTemplateListParams extends ICollectionParams, ITicketTemplateGetParams {
}
/**
 * TicketTemplate specs
 *
 * @export
 * @interface ITicketTemplateSpecs
 */
export interface ITicketTemplateSpecs extends IAttributeValues, ILangParam {
    name: string;
    description?: string;
    body: string;
    ticket_header_image?: number;
    nametag_html_template?: string;
    group_plugin_id?: number;
    with_attributes?: 0 | 1;
}
/**
 * TicketTemplate update specs
 *
 * @export
 * @interface ITicketTemplateUpdateSpecs
 */
export interface ITicketTemplateUpdateSpecs extends IAttributeValues, ILangParam {
    name?: string;
    description?: string;
    body?: string;
    ticket_header_image?: number;
    nametag_html_template?: string;
    with_attributes?: 0 | 1;
}
/**
 * TicketTemplate endpoint methods
 *
 * @export
 * @interface ITicketTemplateEndpoint
 */
export interface ITicketTemplateEndpoint {
    list(group_id: number, params?: ITicketTemplateListParams): Observable<AxiosResponse<ICollection<ITicketTemplate>>>;
    create(group_id: number, specs: ITicketTemplateSpecs): Observable<AxiosResponse<IContainer<ITicketTemplate>>>;
    get(group_id: number, id: number, params?: ITicketTemplateGetParams): Observable<AxiosResponse<IContainer<ITicketTemplate>>>;
    update(group_id: number, id: number, specs: ITicketTemplateUpdateSpecs): Observable<AxiosResponse<IContainer<ITicketTemplate>>>;
    delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}
/**
 * Guard of ITicketTemplate.
 *
 * @export
 * @param {any} data
 * @returns {data is ITicketTemplate}
 */
export declare function isTicketTemplate(data: any): data is ITicketTemplate;
/**
 * TicketTemplate endpoint
 *
 * @class TicketTemplateEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {ITicketTemplateEndpoint}
 */
export declare class TicketTemplateEndpoint extends Endpoint implements ITicketTemplateEndpoint {
    /**
     * Creates an instance of TicketTemplateEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof TicketTemplateEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List all ticket templates.
     *
     * @param {number} group_id
     * @param {ITicketTemplateListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<ITicketTemplate>>>}
     *
     * @memberof TicketTemplateEndpoint
     */
    list(group_id: number, params?: ITicketTemplateListParams): Observable<AxiosResponse<ICollection<ITicketTemplate>>>;
    /**
     * Create new ticket template.
     *
     * @param {number} group_id
     * @param {ITicketTemplateSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<ITicketTemplate>>>}
     *
     * @memberof TicketTemplateEndpoint
     */
    create(group_id: number, specs: ITicketTemplateSpecs): Observable<AxiosResponse<IContainer<ITicketTemplate>>>;
    /**
     * Get ticket template by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ITicketTemplateGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<ITicketTemplate>>>}
     *
     * @memberof TicketTemplateEndpoint
     */
    get(group_id: number, id: number, params?: ITicketTemplateGetParams): Observable<AxiosResponse<IContainer<ITicketTemplate>>>;
    /**
     * Update ticket template by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {ITicketTemplateUpdateSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<ITicketTemplate>>>}
     *
     * @memberof TicketTemplateEndpoint
     */
    update(group_id: number, id: number, specs: ITicketTemplateUpdateSpecs): Observable<AxiosResponse<IContainer<ITicketTemplate>>>;
    /**
     * Delete ticket template by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof TicketTemplateEndpoint
     */
    delete(group_id: number, id: number): Observable<AxiosResponse<IContainer<void>>>;
}
