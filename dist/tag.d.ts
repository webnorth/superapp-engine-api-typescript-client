import { Observable } from 'rxjs';
import Endpoint from './common/endpoint';
import { IAttributeListParams, ICollection, ITag } from './common/interfaces';
import SuperappEngine from './superappEngine';
import { AxiosResponse } from 'axios';
/**
 * Tag tags() params
 *
 * @export
 * @interface ITagsParams
 */
export interface ITagsParams extends IAttributeListParams {
    group_plugin_id: number;
}
/**
 * Tag endpoint methods
 *
 * @export
 * @interface ITagEndpoint
 */
export interface ITagEndpoint {
    list(group_id: number, params: ITagsParams): Observable<AxiosResponse<ICollection<ITag>>>;
}
/**
 * Tag endpoint
 *
 * @class TagEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {ITagEndpoint}
 */
export declare class TagEndpoint extends Endpoint implements ITagEndpoint {
    /**
     * Creates an instance of TagEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof TagEndpoint
     */
    constructor(superappEngine: SuperappEngine);
    /**
     * List all group tags.
     *
     * @param {number} group_id
     * @param {ITagsParams} params
     * @returns {Observable<AxiosResponse<ICollection<ITag>>>}
     *
     * @memberof TagEndpoint
     */
    list(group_id: number, params: ITagsParams): Observable<AxiosResponse<ICollection<ITag>>>;
}
