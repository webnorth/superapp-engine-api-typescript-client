"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("./common/endpoint");
var type_guards_1 = require("./common/type.guards");
/**
 * UserAttribute endpoint
 *
 * @class UserAttributeEndpoint
 * @extends {Endpoint<SuperappEngine>}
 * @implements {IUserAttributeEndpoint}
 */
var UserAttributeEndpoint = /** @class */ (function (_super) {
    __extends(UserAttributeEndpoint, _super);
    /**
     * Creates an instance of UserAttributeEndpoint.
     * @param {SuperappEngine} superappEngine
     *
     * @memberof UserAttributeEndpoint
     */
    function UserAttributeEndpoint(superappEngine) {
        // console.log('constructor(superappEngine: SuperappEngine)', superappEngine);
        return _super.call(this, superappEngine, '/groups/:group_id/users/attributes') || this;
    }
    /**
     * List all group user attributes.
     *
     * @param {number} group_id
     * @param {IAttributeListParams} [params]
     * @returns {Observable<AxiosResponse<ICollection<IAttribute>>>}
     *
     * @memberof UserAttributeEndpoint
     */
    UserAttributeEndpoint.prototype.list = function (group_id, params) {
        // console.log('list(group_id, params?)', group_id, params);
        var url = this.replacePrefixSegments({ group_id: group_id });
        // console.log('url', url);
        return this.getCollection(url, params, type_guards_1.isAttribute);
    };
    /**
     * Create new group user attribute.
     *
     * @param {number} group_id
     * @param {IAttributeSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IAttribute>>>}
     *
     * @memberof UserAttributeEndpoint
     */
    UserAttributeEndpoint.prototype.create = function (group_id, specs) {
        // console.log('create(group_id, specs)', group_id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id });
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isAttribute);
    };
    /**
     * Get group user attribute by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IAttributeGetParams} [params]
     * @returns {Observable<AxiosResponse<IContainer<IAttribute>>>}
     *
     * @memberof UserAttributeEndpoint
     */
    UserAttributeEndpoint.prototype.get = function (group_id, id, params) {
        // console.log('get(group_id, id, params?)', group_id, id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        // console.log('url', url);
        var promise = this.api.http.get(url);
        return this.getContainer(promise, type_guards_1.isAttribute);
    };
    /**
     * List all group user attributes list_related.
     *
     * @param {number} group_id
     * @param {IUserAttributeListRelatedParams} params
     * @returns {Observable<AxiosResponse<ICollection<IAttribute>>>}
     *
     * @memberof UserAttributeEndpoint
     */
    UserAttributeEndpoint.prototype.list_related = function (group_id, params) {
        // console.log('list(group_id, params)', group_id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/list_related';
        // console.log('url', url);
        return this.getCollection(url, params, type_guards_1.isAttribute);
    };
    /**
     * Get group user attribute based on - category; previous attribute; user value status.
     *
     * @param {number} group_id
     * @param {IUserAttributeNextParams} params
     * @returns {Observable<AxiosResponse<IContainer<IAttribute>>>}
     *
     * @memberof UserAttributeEndpoint
     */
    UserAttributeEndpoint.prototype.next = function (group_id, params) {
        // console.log('next(group_id, params)', group_id, params);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/next';
        // console.log('url', url);
        var promise = this.api.http.get(url, { params: params });
        return this.getContainer(promise, type_guards_1.isAttributeOrVoid);
    };
    /**
     * Update group user attribute by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @param {IAttributeSpecs} specs
     * @returns {Observable<AxiosResponse<IContainer<IAttribute>>>}
     *
     * @memberof UserAttributeEndpoint
     */
    UserAttributeEndpoint.prototype.update = function (group_id, id, specs) {
        // console.log('update(group_id, id, specs)', group_id, id, specs);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        var promise = this.api.http.post(url, specs);
        return this.getContainer(promise, type_guards_1.isAttribute);
    };
    /**
     * Delete group user attribute by id.
     *
     * @param {number} group_id
     * @param {number} id
     * @returns {Observable<AxiosResponse<IContainer<void>>>}
     *
     * @memberof UserAttributeEndpoint
     */
    UserAttributeEndpoint.prototype.delete = function (group_id, id) {
        // console.log('get(group_id, id)', group_id, id);
        var url = this.replacePrefixSegments({ group_id: group_id }) + '/' + id;
        var promise = this.api.http.delete(url);
        return this.getContainer(promise);
    };
    return UserAttributeEndpoint;
}(endpoint_1.default));
exports.UserAttributeEndpoint = UserAttributeEndpoint;
//# sourceMappingURL=userAttribute.js.map