# SuperApp Engine API TypeScript client

## Environment

### Node version
Recommended node v14 and npm v7

### Node Version Manager (NVM)
If you are using NVM, type `nvm use` to use the appropriate version.

## Making changes
- Make changes to the code in `src/`
- Run `npm run build`
- Commit the changes to Git, including `dist/`
